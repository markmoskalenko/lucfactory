<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tbl_education".
 *
 * @property integer $id
 * @property string $start_date
 * @property string $end_date
 * @property integer $company_id
 * @property integer $country_id
 * @property integer $city_id
 * @property string $comment
 * @property integer $user_id
 *
 * @property DictionarySchools $company
 * @property User $user
 */
class Station extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_station';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'name', 'performance', 'price', 'img'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Категория'),
            'name'=> Yii::t('app', 'Название'),
            'performance' => Yii::t('app', 'Мощьность'),
            'price' => Yii::t('app', 'Цена'),
            'img' => Yii::t('app', 'Картинка'),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getUserStation()
    {
        return $this->hasOne(UserStation::className(), ['stantion_id'=>'id'])->andOnCondition(['user_id'=> User::u()->id]);
    }

    public function getUsersStation()
    {
        return $this->hasMany(UserStation::className(), ['stantion_id'=>'id']);
    }

    public function search($params)
    {
        $query = Station::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'category' => $this->category,
            'name' => $this->name,
            'performance' => $this->performance,
            'price' => $this->price,
            'img' => $this->img,
        ]);

        $query->andFilterWhere(['like', 'category', $this->category])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'performance', $this->performance])
            ->andFilterWhere(['like', 'price', $this->price])
            ->andFilterWhere(['like', 'img', $this->img]);


        return $dataProvider;
    }

    public function buy($oUserStation, $station){
        $oUser = User::u();
        $oUser->setScenario('buy');
        if ($oUser->in_balance >= $station->price) {
            $oUser->in_balance = $oUser->in_balance - $station->price;
            if ($oUser->save()) {

                $oUserStation->user_id = $oUser::u()->id;
                $oUserStation->stantion_id = $station->id;
                $oUserStation->count_stations_purchased += 1;
                $oUserStation->save();
                Yii::$app->session->setFlash('success', Yii::t('app', 'Денежные средства списаны!'));
            }

        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'У вас не достаточно средств!'));
        }
    }

    public static function water(){
        $oStation = Station::find()->all();

        foreach ($oStation as $station){

            $usersStantion = $station->usersStation;

            foreach( $usersStantion as $us ){

                if( $us ){
                    $us->woter += $us->count_stations_purchased *  $station->performance;
                    $us->save();
                }
            };
        }
    }
}