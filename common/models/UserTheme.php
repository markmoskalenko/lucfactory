<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_user_theme".
 *
 * @property string $id
 * @property string $theme_id
 * @property string $user_id
 * @property integer $is_current
 * @property string $created_at
 *
 * @property Themes $theme
 * @property User $user
 */
class UserTheme extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user_theme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['theme_id'], 'required'],
            [['theme_id', 'user_id', 'is_current'], 'integer'],
            [['created_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'theme_id' => Yii::t('app', 'ID темы'),
            'user_id' => Yii::t('app', 'ID пользователя'),
            'is_current' => Yii::t('app', 'Текущая'),
            'created_at' => Yii::t('app', 'Создана в'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Themes::className(), ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    public static function saveBuyTemplate($id){

        $model = new self;
        $model->theme_id =$id;
        return $model->save();

    }

    /**
     * Сохранение добавленой темы
     * @param $id
     * @return bool
     */

    public static function saveAddTemplate($id){

        $model = new self;
        $model->theme_id =$id;
        return $model->save();

    }


    /**
     * Применение темы
     *
     * @return bool
     */
    public function setCurrent(){

        self::updateAll(['is_current'=>0], ['user_id'=>User::u()->id]);

        $this->is_current = 1;

        return $this->save();
    }

    public function beforeSave($insert){

        if (parent::beforeSave($insert)) {
            if($this->isNewRecord){
                $this->user_id = \Yii::$app->user->id;
                $this->is_current=0;
            }
            return true;
        } else {
            return false;
        }
    }

}
