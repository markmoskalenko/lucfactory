<?php

namespace common\models;

use Doctrine\Common\Collections\Criteria;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tbl_themes".
 *
 * @property string $id
 * @property string $title
 * @property string $img
 * @property string $cost
 * @property string $system_name
 * @property integer $is_published
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 * @property UserTheme[] $userThemes
 */
class Lottery extends \yii\db\ActiveRecord
{
    public $winer_one;
    public $winer_two;
    public $winer_three;
    public $count;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_lottery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id','winer', 'number', 'date'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'number' => Yii::t('app', '№ билета'),
            'date' => Yii::t('app', 'Дата'),
            'winer' => Yii::t('app', 'Победитель'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function search()
    {
        $query = Lottery::find()->andWhere('winer=:winer', [':winer' => 1]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]],
            'pagination'=>false,
        ]);

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'number' => $this->number,
            'date' => $this->date,
            'winer' => $this->winer,
        ]);

        return $dataProvider;
    }


    public function searchAll()
    {
        $sDateStart = date('Y-m-d').' 00:00:00';
        $sDateEnd = date('Y-m-d').' 23:59:59';
        $query = Lottery::findBySql("SELECT * FROM tbl_lottery  WHERE date BETWEEN STR_TO_DATE('".$sDateStart."', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('".$sDateEnd."', '%Y-%m-%d %H:%i:%s')");

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_ASC]],
            'pagination'=>false,
        ]);

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'number' => $this->number,
            'date' => $this->date,
            'winer' => $this->winer,
        ]);

        return $dataProvider;
    }

    public function buyLottery(){
        $oUser = User::u();
        $oUser->setScenario('buy');
        $sDateStart = date('Y-m-d').' 00:00:00';
        $sDateEnd = date('Y-m-d').' 23:59:59';
        $oLottery = Lottery::findBySql("SELECT COUNT(id) FROM tbl_lottery  WHERE date BETWEEN STR_TO_DATE('".$sDateStart."', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('".$sDateEnd."', '%Y-%m-%d %H:%i:%s')")->count();


        if($oLottery < 10){

            if ($oUser->in_balance >= 1000) {
                $oUser->in_balance = $oUser->in_balance - 1000;
                if ($oUser->save()) {
                    $this->user_id = $oUser->id;
                    $this->number = $oLottery + 1;
                    $this->save();

                    Yii::$app->session->setFlash('success', Yii::t('app', 'Лотерейный билет куплен!'));
                    return true;
                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'У вас не достаточно средств!'));
                return false;
            }
        }elseif($oLottery == 10){
            $oWiner = Lottery::findBySql("SELECT * FROM tbl_lottery  WHERE date BETWEEN STR_TO_DATE('".$sDateStart."', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('".$sDateEnd."', '%Y-%m-%d %H:%i:%s') ORDER BY RAND() LIMIT 3")->all();

            $oLottery = Lottery::findBySql("SELECT COUNT(id) FROM tbl_lottery WHERE DATE BETWEEN STR_TO_DATE('".$sDateStart."', '%Y-%m-%d %H:%i:%s') AND STR_TO_DATE('".$sDateEnd."', '%Y-%m-%d %H:%i:%s') AND winer='1'")->count();

            if($oLottery == 3){
                Yii::$app->session->setFlash('error', Yii::t('app', 'Лотерея проводится один раз в день!'));
                return false;
            }

            $i = 1;
            foreach($oWiner as $lottery){
                if($i==1){
                    $lottery->winer = 1;
                    $this->winer_one = $lottery->number;
                    $lottery->user->in_balance += 5000;
                    $lottery->user->setScenario('buy');
                    $lottery->user->save();
                    $lottery->save();
                }
                if($i==2){
                    $lottery->winer = 1;
                    $this->winer_two = $lottery->number;
                    $lottery->user->in_balance += 2500;
                    $lottery->user->setScenario('buy');
                    $lottery->user->save();
                    $lottery->save();
                }
                if($i==3){
                    $lottery->winer = 1;
                    $this->winer_three = $lottery->number;
                    $lottery->user->in_balance += 2000;
                    $lottery->user->setScenario('buy');
                    $lottery->user->save();
                    $lottery->save();
                }
                $i++;
            }
            $this->count = count($oWiner);

            if($oUser->save()){

                Yii::$app->session->setFlash('success', 'Билет с № '.$this->winer_one.' выйграл 5000 USD<br/> Билет с № '.$this->winer_two.' выйграл 2500 USD<br/>
                Билет с № '.$this->winer_three.' выйграл 2000 USD');
                return true;
            }

        }
    }

}
