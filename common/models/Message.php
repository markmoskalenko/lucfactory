<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_messages".
 *
 * @property integer $id
 * @property string $date
 * @property integer $to_user_id
 * @property string $name
 * @property integer $is_read
 * @property string $email
 * @property string $text
 * @property integer $category
 *
 * @property User $user
 */
class Message extends \yii\db\ActiveRecord
{
    const CATEGORY_MAIN     = 1;
    const CATEGORY_MYRES    = 2;
    const CATEGORY_ARCHIVE  = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_messages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['date', 'safe'],

            ['to_user_id', 'integer'],

            ['is_read', 'integer'],

            ['category', 'integer'],

            ['text', 'string'],

            ['text', 'filter', 'filter' => function ($value) {
                    return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
                }],

            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['name', 'required'],
            ['name', 'string', 'max' => 255],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'date' => Yii::t('app', 'Дата'),
            'to_user_id' => Yii::t('app', 'Получатель'),
            'name' => Yii::t('app', 'Имя пользователя'),
            'is_read' => Yii::t('app', 'Прочтено'),
            'email' => Yii::t('app', 'Email'),
            'text' => Yii::t('app', 'Сообщение'),
            'category' => Yii::t('app', 'Категория'),
        ];
    }

    public static function getNewCount(){
        return self::find()
                ->where(['to_user_id' => Yii::$app->user->id, 'is_read'=>0])
                ->asArray()
                ->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserTo()
    {
        return $this->hasOne(User::className(), ['id' => 'to_user_id']);
    }
}