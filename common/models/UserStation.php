<?php

namespace common\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "tbl_themes".
 *
 * @property string $id
 * @property string $title
 * @property string $img
 * @property string $cost
 * @property string $system_name
 * @property integer $is_published
 * @property integer $sort
 * @property string $created_at
 * @property string $updated_at
 * @property UserTheme[] $userThemes
 */
class UserStation extends \yii\db\ActiveRecord
{
    public $image;

    public $title;

    public $translateModelName = '\common\models\ThemesTranslate';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_userstantion';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'stantion_id', 'count_stations_purchased', 'woter', 'update' , 'lutz'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', ''),
            'stantion_id' => Yii::t('app', ''),
            'count_stations_purchased' => Yii::t('app', ''),
            'woter' => Yii::t('app', ''),
            'lutz' => Yii::t('app', ''),
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStation()
    {
        return $this->hasOne(Station::className(), ['stantion_id' => 'id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStations()
    {
        return $this->hasOne(Station::className(), ['id' => 'stantion_id']);
    }


    /**
     * Покупка шаблона и снятие денег
     * @return bool
     */
    public function buy(){

        if( UserTheme::find()->where(['user_id'=>User::u()->id, 'theme_id'=>$this->id])->exists()){

            Yii::$app->session->setFlash('success', Yii::t('app', 'Тема уже куплена!'));

        }else{


            if($isBuy = Invoice::addInvoice($this->cost,'Покупка темы', Invoice::STATUS_SUCCESS, Invoice::TYPE_DECREMENT)){

                Yii::$app->session->setFlash('success', Yii::t('app', 'Денежные средства списаны!'));

                if( UserTheme::saveBuyTemplate($this->id) ){
                    Yii::$app->session->setFlash('success', Yii::t('app', 'Тема успешно куплена!'));
                }else{
                    Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения темы. Обратитесь к админестрации сайта.'));
                }

            }else{
                Yii::$app->session->setFlash('error', Yii::t('app', 'У вас недостаточно денежных средств. Пополните баланс.'));
            }

        }

        return;
    }


    public function add(){
        if( UserTheme::saveAddTemplate($this->id) ){
            Yii::$app->session->setFlash('success', Yii::t('app', 'Тема успешно добавлена!'));
        }else{
            Yii::$app->session->setFlash('error', Yii::t('app', 'Ошибка сохранения темы. Обратитесь к админестрации сайта.'));
        }
    }



}
