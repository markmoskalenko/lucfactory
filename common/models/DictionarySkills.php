<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_dictionary_skills".
 *
 * @property string $id
 * @property string $name
 * @property integer $is_public
 * @property string $language_id
 *
 * @property DictionaryLanguage $language
 * @property Skills[] $skills
 */
class DictionarySkills extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dictionary_skills';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'language_id'], 'required'],
            [['is_public', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Значение'),
            'is_public' => Yii::t('app', 'Опубликовано'),
            'language_id' => Yii::t('app', 'Язык'),
        ];
    }

    public static function getIdByName($name, $create = true){

        $oModel = parent::find()->andWhere(['name'=>$name])->one();

        if( !$oModel && $create ){
            $oModel = new self;

            $oModel->name = $name;
            $oModel->language_id = DictionaryLanguage::getCurrent()->id;
            $oModel->is_public = 0;
            $oModel->save();
        }

        return $oModel->id;
    }


    public static function getByAutocomlete(){

        $query = self::find();

        $query->andFilterWhere([
            'is_public' => 1,
            'language_id' => DictionaryLanguage::getCurrent()->id,
        ]);

        $r = $query->asArray()->all();

        $data = [];
        foreach( $r as $i ){
            $data[] = $i['name'];
        }
        return $data;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSkills()
    {
        return $this->hasMany(Skills::className(), ['skills_id' => 'id']);
    }
}
