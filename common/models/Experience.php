<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_experience".
 *
 * @property string $id
 * @property string $start_date
 * @property string $end_date
 * @property string $company_id
 * @property string $country_id
 * @property string $city_id
 * @property string $comment
 * @property string $user_id
 *
 * @property DictionaryCompany $company
 * @property User $user
 */
class Experience extends \yii\db\ActiveRecord
{
    public $company_name;
    public $position_name;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_experience';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date','company_name', 'position_name','company_id', 'country_id', 'city_id', 'comment'], 'required'],
            [['company_id', 'country_id', 'city_id', 'user_id', 'position_id', 'is_public'], 'integer'],
            [['comment', 'company_name', 'position_name'], 'string'],
            ['comment', 'filter', 'filter' => function ($value) {
                    return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),

            'start_date' => Yii::t('app', 'Дата начала'),
            'end_date' => Yii::t('app', 'Дата окончания'),
            'company_id' => Yii::t('app', 'Компания'),
            'company_name' => Yii::t('app', 'Компания'),
            'country_id' => Yii::t('app', 'Страна'),
            'city_id' => Yii::t('app', 'Город'),
            'comment' => Yii::t('app', 'Комментарий'),
            'user_id' => Yii::t('app', 'ID пользователя'),
            'position_id' => Yii::t('app', 'Должность'),
            'position_name' => Yii::t('app', 'Должность'),
            'dates' => Yii::t('app', 'Период работы'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(DictionaryCompany::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['country_id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['city_id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPosition()
    {
        return $this->hasOne(DictionaryPosition::className(), ['id' => 'position_id']);
    }

    public function afterFind(){

        $this->company_name = $this->company->name;

        $this->position_name = $this->position->name;


        parent::afterFind();
    }

    public function beforeValidate(){

        $this->user_id = User::u()->id;

        $this->company_id = DictionaryCompany::getIdByName( $this->company_name );

        $this->position_id = DictionaryPosition::getIdByName( $this->position_name );

        return parent::beforeValidate();
    }
}