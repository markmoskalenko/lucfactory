<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * PagesSearch represents the model behind the search form about `common\models\Pages`.
 */
class PagesSearch extends Pages
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'is_public'], 'integer'],
            [['title', 'content', 'description', 'meta_k', 'meta_d', 'slug'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Pages::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


//        $dataProvider->sort->attributes['title'] = [
//            'asc' => ['tr.title' => SORT_ASC],
//            'desc' => ['tr.title' => SORT_DESC],
//        ];

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        'id' => SORT_ASC,
                    ],
                    'desc' => [
                        'id' => SORT_DESC,
                    ],
                ],
                'slug' => [
                    'asc' => [
                        'slug' => SORT_ASC,
                    ],
                    'desc' => [
                        'slug' => SORT_DESC,
                    ],
                ],
                'is_public' => [
                    'asc' => [
                        'is_public' => SORT_ASC,
                    ],
                    'desc' => [
                        'is_public' => SORT_DESC,
                    ],
                ],
                'created_at' => [
                    'asc' => [
                        'created_at' => SORT_ASC,
                    ],
                    'desc' => [
                        'created_at' => SORT_DESC,
                    ],
                ],
                'title' => [
                    'asc' => [
                        'tr.title' => SORT_ASC,
                    ],
                    'desc' => [
                        'tr.title' => SORT_DESC,
                    ],
                ],
                'description' => [
                    'asc' => [
                        'tr.description' => SORT_ASC,
                    ],
                    'desc' => [
                        'tr.description' => SORT_DESC,
                    ],
                ],
                'meta_k' => [
                    'asc' => [
                        'tr.meta_k' => SORT_ASC,
                    ],
                    'desc' => [
                        'tr.meta_k' => SORT_DESC,
                    ],
                ],
                'meta_d' => [
                    'asc' => [
                        'tr.meta_d' => SORT_ASC,
                    ],
                    'desc' => [
                        'tr.meta_d' => SORT_DESC,
                    ],
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'is_public' => $this->is_public,
        ]);

        $query
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'tr.title', $this->title])
            ->andFilterWhere(['like', 'tr.description', $this->description])
            ->andFilterWhere(['like', 'tr.meta_k', $this->meta_k])
            ->andFilterWhere(['like', 'tr.meta_d', $this->meta_d])
        ;

        return $dataProvider;
    }
}
