<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_dictionary_position".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_public
 * @property string $language_id
 *
 * @property DictionaryLanguage $language
 * @property Experience[] $experiences
 */
class DictionaryPosition extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dictionary_position';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_public', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['name', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'is_public' => Yii::t('app', 'Is Public'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public static function getIdByName($name, $create = true){

        $oPosition = parent::find()->andWhere(['name'=>$name])->one();

        if( !$oPosition && $create ){
            $oPosition = new self;
            $oPosition->name = $name;
            $oPosition->language_id = DictionaryLanguage::getCurrent()->id;
            $oPosition->is_public = 0;
            $oPosition->save();
        }

        return $oPosition->id;
    }

    public static function getByAutocomlete(){

        $query = self::find();

        $query->andFilterWhere([
            'is_public' => 1,
            'language_id' => DictionaryLanguage::getCurrent()->id,
        ]);

        $r = $query->asArray()->all();

        $data = [];
        foreach( $r as $i ){
            $data[] = $i['name'];
        }
        return $data;

    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExperiences()
    {
        return $this->hasMany(Experience::className(), ['position_id' => 'id']);
    }
}
