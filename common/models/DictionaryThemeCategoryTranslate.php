<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_dictionary_theme_category_translate".
 *
 * @property integer $id
 * @property string $title
 * @property integer $object_id
 * @property integer $language_id
 *
 * @property DictionaryLanguage $language
 * @property DictionaryThemeCategory $object
 */
class DictionaryThemeCategoryTranslate extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_dictionary_theme_category_translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'object_id', 'language_id'], 'required'],
            [['object_id', 'language_id'], 'integer'],
            [['title'], 'string', 'max' => 255],
            ['title', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'object_id' => Yii::t('app', 'Object ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLanguage()
    {
        return $this->hasOne(DictionaryLanguage::className(), ['id' => 'language_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(DictionaryThemeCategory::className(), ['id' => 'object_id']);
    }
}
