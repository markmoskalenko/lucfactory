<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $is_public
 * @property string $slug
 *
 */
class Feedback extends \yii\db\ActiveRecord
{
    public $captcha;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['username'], 'string', 'max' => 255],

            ['description', 'required'],
            ['description', 'string'],

            ['captcha', 'required'],
            ['captcha', 'captcha'],

            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Контактное лицо:',
            'description' => 'Текст сообщения:',
            'email' => 'Почта для связи:',
            'captcha' => 'Код на картинке:',
        ];
    }

    public function search()
    {
        $query = Feedback::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);


//        $dataProvider->sort->attributes['title'] = [
//            'asc' => ['tr.title' => SORT_ASC],
//            'desc' => ['tr.title' => SORT_DESC],
//        ];

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        'id' => SORT_ASC,
                    ],
                    'desc' => [
                        'id' => SORT_DESC,
                    ],
                ],
                'username' => [
                    'asc' => [
                        'username' => SORT_ASC,
                    ],
                    'desc' => [
                        'username' => SORT_DESC,
                    ],
                ],
                'email' => [
                    'asc' => [
                        'email' => SORT_ASC,
                    ],
                    'desc' => [
                        'email' => SORT_DESC,
                    ],
                ],

            ],
        ]);

        return $dataProvider;
    }


}
