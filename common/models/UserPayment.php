<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "tbl_user".
 *
 * @property string $id
 * @property string $created_at
 * @property string $sum
 * @property integer $user_id
 *
 */
class UserPayment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['sum','status', 'user_id'], 'number'],
            ['purse', 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата',
            'sum' => 'Сумма',
            'user_id' => 'Пользователь',
            'status' => 'Выплачено',
            'purse' => 'Кошелек',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentHistories()
    {
        return $this->hasMany(PaymentHistory::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }


    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = UserPayment::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=> ['id'=>SORT_DESC]]
        ]);

        return $dataProvider;
    }
    public function searchPayments()
    {
        $sDateStart = date('Y-m-d').' 00:00:00';
        $sDateEnd = date('Y-m-d').' 23:59:59';

        $query = UserPayment::find()->andWhere(['between', 'created_at', $sDateStart,$sDateEnd, ])->andWhere('status=:status',[':status'=>1])->limit(100);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=> ['id'=>SORT_DESC]]
        ]);

        return $dataProvider;
    }
    public function searchUserPayment()
    {
        $oUser = User::u();
        $query = UserPayment::find()->andWhere('user_id=:user_id',['user_id'=>$oUser->id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder'=> ['id'=>SORT_DESC]]
        ]);

        return $dataProvider;
    }

}
