<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "tbl_user".
 *
 * @property string $id
 * @property string $created_at
 * @property string $sum
 * @property integer $user_id
 *
 */
class UserInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_user_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at'], 'safe'],
            [['sum', 'user_id'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата',
            'sum' => 'Сумма',
            'user_id' => 'Пользователь',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentHistories()
    {
        return $this->hasMany(PaymentHistory::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getOrderByUser($userId)
    {
        $order = self::find()->andWhere(['user_id'=>$userId])->one();

        if (!$order){
            $order = new UserInvoice();
            $order->user_id = $userId;
            $order->sum = Yii::$app->params['pay']['default_amount'];
            $order->created_at = time();
            $order->save();
        }

        return $order;
    }


}
