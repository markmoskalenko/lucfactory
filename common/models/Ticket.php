<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_tickets".
 *
 * @property integer $id
 * @property string $title
 * @property string $message
 * @property integer $created_at
 * @property integer $user_id
 * @property integer $status
 */
class Ticket extends \yii\db\ActiveRecord
{
    const STATUS_OK = 2;
    const STATUS_NEW = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_tickets';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'message', 'user_id'], 'required'],
            [['message'], 'string'],
            [['created_at', 'user_id', 'status'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Тема сообщения',
            'message' => 'Текст сообщения',
            'created_at' => 'Дата создания',
            'user_id' => 'Пользователь',
            'status' => 'Статус',
        ];
    }

    public static function getTickets($userId = null)
    {
        $sql = '
            SELECT * FROM '. self::tableName() .'
        ';
        if ($userId){
            $sql .= ' WHERE user_id = '. $userId;
        }

        $sql .= ' ORDER BY created_at ASC';
        return self::findBySql($sql)->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
