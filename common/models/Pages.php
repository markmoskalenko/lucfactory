<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_pages".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $is_public
 * @property string $slug
 *
 * @property PagesTranslate[] $pagesTranslates
 */
class Pages extends \yii\db\ActiveRecord
{
    public $contact_feedback_form = 5;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['slug', 'title', 'meta_k'], 'string', 'max' => 255],
            [['description', 'content', 'meta_d'], 'string'],
            [['created_at', 'is_public'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'content' => 'Контент',
            'created_at' => 'Создана',
            'is_public' => 'Опубликована',
            'slug' => 'URL',
        ];
    }


}
