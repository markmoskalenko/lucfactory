<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tbl_invoice".
 *
 * @property integer $id
 * @property integer $sum
 * @property integer $type
 * @property integer $date
 * @property string $description
 * @property string $status
 * @property integer $user_id
 *
 * @property User $user
 */
class Invoice extends \yii\db\ActiveRecord
{
    const STATUS_SUCCESS = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_PENDING = 3;
    const STATUS_FAIL = 4;
    const STATUS_SITEERROR = 5;

    const TYPE_INCREMTNT = 1;
    const TYPE_DECREMENT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'date', 'status', 'type'], 'integer'],
            [['sum'], 'number'],
            [['description'], 'string', 'max' => 255],
            ['description', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'Номер транзакции'),
            'user_id' => Yii::t('app', 'Пользователь'),
            'user' => Yii::t('app', 'Пользователь'),
            'sum' => Yii::t('app', 'Сумма'),
            'date' => Yii::t('app', 'Дата'),
            'type' => Yii::t('app', 'Тип'),
            'description' => Yii::t('app', 'Описание'),
            'status' => Yii::t('app', 'Статус'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public static function getType($code = 0)
    {
        $types = array(
            1 => \Yii::t('app', 'Начисление'),
            2 => \Yii::t('app', 'Снятие'),
        );
        if (isset($types[$code])) {
            return $types[$code];
        } else {
            return $types;
        }
    }

    public static function addInvoice($sum, $description, $status, $type, $invoiceId = 0)
    {
        if ($invoiceId == 0){
            $model = new Invoice();
        } else {
            $model = Invoice::findOne($invoiceId);
            if ($model == null) {
                $model = new Invoice();
            }
        }
        $model->sum = $sum;
        $model->description = $description;
        $model->status = $status;
        $model->type = $type;
        $model->user_id = Yii::$app->user->id;
        $model->date = time();

        $res = true;
        $user =  User::u();
        if ($status == self::STATUS_SUCCESS || $status == self::STATUS_ACCEPTED) {
            if ($type == self::TYPE_INCREMTNT) {
                $res = $user->incBalance($sum);
            } else {
                $res = $user->decBalance($sum);
            }
        }

        if ($res) {
            $model->balance = $user->in_balance;
            $res = $model->save();
        }

        return $res ? $model : $res;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $userId = null)
    {
        if ($userId) {
            $query = Invoice::find()->where(['user_id' => $userId, 'tbl_invoice.status' => Invoice::STATUS_SUCCESS])->orWhere(['user_id' => $userId, 'tbl_invoice.status' => Invoice::STATUS_ACCEPTED]);
        } else {
            $query = Invoice::find();
        }

        $query->joinWith(
            [
                'user' => function ($q) {
                        $q->from(User::tableName().' u');
                    },
            ]
        );

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id' => [
                    'asc' => [
                        'id' => SORT_ASC,
                    ],
                    'desc' => [
                        'id' => SORT_DESC,
                    ],
                ],
                'user' => [
                    'asc' => [
                        'u.username' => SORT_ASC,
                    ],
                    'desc' => [
                        'u.username' => SORT_DESC,
                    ],
                ],
                'type' => [
                    'asc' => [
                        'type' => SORT_ASC,
                    ],
                    'desc' => [
                        'type' => SORT_DESC,
                    ],
                ],
                'sum' => [
                    'asc' => [
                        'sum' => SORT_ASC,
                    ],
                    'desc' => [
                        'sum' => SORT_DESC,
                    ],
                ],
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}