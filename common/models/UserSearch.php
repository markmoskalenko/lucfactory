<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'profession_id', 'country_id', 'city_id', 'language_id', 'date_of_birth_day', 'date_of_birth_month', 'date_of_birth_year'], 'integer'],
            [['username', 'email', 'auth_key', 'password_hash', 'password_reset_token', 'created_at', 'updated_at', 'role', 'activate_key', 'subdomain', 'avatar', 'phone', 'skype', 's_vk', 's_gplus', 's_ya', 's_fb', 's_odn', 's_tw', 's_mail', 's_git', 's_inst', 's_status_title', 'surname', 'flickr', 'linkedin', 'my_site', 'phone_mob'], 'safe'],
            [['balance'], 'number'],

            ['username', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['email', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['subdomain', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['surname', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['s_status_title', 'filter', 'filter' => function ($value) {
                    return nl2br( \common\filters\HtmlPurifier::escape( $value, 'br') );
                }],

            ['s_fb', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['s_tw', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['flickr', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['s_gplus', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['s_vk','filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['s_git', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['linkedin', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['s_mail', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['phone', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['phone_mob', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['my_site', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['profession', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],

            ['activate_key', 'filter', 'filter' => function ($value) {
                    return \common\filters\HtmlPurifier::escape( $value );
                }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'=> ['defaultOrder' => ['id'=>SORT_DESC]]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'profession_id' => $this->profession_id,
            'country_id' => $this->country_id,
            'city_id' => $this->city_id,
            'language_id' => $this->language_id,
            'balance' => $this->balance,
            'date_of_birth_day' => $this->date_of_birth_day,
            'date_of_birth_month' => $this->date_of_birth_month,
            'date_of_birth_year' => $this->date_of_birth_year,
        ]);

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'role', $this->role])
            ->andFilterWhere(['like', 'activate_key', $this->activate_key])
            ->andFilterWhere(['like', 'subdomain', $this->subdomain])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 's_vk', $this->s_vk])
            ->andFilterWhere(['like', 's_gplus', $this->s_gplus])
            ->andFilterWhere(['like', 's_ya', $this->s_ya])
            ->andFilterWhere(['like', 's_fb', $this->s_fb])
            ->andFilterWhere(['like', 's_odn', $this->s_odn])
            ->andFilterWhere(['like', 's_tw', $this->s_tw])
            ->andFilterWhere(['like', 's_mail', $this->s_mail])
            ->andFilterWhere(['like', 's_git', $this->s_git])
            ->andFilterWhere(['like', 's_inst', $this->s_inst])
            ->andFilterWhere(['like', 's_status_title', $this->s_status_title])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'flickr', $this->flickr])
            ->andFilterWhere(['like', 'linkedin', $this->linkedin])
            ->andFilterWhere(['like', 'my_site', $this->my_site])
            ->andFilterWhere(['like', 'phone_mob', $this->phone_mob]);

        return $dataProvider;
    }
}
