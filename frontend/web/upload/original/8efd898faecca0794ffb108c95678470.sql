/*
 Navicat Premium Data Transfer

 Source Server         : IT-Yes
 Source Server Type    : MySQL
 Source Server Version : 50530
 Source Host           : by111.atservers.net
 Source Database       : user1152346_pozitivmag

 Target Server Type    : MySQL
 Target Server Version : 50530
 File Encoding         : utf-8

 Date: 08/21/2014 16:38:05 PM
*/

SET NAMES utf8;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
--  Table structure for `ActionLog`
-- ----------------------------
DROP TABLE IF EXISTS `ActionLog`;
CREATE TABLE `ActionLog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `event` tinyint(255) DEFAULT NULL,
  `model_name` varchar(50) DEFAULT '',
  `model_title` text,
  `datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `event` (`event`),
  KEY `datetime` (`datetime`),
  KEY `model_name` (`model_name`)
) ENGINE=MyISAM AUTO_INCREMENT=1843 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `ActionLog`
-- ----------------------------
BEGIN;
INSERT INTO `ActionLog` VALUES ('1', 'admin', '3', 'Order', '1', '2014-01-27 19:31:35'), ('2', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:31:07'), ('3', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:31:14'), ('4', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:32:16'), ('5', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:33:32'), ('6', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:36:14'), ('7', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:37:19'), ('8', 'admin', '2', 'StoreProduct', 'Nokia N9', '2014-01-27 20:37:55'), ('9', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:40:06'), ('10', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:51:22'), ('11', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:52:12'), ('12', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:54:00'), ('13', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:55:31'), ('14', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 20:56:05'), ('15', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 21:09:02'), ('16', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 21:15:24'), ('17', 'admin', '2', 'StoreManufacturer', 'Acer', '2014-01-27 21:20:55'), ('18', 'admin', '1', 'StoreCategory', 'Краски', '2014-01-30 17:43:18'), ('19', 'admin', '1', 'StoreCategory', 'Декоративные краски', '2014-02-11 19:06:19'), ('20', 'admin', '1', 'StoreCategory', 'ELISE', '2014-02-11 19:10:07'), ('21', 'admin', '2', 'StoreCategory', 'ELISE', '2014-02-11 19:10:11'), ('22', 'admin', '1', 'StoreCategory', 'NAXOS', '2014-02-11 19:10:17'), ('23', 'admin', '2', 'StoreCategory', 'NAXOS', '2014-02-11 19:10:21'), ('24', 'admin', '1', 'StoreCategory', 'EDONI', '2014-02-11 19:10:27'), ('25', 'admin', '2', 'StoreCategory', 'EDONI', '2014-02-11 19:10:30'), ('26', 'admin', '1', 'StoreCategory', 'DIORA', '2014-02-11 19:10:39'), ('27', 'admin', '1', 'StoreCategory', 'SHARDANA', '2014-02-11 19:10:45'), ('28', 'admin', '1', 'StoreCategory', 'NOBILI MANIE', '2014-02-11 19:10:50'), ('29', 'admin', '1', 'StoreCategory', 'VELUR', '2014-02-11 19:10:55'), ('30', 'admin', '1', 'StoreCategory', 'CONTRAST pearly', '2014-02-11 19:11:00'), ('31', 'admin', '1', 'StoreCategory', 'TERRE MEDICEE', '2014-02-11 19:11:04'), ('32', 'admin', '1', 'StoreCategory', 'PERSEPOLIS', '2014-02-11 19:11:09'), ('33', 'admin', '1', 'StoreCategory', 'BISANTYUM', '2014-02-11 19:11:13'), ('34', 'admin', '1', 'StoreCategory', 'ARETINО', '2014-02-11 19:11:19'), ('35', 'admin', '1', 'StoreCategory', 'NUVOLE', '2014-02-11 19:11:23'), ('36', 'admin', '1', 'StoreCategory', 'MILLELUCI', '2014-02-11 19:11:30'), ('37', 'admin', '1', 'StoreCategory', 'POMPEIANO', '2014-02-11 19:11:53'), ('38', 'admin', '2', 'StoreCategory', 'DIORA', '2014-02-11 19:12:19'), ('39', 'admin', '2', 'StoreCategory', 'SHARDANA', '2014-02-11 19:12:21'), ('40', 'admin', '2', 'StoreCategory', 'NOBILI MANIE', '2014-02-11 19:12:23'), ('41', 'admin', '2', 'StoreCategory', 'VELUR', '2014-02-11 19:12:25'), ('42', 'admin', '2', 'StoreCategory', 'CONTRAST pearly', '2014-02-11 19:12:27'), ('43', 'admin', '2', 'StoreCategory', 'CONTRAST pearly', '2014-02-11 19:12:29'), ('44', 'admin', '2', 'StoreCategory', 'TERRE MEDICEE', '2014-02-11 19:12:32'), ('45', 'admin', '2', 'StoreCategory', 'PERSEPOLIS', '2014-02-11 19:12:34'), ('46', 'admin', '2', 'StoreCategory', 'BISANTYUM', '2014-02-11 19:12:36'), ('47', 'admin', '2', 'StoreCategory', 'ARETINО', '2014-02-11 19:12:40'), ('48', 'admin', '2', 'StoreCategory', 'ARETINО', '2014-02-11 19:12:44'), ('49', 'admin', '2', 'StoreCategory', 'NUVOLE', '2014-02-11 19:12:54'), ('50', 'admin', '2', 'StoreCategory', 'MILLELUCI', '2014-02-11 19:13:00'), ('51', 'admin', '2', 'StoreCategory', 'MILLELUCI', '2014-02-11 19:13:03'), ('52', 'admin', '2', 'StoreCategory', 'POMPEIANO', '2014-02-11 19:13:06'), ('53', 'admin', '1', 'StoreManufacturer', 'MaxMeyer', '2014-02-11 19:14:03'), ('54', 'admin', '3', 'StoreCategory', 'ELISE', '2014-02-11 19:15:01'), ('55', 'admin', '3', 'StoreCategory', 'NAXOS', '2014-02-11 19:15:06'), ('56', 'admin', '3', 'StoreCategory', 'EDONI', '2014-02-11 19:15:06'), ('57', 'admin', '3', 'StoreCategory', 'DIORA', '2014-02-11 19:15:06'), ('58', 'admin', '3', 'StoreCategory', 'SHARDANA', '2014-02-11 19:15:06'), ('59', 'admin', '3', 'StoreCategory', 'NOBILI MANIE', '2014-02-11 19:15:06'), ('60', 'admin', '3', 'StoreCategory', 'ARETINО', '2014-02-11 19:15:06'), ('61', 'admin', '3', 'StoreCategory', 'VELUR', '2014-02-11 19:15:06'), ('62', 'admin', '3', 'StoreCategory', 'CONTRAST pearly', '2014-02-11 19:15:06'), ('63', 'admin', '3', 'StoreCategory', 'TERRE MEDICEE', '2014-02-11 19:15:06'), ('64', 'admin', '3', 'StoreCategory', 'PERSEPOLIS', '2014-02-11 19:15:06'), ('65', 'admin', '3', 'StoreCategory', 'BISANTYUM', '2014-02-11 19:15:06'), ('66', 'admin', '3', 'StoreCategory', 'NUVOLE', '2014-02-11 19:15:07'), ('67', 'admin', '3', 'StoreCategory', 'MILLELUCI', '2014-02-11 19:15:07'), ('68', 'admin', '3', 'StoreCategory', 'POMPEIANO', '2014-02-11 19:15:07'), ('69', 'admin', '3', 'StoreCategory', 'Декоративные краски', '2014-02-11 19:15:07'), ('70', 'admin', '1', 'StoreCategory', 'Декоративные краски', '2014-02-11 19:15:21'), ('71', 'admin', '1', 'StoreCategory', 'MaxMeyer', '2014-02-11 19:15:43'), ('72', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-02-11 19:15:49'), ('73', 'admin', '1', 'StoreCategory', 'SAIF', '2014-02-11 19:15:58'), ('74', 'admin', '2', 'StoreCategory', 'MaxMeyer', '2014-02-11 19:16:07'), ('75', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-02-11 19:16:09'), ('76', 'admin', '2', 'StoreCategory', 'SAIF', '2014-02-11 19:16:11'), ('77', 'admin', '2', 'StoreProduct', 'Nokia N9', '2014-02-11 19:16:42'), ('78', 'admin', '2', 'StoreProduct', 'Nokia N9', '2014-02-11 19:17:24'), ('79', 'admin', '3', 'StoreManufacturer', 'Lenovo', '2014-02-11 19:18:07'), ('80', 'admin', '3', 'StoreManufacturer', 'Asus', '2014-02-11 19:18:07'), ('81', 'admin', '3', 'StoreManufacturer', 'Dell', '2014-02-11 19:18:07'), ('82', 'admin', '3', 'StoreManufacturer', 'Fujitsu', '2014-02-11 19:18:07'), ('83', 'admin', '3', 'StoreManufacturer', 'HP', '2014-02-11 19:18:07'), ('84', 'admin', '3', 'StoreManufacturer', 'Apple', '2014-02-11 19:18:07'), ('85', 'admin', '3', 'StoreManufacturer', 'Sony', '2014-02-11 19:18:07'), ('86', 'admin', '3', 'StoreManufacturer', 'Acer', '2014-02-11 19:18:07'), ('87', 'admin', '3', 'StoreManufacturer', 'Logitech', '2014-02-11 19:18:07'), ('88', 'admin', '3', 'StoreManufacturer', 'Microlab', '2014-02-11 19:18:07'), ('89', 'admin', '3', 'StoreManufacturer', 'Edifier', '2014-02-11 19:18:07'), ('90', 'admin', '3', 'StoreManufacturer', 'Sven', '2014-02-11 19:18:07'), ('91', 'admin', '3', 'StoreManufacturer', 'LG', '2014-02-11 19:18:07'), ('92', 'admin', '3', 'StoreManufacturer', 'Samsung', '2014-02-11 19:18:07'), ('93', 'admin', '3', 'StoreManufacturer', 'Philips', '2014-02-11 19:18:07'), ('94', 'admin', '3', 'StoreManufacturer', 'HTC', '2014-02-11 19:18:07'), ('95', 'admin', '3', 'StoreManufacturer', 'Nokia', '2014-02-11 19:18:07'), ('96', 'admin', '3', 'StoreManufacturer', 'BlackBerry', '2014-02-11 19:18:07'), ('97', 'admin', '3', 'StoreManufacturer', 'MaxMeyer', '2014-02-11 19:18:07'), ('98', 'admin', '2', 'StoreProduct', 'Nokia N9', '2014-02-11 19:22:38'), ('99', 'admin', '3', 'StoreProduct', 'Apple MacBook Pro 15 Late 2011', '2014-02-11 19:23:55'), ('100', 'admin', '3', 'StoreProduct', 'Lenovo THINKPAD W520', '2014-02-11 19:23:55'), ('101', 'admin', '3', 'StoreProduct', 'Sven SPS-820', '2014-02-11 19:23:55'), ('102', 'admin', '3', 'StoreProduct', 'Edifier M1385', '2014-02-11 19:23:55'), ('103', 'admin', '3', 'StoreProduct', 'Edifier X600', '2014-02-11 19:23:55'), ('104', 'admin', '3', 'StoreProduct', 'Microlab FC 362', '2014-02-11 19:23:55'), ('105', 'admin', '3', 'StoreProduct', 'DELL U2412M', '2014-02-11 19:23:55'), ('106', 'admin', '3', 'StoreProduct', 'DELL U2312HM', '2014-02-11 19:23:55'), ('107', 'admin', '3', 'StoreProduct', 'LG Flatron M2250D', '2014-02-11 19:23:55'), ('108', 'admin', '3', 'StoreProduct', 'LG Flatron IPS226V', '2014-02-11 19:23:55'), ('109', 'admin', '3', 'StoreProduct', 'Samsung SyncMaster S22A350N', '2014-02-11 19:23:55'), ('110', 'admin', '3', 'StoreProduct', 'Philips 237E3QPHSU', '2014-02-11 19:23:55'), ('111', 'admin', '3', 'StoreProduct', 'Philips 227E3LSU', '2014-02-11 19:23:55'), ('112', 'admin', '3', 'StoreProduct', 'HP ZR2740w', '2014-02-11 19:23:55'), ('113', 'admin', '3', 'StoreProduct', 'HP ZR2440w', '2014-02-11 19:23:55'), ('114', 'admin', '3', 'StoreProduct', 'Samsung Galaxy Ace II', '2014-02-11 19:23:55'), ('115', 'admin', '3', 'StoreProduct', 'HTC One XL', '2014-02-11 19:23:55'), ('116', 'admin', '3', 'StoreProduct', 'HTC Sensation XL', '2014-02-11 19:23:55'), ('117', 'admin', '3', 'StoreProduct', 'Apple iPhone 4S 16Gb', '2014-02-11 19:23:55'), ('118', 'admin', '3', 'StoreProduct', 'Apple iPhone 3GS 8Gb', '2014-02-11 19:23:55'), ('119', 'admin', '3', 'StoreProduct', 'Apple iPhone 4 16Gb', '2014-02-11 19:23:55'), ('120', 'admin', '3', 'StoreProduct', 'Nokia N9', '2014-02-11 19:23:55'), ('121', 'admin', '3', 'StoreProduct', 'BlackBerry Bold 9900', '2014-02-11 19:23:55'), ('122', 'admin', '3', 'StoreProduct', 'BlackBerry Bold 9780', '2014-02-11 19:23:55'), ('123', 'admin', '3', 'StoreProduct', 'Apple iPad 2 16Gb Wi-Fi + 3G', '2014-02-11 19:23:55'), ('124', 'admin', '3', 'StoreProduct', 'Apple iPad 2 64Gb Wi-Fi + 3G', '2014-02-11 19:23:55'), ('125', 'admin', '3', 'StoreProduct', 'Samsung Galaxy Tab 7.0 Plus P6200 16GB', '2014-02-11 19:23:55'), ('126', 'admin', '3', 'StoreProduct', 'Acer Iconia Tab A100 8Gb', '2014-02-11 19:23:55'), ('127', 'admin', '3', 'StoreProduct', 'Asus Transformer Pad Prime 201 64Gb', '2014-02-11 19:23:55'), ('128', 'admin', '3', 'StoreProduct', 'Samsung Galaxy Tab 10.1 P7500 16Gb', '2014-02-11 19:23:55'), ('129', 'admin', '1', 'StoreCategory', 'ELEKTA', '2014-02-11 19:24:31'), ('130', 'admin', '2', 'StoreCategory', 'ELEKTA', '2014-02-11 19:25:03'), ('131', 'admin', '1', 'StoreCategory', 'Венецианские штукатурки', '2014-02-11 19:25:12'), ('132', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-02-11 19:25:21'), ('133', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-02-11 19:25:25'), ('134', 'admin', '1', 'StoreCategory', 'ELEKTA', '2014-02-11 19:25:35'), ('135', 'admin', '2', 'StoreCategory', 'ELEKTA', '2014-02-11 19:25:39'), ('136', 'admin', '1', 'StoreCategory', 'Декоративные штукатурки', '2014-02-11 19:25:53'), ('137', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-02-11 19:26:03'), ('138', 'admin', '1', 'StoreCategory', 'VIERO', '2014-02-11 19:26:12'), ('139', 'admin', '1', 'StoreCategory', 'ELEKTA', '2014-02-11 19:26:19'), ('140', 'admin', '1', 'StoreCategory', 'SAIF', '2014-02-11 19:26:26'), ('141', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-02-11 19:26:37'), ('142', 'admin', '2', 'StoreCategory', 'VIERO', '2014-02-11 19:26:39'), ('143', 'admin', '2', 'StoreCategory', 'ELEKTA', '2014-02-11 19:26:40'), ('144', 'admin', '2', 'StoreCategory', 'SAIF', '2014-02-11 19:26:43'), ('145', 'admin', '1', 'StoreCategory', 'Краски на водной основе', '2014-02-11 19:26:49'), ('146', 'admin', '1', 'StoreCategory', 'SAIF', '2014-02-11 19:26:57'), ('147', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-02-11 19:27:03'), ('148', 'admin', '1', 'StoreCategory', 'Подготовка поверхности', '2014-02-11 19:27:11'), ('149', 'admin', '2', 'StoreCategory', 'SAIF', '2014-02-11 19:27:16'), ('150', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-02-11 19:27:17'), ('151', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-02-11 19:27:24'), ('152', 'admin', '1', 'StoreCategory', 'SAIF', '2014-02-11 19:27:30'), ('153', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-02-11 19:27:32'), ('154', 'admin', '2', 'StoreCategory', 'SAIF', '2014-02-11 19:27:34'), ('155', 'admin', '3', 'StoreProduct', 'Lenovo B570', '2014-02-11 19:27:45'), ('156', 'admin', '3', 'StoreProduct', 'Lenovo G570', '2014-02-11 19:27:45'), ('157', 'admin', '3', 'StoreProduct', 'ASUS K53U', '2014-02-11 19:27:45'), ('158', 'admin', '3', 'StoreProduct', 'ASUS X54C', '2014-02-11 19:27:45'), ('159', 'admin', '3', 'StoreProduct', 'DELL INSPIRON N5050', '2014-02-11 19:27:45'), ('160', 'admin', '3', 'StoreProduct', 'Fujitsu LIFEBOOK AH531', '2014-02-11 19:27:45'), ('161', 'admin', '3', 'StoreCategory', 'Бюджетный', '2014-02-11 19:27:45'), ('162', 'admin', '3', 'StoreProduct', 'HP EliteBook 8560w', '2014-02-11 19:27:45'), ('163', 'admin', '3', 'StoreProduct', 'DELL ALIENWARE M17x', '2014-02-11 19:27:45'), ('164', 'admin', '3', 'StoreProduct', 'Sony VAIO VPC-F13S8R', '2014-02-11 19:27:45'), ('165', 'admin', '3', 'StoreProduct', 'Acer ASPIRE 5943G-7748G75TWiss', '2014-02-11 19:27:45'), ('166', 'admin', '3', 'StoreCategory', 'Игровой', '2014-02-11 19:27:45'), ('167', 'admin', '3', 'StoreCategory', 'Ноутбуки', '2014-02-11 19:27:45'), ('168', 'admin', '3', 'StoreProduct', 'Logitech X-530', '2014-02-11 19:27:50'), ('169', 'admin', '3', 'StoreProduct', 'Microlab M-860', '2014-02-11 19:27:50'), ('170', 'admin', '3', 'StoreProduct', 'Edifier M3700', '2014-02-11 19:27:50'), ('171', 'admin', '3', 'StoreProduct', 'Logitech Z-313', '2014-02-11 19:27:50'), ('172', 'admin', '3', 'StoreCategory', 'Компьютерная акустика', '2014-02-11 19:27:50'), ('173', 'admin', '3', 'StoreCategory', 'Планшеты', '2014-02-11 19:27:50'), ('174', 'admin', '3', 'StoreCategory', 'Компьютеры', '2014-02-11 19:27:50'), ('175', 'admin', '3', 'StoreCategory', 'Мониторы', '2014-02-11 19:27:55'), ('176', 'admin', '3', 'StoreCategory', 'Телефоны', '2014-02-11 19:27:59'), ('177', 'admin', '3', 'StoreCategory', 'Краски', '2014-02-11 19:28:02'), ('178', 'admin', '3', 'StoreProductType', 'Простой продукт', '2014-02-11 19:29:56'), ('179', 'admin', '3', 'StoreProductType', 'Ноутбук', '2014-02-11 19:29:56'), ('180', 'admin', '3', 'StoreProductType', 'Акустика', '2014-02-11 19:29:56'), ('181', 'admin', '3', 'StoreProductType', 'Монитор', '2014-02-11 19:29:56'), ('182', 'admin', '3', 'StoreProductType', 'Телефон', '2014-02-11 19:29:56'), ('183', 'admin', '3', 'StoreProductType', 'Планшет', '2014-02-11 19:29:56'), ('184', 'admin', '1', 'StoreProductType', 'Общий тип', '2014-02-11 19:34:05'), ('185', 'admin', '1', 'StoreProduct', 'ELISE', '2014-02-11 19:35:53'), ('186', 'admin', '2', 'StoreProduct', 'ELISE', '2014-02-11 19:37:04'), ('187', 'admin', '2', 'StoreCurrency', 'Рубли', '2014-02-11 19:38:00'), ('188', 'admin', '2', 'StoreProduct', 'ELISE', '2014-02-11 19:38:19'), ('189', 'admin', '1', 'StoreProduct', 'NAXOS', '2014-02-11 19:40:32'), ('190', 'admin', '1', 'StoreProduct', 'NAXOS (копия)', '2014-02-11 19:41:26'), ('191', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-02-11 19:42:56'), ('192', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-02-11 19:43:04'), ('193', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-02-11 20:05:25'), ('194', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-02-11 20:07:07'), ('195', 'admin', '1', 'StoreProduct', 'DIORA gold (копия)', '2014-02-11 20:07:32'), ('196', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-02-11 20:08:10'), ('197', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-02-11 20:08:40'), ('198', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-02-11 20:08:47'), ('199', 'admin', '3', 'Page', 'Google презентовал свои очки дополненной реальности‎', '2014-02-11 20:09:51'), ('200', 'admin', '3', 'Page', 'За 8,5 месяцев Android 4.0 попал на 11% устройств', '2014-02-11 20:09:51'), ('201', 'admin', '3', 'Page', 'Samsung пытается избежать запрета на Galaxy Nexus', '2014-02-11 20:09:51'), ('202', 'admin', '1', 'PageCategory', 'Текстовая страница', '2014-02-11 20:10:17'), ('203', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-11 20:10:17'), ('204', 'admin', '1', 'Page', 'О компании', '2014-02-11 20:10:34'), ('205', 'admin', '2', 'Page', 'О компании', '2014-02-11 20:23:53'), ('206', 'admin', '2', 'Page', 'О компании', '2014-02-11 20:28:59'), ('207', 'admin', '2', 'Page', 'О компании', '2014-02-11 20:29:35'), ('208', 'admin', '2', 'Page', 'О компании', '2014-02-11 20:29:58'), ('209', 'admin', '2', 'Page', 'О компании', '2014-02-11 20:32:36'), ('210', 'admin', '2', 'Page', 'О компании', '2014-02-11 20:39:55'), ('211', 'admin', '1', 'StoreManufacturer', 'sdfdf', '2014-02-11 20:42:47'), ('212', 'admin', '2', 'StoreManufacturer', 'MaxMeyer', '2014-02-11 20:45:12'), ('213', 'admin', '1', 'StoreManufacturer', 'BALDINI', '2014-02-11 20:45:30'), ('214', 'admin', '1', 'StoreManufacturer', 'SAIF', '2014-02-11 20:45:43'), ('215', 'admin', '1', 'StoreManufacturer', 'ELEKTA', '2014-02-11 20:45:53'), ('216', 'admin', '1', 'StoreManufacturer', 'VIERO', '2014-02-11 20:46:09'), ('217', 'admin', '2', 'StoreManufacturer', 'MaxMeyer', '2014-02-11 20:47:46'), ('218', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 20:54:48'), ('219', 'admin', '2', 'StoreManufacturer', 'ELEKTA', '2014-02-11 20:58:47'), ('220', 'admin', '2', 'StoreManufacturer', 'VIERO', '2014-02-11 21:04:43'), ('221', 'admin', '2', 'StoreManufacturer', 'VIERO', '2014-02-11 21:06:42'), ('222', 'admin', '2', 'StoreManufacturer', 'VIERO', '2014-02-11 21:09:22'), ('223', 'admin', '2', 'StoreManufacturer', 'VIERO', '2014-02-11 21:10:40'), ('224', 'admin', '2', 'StoreManufacturer', 'SAIF', '2014-02-11 21:11:27'), ('225', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:23:53'), ('226', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:28:41'), ('227', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:29:41'), ('228', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:30:40'), ('229', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:31:35'), ('230', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:34:13'), ('231', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:35:23'), ('232', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-11 21:36:23'), ('233', 'admin', '2', 'StoreManufacturer', 'MaxMeyer', '2014-02-11 21:54:31'), ('234', 'admin', '2', 'StoreManufacturer', 'SAIF', '2014-02-11 22:00:22'), ('235', 'admin', '2', 'StoreManufacturer', 'ELEKTA', '2014-02-11 22:36:57'), ('236', 'admin', '2', 'StoreManufacturer', 'ELEKTA', '2014-02-11 22:39:25'), ('237', 'admin', '3', 'Order', '3', '2014-02-14 22:16:54'), ('238', 'admin', '3', 'Order', '2', '2014-02-14 22:16:58'), ('239', 'admin', '3', 'Comment', 'ыаыаываы в ыва ыв ыа', '2014-02-14 22:17:03'), ('240', 'admin', '3', 'Comment', 'авпвап', '2014-02-14 22:17:06'), ('241', 'admin', '2', 'StorePaymentMethod', 'WebMoney', '2014-02-17 20:46:58'), ('242', 'admin', '2', 'StorePaymentMethod', 'Robokassa', '2014-02-17 20:47:08'), ('243', 'admin', '2', 'StorePaymentMethod', 'Qiwi', '2014-02-17 20:47:28'), ('244', 'admin', '2', 'StorePaymentMethod', 'Qiwi', '2014-02-17 20:47:38'), ('245', 'admin', '2', 'StorePaymentMethod', 'Qiwi', '2014-02-17 20:48:08'), ('246', 'admin', '2', 'StorePaymentMethod', 'Qiwi', '2014-02-17 20:49:49'), ('247', 'admin', '2', 'Comment', 'wefw', '2014-02-19 12:36:11'), ('248', 'admin', '2', 'Comment', 'dsdf', '2014-02-19 21:35:57'), ('249', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-02-20 09:49:19'), ('250', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-02-20 09:58:26'), ('251', 'admin', '1', 'Page', 'Baldini', '2014-02-20 10:44:28'), ('252', 'admin', '2', 'StoreManufacturer', 'BALDINI', '2014-02-20 10:46:21'), ('253', 'admin', '1', 'StoreManufacturer', 'Elekta', '2014-02-20 10:46:55'), ('254', 'admin', '1', 'StoreManufacturer', 'Elekta', '2014-02-20 10:47:09'), ('255', 'admin', '3', 'StoreManufacturer', 'Elekta', '2014-02-20 10:47:28'), ('256', 'admin', '3', 'StoreManufacturer', 'Elekta', '2014-02-20 10:47:28'), ('257', 'admin', '1', 'Page', 'Elekta', '2014-02-20 10:47:53'), ('258', 'admin', '1', 'Page', 'Maxmeyer', '2014-02-20 10:48:22'), ('259', 'admin', '1', 'Page', 'Saif', '2014-02-20 10:48:50'), ('260', 'admin', '1', 'Page', 'Viero', '2014-02-20 10:49:21'), ('261', 'admin', '1', 'PageCategory', 'Блог', '2014-02-20 18:13:55'), ('262', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 18:13:55'), ('263', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 18:13:55'), ('264', 'admin', '1', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 18:22:50'), ('265', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 18:26:17'), ('266', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 18:28:06'), ('267', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 18:28:44'), ('268', 'admin', '1', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 18:40:10'), ('269', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 18:40:10'), ('270', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 18:40:10'), ('271', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 18:40:10'), ('272', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 20:14:32'), ('273', 'admin', '1', 'PageCategory', 'Декоративная краска', '2014-02-20 21:19:19'), ('274', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 21:19:19'), ('275', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 21:19:19'), ('276', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 21:19:19'), ('277', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-02-20 21:19:19'), ('278', 'admin', '1', 'PageCategory', 'Дизайн интерьера', '2014-02-20 21:19:48'), ('279', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 21:19:48'), ('280', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 21:19:48'), ('281', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 21:19:48'), ('282', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-02-20 21:19:48'), ('283', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-02-20 21:19:48'), ('284', 'admin', '1', 'PageCategory', 'Дизайн стен', '2014-02-20 21:20:01'), ('285', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 21:20:01'), ('286', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 21:20:01'), ('287', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 21:20:01'), ('288', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-02-20 21:20:01'), ('289', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-02-20 21:20:01'), ('290', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-02-20 21:20:01'), ('291', 'admin', '1', 'PageCategory', 'Новости', '2014-02-20 21:20:18'), ('292', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 21:20:18'), ('293', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 21:20:18'), ('294', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 21:20:18'), ('295', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-02-20 21:20:18'), ('296', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-02-20 21:20:18'), ('297', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-02-20 21:20:18'), ('298', 'admin', '2', 'PageCategory', 'Новости', '2014-02-20 21:20:18'), ('299', 'admin', '2', 'PageCategory', 'Новости', '2014-02-20 21:20:26'), ('300', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-02-20 21:20:26'), ('301', 'admin', '2', 'PageCategory', 'Блог', '2014-02-20 21:20:26'), ('302', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-02-20 21:20:26'), ('303', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-02-20 21:20:26'), ('304', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-02-20 21:20:26'), ('305', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-02-20 21:20:26'), ('306', 'admin', '2', 'PageCategory', 'Новости', '2014-02-20 21:20:26'), ('307', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 21:24:38'), ('308', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 21:25:55'), ('309', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 21:27:03'), ('310', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-02-20 21:27:24'), ('311', 'admin', '1', 'Page', 'Оригинальный дизайн стен', '2014-02-20 22:19:49'), ('312', 'admin', '2', 'Page', 'Оригинальный дизайн стен', '2014-02-20 22:20:43'), ('313', 'admin', '1', 'SystemModules', 'news', '2014-02-20 22:48:46'), ('314', 'admin', '3', 'SSystemLanguage', 'English', '2014-02-20 23:38:50'), ('315', 'admin', '1', 'SSystemLanguage', 'Английский', '2014-02-21 00:06:52'), ('316', 'admin', '1', 'StoreAttribute', 'dfdfdf', '2014-02-28 14:08:17'), ('317', 'admin', '2', 'StoreAttribute', 'dfdfdf', '2014-02-28 14:08:44'), ('318', 'admin', '2', 'StoreProductType', 'Общий тип', '2014-02-28 14:10:18'), ('319', 'admin', '1', 'StoreAttribute', 'sdsdsd', '2014-02-28 14:10:48'), ('320', 'admin', '2', 'StoreProductType', 'Общий тип', '2014-02-28 14:11:03'), ('321', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-02-28 14:11:24'), ('322', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-02-28 14:13:31'), ('323', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-17 08:54:04'), ('324', 'admin', '2', 'StoreProduct', 'NAXOS gold', '2014-03-17 08:56:34'), ('325', 'admin', '1', 'StoreProduct', 'NAXOS gold (копия)', '2014-03-17 08:56:42'), ('326', 'admin', '2', 'StoreProduct', 'NAXOS silver', '2014-03-17 08:57:37'), ('327', 'admin', '2', 'StoreProduct', 'NAXOS gold', '2014-03-17 08:58:03'), ('328', 'admin', '3', 'StoreAttribute', 'processor_manufacturer', '2014-03-17 09:00:20'), ('329', 'admin', '3', 'StoreAttribute', 'sound_type', '2014-03-17 09:00:20'), ('330', 'admin', '3', 'StoreAttribute', 'tablet_screen_size', '2014-03-17 09:00:20'), ('331', 'admin', '3', 'StoreAttribute', 'freq', '2014-03-17 09:00:20'), ('332', 'admin', '3', 'StoreAttribute', 'monitor_diagonal', '2014-03-17 09:00:20'), ('333', 'admin', '3', 'StoreAttribute', 'memmory_size', '2014-03-17 09:00:20'), ('334', 'admin', '3', 'StoreAttribute', 'memmory', '2014-03-17 09:00:20'), ('335', 'admin', '3', 'StoreAttribute', 'monitor_dimension', '2014-03-17 09:00:20'), ('336', 'admin', '3', 'StoreAttribute', 'weight', '2014-03-17 09:00:20'), ('337', 'admin', '3', 'StoreAttribute', 'memmory_type', '2014-03-17 09:00:20'), ('338', 'admin', '3', 'StoreAttribute', 'view_angle', '2014-03-17 09:00:20'), ('339', 'admin', '3', 'StoreAttribute', 'screen', '2014-03-17 09:00:32'), ('340', 'admin', '3', 'StoreAttribute', 'screen_dimension', '2014-03-17 09:00:43'), ('341', 'admin', '3', 'StoreAttribute', 'rms_power', '2014-03-17 09:00:43'), ('342', 'admin', '3', 'StoreAttribute', 'corpus_material', '2014-03-17 09:00:43'), ('343', 'admin', '3', 'StoreAttribute', 'phone_platform', '2014-03-17 09:00:43'), ('344', 'admin', '3', 'StoreAttribute', 'phone_weight', '2014-03-17 09:00:43'), ('345', 'admin', '3', 'StoreAttribute', 'phone_display', '2014-03-17 09:00:43'), ('346', 'admin', '3', 'StoreAttribute', 'phone_camera', '2014-03-17 09:00:43'), ('347', 'admin', '2', 'StoreProductType', 'Общий тип', '2014-03-17 09:01:28'), ('348', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-17 09:04:17'), ('349', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-17 09:04:17'), ('350', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-17 09:04:17'), ('351', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-17 09:04:17'), ('352', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-17 09:04:17'), ('353', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-17 09:04:17'), ('354', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-17 09:04:17'), ('355', 'admin', '3', 'StoreAttribute', 'dfdfdf', '2014-03-17 09:04:29'), ('356', 'admin', '3', 'StoreAttribute', 'sdsdsd', '2014-03-17 09:04:29'), ('357', 'admin', '1', 'StoreAttribute', 'fasovka', '2014-03-17 09:05:19'), ('358', 'admin', '1', 'StoreAttribute', 'rashod', '2014-03-17 09:06:21'), ('359', 'admin', '2', 'StoreProductType', 'Общий тип', '2014-03-17 09:06:53'), ('360', 'admin', '1', 'StoreProduct', 'Product Name', '2014-03-17 09:13:45'), ('361', 'admin', '3', 'StoreProduct', 'Product Name', '2014-03-17 09:14:01'), ('362', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-17 09:20:19'), ('363', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-17 09:20:59'), ('364', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-17 09:21:10'), ('365', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-17 09:21:50'), ('366', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-17 09:27:39'), ('367', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-17 09:31:10'), ('368', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-17 09:56:29'), ('369', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-20 09:37:46'), ('370', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:37:46'), ('371', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:37:46'), ('372', 'admin', '1', 'StoreAttribute', '', '2014-03-20 09:37:46'), ('373', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:37:46'), ('374', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:37:46'), ('375', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-20 09:37:46'), ('376', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:37:46'), ('377', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:38:56'), ('378', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:38:56'), ('379', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:38:56'), ('380', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 09:38:56'), ('381', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:38:56'), ('382', 'admin', '3', 'StoreAttribute', '', '2014-03-20 09:39:10'), ('383', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 09:39:23'), ('384', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:39:23'), ('385', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:39:23'), ('386', 'admin', '1', 'StoreAttribute', '', '2014-03-20 09:39:23'), ('387', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:39:23'), ('388', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:39:23'), ('389', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-20 09:39:23'), ('390', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:39:23'), ('391', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 09:40:13'), ('392', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:40:13'), ('393', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:40:13'), ('394', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:13'), ('395', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:13'), ('396', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:13'), ('397', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:13'), ('398', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-20 09:40:19'), ('399', 'admin', '2', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:40:19'), ('400', 'admin', '2', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:40:19'), ('401', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:20'), ('402', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:20'), ('403', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:20'), ('404', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:20'), ('405', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 09:40:45'), ('406', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:45'), ('407', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:45'), ('408', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:40:45'), ('409', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 09:40:45'), ('410', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:40:45'), ('411', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:45'), ('412', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:45'), ('413', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:40:45'), ('414', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:40:45'), ('415', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:40:45'), ('416', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:40:45'), ('417', 'admin', '3', 'StoreAttribute', '', '2014-03-20 09:40:54'), ('418', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 09:46:54'), ('419', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:46:55'), ('420', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:46:56'), ('421', 'admin', '1', 'StoreAttribute', '', '2014-03-20 09:46:56'), ('422', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:46:56'), ('423', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:46:57'), ('424', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:46:58'), ('425', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:47:00'), ('426', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:47:27'), ('427', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:49:52'), ('428', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:52:33'), ('429', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:52:33'), ('430', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:52:33'), ('431', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:52:33'), ('432', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:52:33'), ('433', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:52:33'), ('434', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 09:52:33'), ('435', 'admin', '3', 'StoreAttribute', '', '2014-03-20 09:52:47'), ('436', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 09:52:58'), ('437', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:52:58'), ('438', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:52:58'), ('439', 'admin', '1', 'StoreAttribute', '', '2014-03-20 09:52:58'), ('440', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:52:58'), ('441', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:52:58'), ('442', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 09:52:58'), ('443', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:52:58'), ('444', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:53:33'), ('445', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:53:33'), ('446', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 09:53:33'), ('447', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 09:53:33'), ('448', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 09:53:33'), ('449', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 09:53:33'), ('450', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:53:56'), ('451', 'admin', '3', 'StoreAttribute', '', '2014-03-20 09:54:08'), ('452', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:54:21'), ('453', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:55:11'), ('454', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:55:31'), ('455', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:57:39'), ('456', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:57:53'), ('457', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:58:27'), ('458', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:58:35'), ('459', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:58:52'), ('460', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 09:59:12'), ('461', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 09:59:23'), ('462', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:59:35'), ('463', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 09:59:52'), ('464', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-20 10:00:03'), ('465', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 10:00:16'), ('466', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 10:01:11'), ('467', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 10:24:01'), ('468', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 10:24:16'), ('469', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 10:24:16'), ('470', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 10:24:16'), ('471', 'admin', '1', 'StoreAttribute', '', '2014-03-20 10:24:16'), ('472', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 10:24:16'), ('473', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 10:24:16'), ('474', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 10:24:17'), ('475', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 10:24:17'), ('476', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 10:25:42'), ('477', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 10:25:42'), ('478', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 10:25:42'), ('479', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 10:25:42'), ('480', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 10:25:42'), ('481', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 10:25:42'), ('482', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 10:25:42'), ('483', 'admin', '3', 'StoreAttribute', '', '2014-03-20 10:26:00'), ('484', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 10:26:12'), ('485', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 10:26:12'), ('486', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 10:26:12'), ('487', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 10:26:12'), ('488', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 10:26:12'), ('489', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 10:26:12'), ('490', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 10:26:12'), ('491', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 13:11:34'), ('492', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-20 13:11:35'), ('493', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-20 13:11:35'), ('494', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 13:11:35'), ('495', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-20 13:11:35'), ('496', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-20 13:11:35'), ('497', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-20 13:11:35'), ('498', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-20 13:11:43'), ('499', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-20 13:11:43'), ('500', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-20 13:11:43'), ('501', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 13:11:43'), ('502', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 13:11:43'), ('503', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-20 13:11:43'), ('504', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-20 13:11:44'), ('505', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-20 13:11:44'), ('506', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-20 13:11:44'), ('507', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-20 13:11:44'), ('508', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-20 13:11:44'), ('509', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-20 13:11:44'), ('510', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-20 13:11:44'), ('511', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-20 13:11:44'), ('512', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-20 13:11:44'), ('513', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-20 13:11:44'), ('514', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-20 13:11:44'), ('515', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-20 13:11:44'), ('516', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-20 13:11:44'), ('517', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-20 13:11:44'), ('518', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-20 13:11:44'), ('519', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-20 13:11:44'), ('520', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-20 13:11:44'), ('521', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-20 13:11:44'), ('522', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-20 13:11:44'), ('523', 'admin', '1', 'StoreProduct', 'TERRE MEDICEE', '2014-03-20 13:11:44'), ('524', 'admin', '1', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-20 13:11:44'), ('525', 'admin', '1', 'StoreProduct', 'BISANTYUM argento', '2014-03-20 13:11:44'), ('526', 'admin', '1', 'StoreProduct', 'BISANTYUM oro', '2014-03-20 13:11:45'), ('527', 'admin', '1', 'StoreProduct', 'ARETINО', '2014-03-20 13:11:45'), ('528', 'admin', '1', 'StoreProduct', 'NUVOLE М-04', '2014-03-20 13:11:45'), ('529', 'admin', '1', 'StoreProduct', 'NUVOLE М-05', '2014-03-20 13:11:45'), ('530', 'admin', '1', 'StoreProduct', 'NUVOLE М-08', '2014-03-20 13:11:45'), ('531', 'admin', '1', 'StoreProduct', 'NUVOLE Incanto', '2014-03-20 13:11:45'), ('532', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-20 13:11:45'), ('533', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-20 13:11:45'), ('534', 'admin', '1', 'StoreProduct', 'POMPEIANO', '2014-03-20 13:11:45'), ('535', 'admin', '1', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-20 13:11:45'), ('536', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-20 13:11:45'), ('537', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-20 13:11:45'), ('538', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-20 13:11:45'), ('539', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-20 13:11:45'), ('540', 'admin', '1', 'StoreProduct', 'R11 SILVER', '2014-03-20 13:11:45'), ('541', 'admin', '1', 'StoreProduct', 'R11 GOLD', '2014-03-20 13:11:45'), ('542', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-20 13:11:45'), ('543', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-20 13:11:45'), ('544', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-20 13:11:45'), ('545', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-20 13:11:45'), ('546', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-20 13:11:45'), ('547', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-20 13:11:45'), ('548', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-20 13:11:46'), ('549', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-20 13:11:46'), ('550', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-20 13:11:46'), ('551', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-20 13:11:46'), ('552', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-20 13:11:46'), ('553', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-20 13:11:46'), ('554', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-20 13:11:46'), ('555', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-20 13:11:46'), ('556', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-20 13:11:46'), ('557', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-20 13:11:46'), ('558', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-20 13:11:46'), ('559', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-20 13:11:46'), ('560', 'admin', '3', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 13:17:56'), ('561', 'admin', '3', 'StoreProduct', 'ARETINО', '2014-03-23 13:17:56'), ('562', 'admin', '3', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 13:17:56'), ('563', 'admin', '3', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 13:17:56'), ('564', 'admin', '3', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 13:17:56'), ('565', 'admin', '3', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 13:17:56'), ('566', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:17:56'), ('567', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:17:56'), ('568', 'admin', '3', 'StoreProduct', 'POMPEIANO', '2014-03-23 13:17:56'), ('569', 'admin', '3', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 13:17:56'), ('570', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 13:17:56'), ('571', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 13:17:56'), ('572', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:17:56'), ('573', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:17:56'), ('574', 'admin', '3', 'StoreProduct', 'R11 SILVER', '2014-03-23 13:17:56'), ('575', 'admin', '3', 'StoreProduct', 'R11 GOLD', '2014-03-23 13:17:56'), ('576', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:17:56'), ('577', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 13:17:56'), ('578', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:17:56'), ('579', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:17:56'), ('580', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 13:17:56'), ('581', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 13:17:56'), ('582', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 13:17:56'), ('583', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 13:17:56'), ('584', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 13:17:56'), ('585', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 13:17:56'), ('586', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 13:17:56'), ('587', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 13:17:56'), ('588', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 13:17:56'), ('589', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 13:17:56'), ('590', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 13:18:03'), ('591', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 13:18:03'), ('592', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 13:18:03'), ('593', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 13:18:03'), ('594', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 13:18:03'), ('595', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 13:18:03'), ('596', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 13:18:04'), ('597', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 13:18:04'), ('598', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 13:18:04'), ('599', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-23 13:18:04'), ('600', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 13:18:04'), ('601', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:18:04'), ('602', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:18:04'), ('603', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:18:04'), ('604', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:18:04'), ('605', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:18:04'), ('606', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:18:04'), ('607', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:18:04'), ('608', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:18:04'), ('609', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 13:18:04'), ('610', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 13:18:04'), ('611', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:18:04'), ('612', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:18:04'), ('613', 'admin', '3', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 13:18:04'), ('614', 'admin', '3', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 13:18:04'), ('615', 'admin', '3', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 13:18:04'), ('616', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 13:18:04'), ('617', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:18:04'), ('618', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 13:18:04'), ('619', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 13:18:04'), ('620', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-23 13:18:10'), ('621', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-23 13:18:10'), ('622', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-23 13:18:32'), ('623', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-23 13:18:32'), ('624', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-23 13:18:32'), ('625', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 13:18:32'), ('626', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 13:18:32'), ('627', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 13:18:32'), ('628', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 13:18:32'), ('629', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 13:18:32'), ('630', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 13:18:32'), ('631', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 13:18:32'), ('632', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 13:18:32'), ('633', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 13:18:32'), ('634', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 13:18:32'), ('635', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:18:32'), ('636', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:18:32'), ('637', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:18:33'), ('638', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:18:33'), ('639', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:18:33'), ('640', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:18:33'), ('641', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:18:33'), ('642', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:18:33'), ('643', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 13:18:33'), ('644', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 13:18:33'), ('645', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:18:33'), ('646', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:18:33'), ('647', 'admin', '1', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 13:18:33'), ('648', 'admin', '1', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 13:18:33'), ('649', 'admin', '1', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 13:18:33'), ('650', 'admin', '1', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 13:18:33'), ('651', 'admin', '1', 'StoreProduct', 'ARETINО', '2014-03-23 13:18:33'), ('652', 'admin', '1', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 13:18:33'), ('653', 'admin', '1', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 13:18:33'), ('654', 'admin', '1', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 13:18:33'), ('655', 'admin', '1', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 13:18:33'), ('656', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:18:33'), ('657', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:18:33'), ('658', 'admin', '1', 'StoreProduct', 'POMPEIANO', '2014-03-23 13:18:33'), ('659', 'admin', '1', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 13:18:33'), ('660', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 13:18:33'), ('661', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 13:18:33'), ('662', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:18:33'), ('663', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:18:33'), ('664', 'admin', '1', 'StoreProduct', 'R11 SILVER', '2014-03-23 13:18:33'), ('665', 'admin', '1', 'StoreProduct', 'R11 GOLD', '2014-03-23 13:18:33'), ('666', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:18:34'), ('667', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 13:18:34'), ('668', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 13:18:34'), ('669', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:18:34'), ('670', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 13:18:34'), ('671', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 13:18:34'), ('672', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:18:34'), ('673', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:18:34'), ('674', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 13:18:34'), ('675', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 13:18:34'), ('676', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 13:18:34'), ('677', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 13:18:34'), ('678', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 13:18:34'), ('679', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 13:18:34'), ('680', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 13:18:34'), ('681', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 13:18:34'), ('682', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 13:18:34'), ('683', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 13:18:34'), ('684', 'admin', '1', 'StoreCategory', 'BALDIN', '2014-03-23 13:18:34'), ('685', 'admin', '1', 'StoreProduct', 'SPECCHIO', '2014-03-23 13:18:34'), ('686', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:18:34'), ('687', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:18:34'), ('688', 'admin', '1', 'StoreProduct', 'OTHELLO whit', '2014-03-23 13:18:34'), ('689', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 13:18:34'), ('690', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 13:18:34'), ('691', 'admin', '1', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 13:18:34'), ('692', 'admin', '1', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 13:18:34'), ('693', 'admin', '1', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 13:18:35'), ('694', 'admin', '1', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 13:18:35'), ('695', 'admin', '1', 'StoreProduct', 'KOLOSSOS', '2014-03-23 13:18:35'), ('696', 'admin', '1', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 13:18:35'), ('697', 'admin', '1', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 13:18:35'), ('698', 'admin', '1', 'StoreProduct', 'VEGA BR', '2014-03-23 13:18:35'), ('699', 'admin', '1', 'StoreProduct', 'ORION', '2014-03-23 13:18:35'), ('700', 'admin', '1', 'StoreProduct', 'VITREX  neutro', '2014-03-23 13:18:35'), ('701', 'admin', '1', 'StoreCategory', 'Ваксы', '2014-03-23 13:18:35'), ('702', 'admin', '1', 'StoreProduct', 'SHINE CERA', '2014-03-23 13:18:35'), ('703', 'admin', '1', 'StoreProduct', 'CERA PETRA', '2014-03-23 13:18:35'), ('704', 'admin', '1', 'StoreProduct', 'UNO TOP', '2014-03-23 13:18:35'), ('705', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:18:35'), ('706', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:18:35'), ('707', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:18:35'), ('708', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:18:35'), ('709', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:18:35'), ('710', 'admin', '1', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 13:18:35'), ('711', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:18:35'), ('712', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:18:35'), ('713', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:18:35'), ('714', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:18:35'), ('715', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:18:35'), ('716', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:18:35'), ('717', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:18:35'), ('718', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:18:36'), ('719', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:18:36'), ('720', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:18:36'), ('721', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:18:36'), ('722', 'admin', '1', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 13:18:36'), ('723', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:18:36'), ('724', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:18:36'), ('725', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:18:36'), ('726', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:18:36'), ('727', 'admin', '3', 'Order', '6', '2014-03-23 13:18:43'), ('728', 'admin', '3', 'Order', '4', '2014-03-23 13:18:43'), ('729', 'admin', '3', 'Order', '5', '2014-03-23 13:18:43'), ('730', 'admin', '3', 'Order', '7', '2014-03-23 13:18:43'), ('731', 'admin', '3', 'Order', '8', '2014-03-23 13:18:43'), ('732', 'admin', '3', 'Order', '9', '2014-03-23 13:18:43'), ('733', 'admin', '3', 'Order', '10', '2014-03-23 13:18:43'), ('734', 'admin', '3', 'Order', '11', '2014-03-23 13:18:43'), ('735', 'admin', '3', 'Order', '12', '2014-03-23 13:18:43'), ('736', 'admin', '3', 'StoreProduct', 'UNO TOP', '2014-03-23 13:20:05'), ('737', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:20:05'), ('738', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:20:05'), ('739', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:20:05'), ('740', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:20:05'), ('741', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:20:05'), ('742', 'admin', '3', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 13:20:05'), ('743', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:20:06'), ('744', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:20:06'), ('745', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:20:06'), ('746', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:06'), ('747', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:06'), ('748', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:06'), ('749', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:06'), ('750', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:20:06'), ('751', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:20:06'), ('752', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:20:13'), ('753', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 13:20:13'), ('754', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 13:20:13'), ('755', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 13:20:13'), ('756', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:20:13'), ('757', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 13:20:13'), ('758', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:20:13'), ('759', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 13:20:13'), ('760', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 13:20:13'), ('761', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 13:20:13'), ('762', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 13:20:13'), ('763', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 13:20:13'), ('764', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 13:20:13'), ('765', 'admin', '3', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 13:20:13'), ('766', 'admin', '3', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 13:20:13'), ('767', 'admin', '3', 'StoreProduct', 'KOLOSSOS', '2014-03-23 13:20:13'), ('768', 'admin', '3', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 13:20:13'), ('769', 'admin', '3', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 13:20:13'), ('770', 'admin', '3', 'StoreProduct', 'VEGA BR', '2014-03-23 13:20:13'), ('771', 'admin', '3', 'StoreProduct', 'ORION', '2014-03-23 13:20:13'), ('772', 'admin', '3', 'StoreProduct', 'VITREX  neutro', '2014-03-23 13:20:13'), ('773', 'admin', '3', 'StoreProduct', 'SHINE CERA', '2014-03-23 13:20:13'), ('774', 'admin', '3', 'StoreProduct', 'CERA PETRA', '2014-03-23 13:20:13'), ('775', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:20:13'), ('776', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:20:13'), ('777', 'admin', '3', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 13:20:13'), ('778', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:20:13'), ('779', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:20:13'), ('780', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:20:13'), ('781', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:20:13'), ('782', 'admin', '3', 'StoreProduct', 'R11 GOLD', '2014-03-23 13:20:20'), ('783', 'admin', '3', 'StoreProduct', 'R11 SILVER', '2014-03-23 13:20:20'), ('784', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:20:20'), ('785', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:20:20'), ('786', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 13:20:20'), ('787', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 13:20:20'), ('788', 'admin', '3', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 13:20:20'), ('789', 'admin', '3', 'StoreProduct', 'POMPEIANO', '2014-03-23 13:20:20'), ('790', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:20:20'), ('791', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:20:20'), ('792', 'admin', '3', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 13:20:20'), ('793', 'admin', '3', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 13:20:20'), ('794', 'admin', '3', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 13:20:20'), ('795', 'admin', '3', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 13:20:20'), ('796', 'admin', '3', 'StoreProduct', 'ARETINО', '2014-03-23 13:20:20'), ('797', 'admin', '3', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 13:20:20'), ('798', 'admin', '3', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 13:20:20'), ('799', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 13:20:20'), ('800', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 13:20:20'), ('801', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 13:20:20'), ('802', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 13:20:20'), ('803', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:20:20'), ('804', 'admin', '3', 'StoreProduct', 'SPECCHIO', '2014-03-23 13:20:20'), ('805', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:20:20'), ('806', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:20:20'), ('807', 'admin', '3', 'StoreProduct', 'OTHELLO whit', '2014-03-23 13:20:20'), ('808', 'admin', '3', 'StoreProduct', 'PETRA', '2014-03-23 13:20:20'), ('809', 'admin', '3', 'StoreProduct', 'PETRA', '2014-03-23 13:20:21'), ('810', 'admin', '3', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 13:20:21'), ('811', 'admin', '3', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 13:20:21'), ('812', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-23 13:20:30'), ('813', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-23 13:20:30'), ('814', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-23 13:20:30'), ('815', 'admin', '3', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 13:20:30'), ('816', 'admin', '3', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 13:20:30'), ('817', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:20:30'), ('818', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:20:30'), ('819', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:20:30'), ('820', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:20:30'), ('821', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:20:30'), ('822', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 13:20:30'), ('823', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 13:20:30'), ('824', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:20:30'), ('825', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:20:30'), ('826', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 13:20:30'), ('827', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 13:20:30'), ('828', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 13:20:30'), ('829', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 13:20:30'), ('830', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 13:20:30'), ('831', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 13:20:30'), ('832', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 13:20:30'), ('833', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 13:20:30'), ('834', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 13:20:30'), ('835', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 13:20:30'), ('836', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:20:30'), ('837', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:20:30'), ('838', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:20:30'), ('839', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-23 13:20:51'), ('840', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-23 13:20:51'), ('841', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-23 13:20:51'), ('842', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 13:20:51'), ('843', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 13:20:51'), ('844', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 13:20:51'), ('845', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 13:20:51'), ('846', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 13:20:52');
INSERT INTO `ActionLog` VALUES ('847', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 13:20:52'), ('848', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 13:20:52'), ('849', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 13:20:52'), ('850', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 13:20:52'), ('851', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 13:20:52'), ('852', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:20:52'), ('853', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:20:52'), ('854', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:20:52'), ('855', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:20:52'), ('856', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:20:52'), ('857', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:20:52'), ('858', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:20:52'), ('859', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:20:52'), ('860', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 13:20:52'), ('861', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 13:20:52'), ('862', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:20:52'), ('863', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:20:52'), ('864', 'admin', '1', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 13:20:52'), ('865', 'admin', '1', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 13:20:52'), ('866', 'admin', '1', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 13:20:53'), ('867', 'admin', '1', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 13:20:53'), ('868', 'admin', '1', 'StoreProduct', 'ARETINО', '2014-03-23 13:20:53'), ('869', 'admin', '1', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 13:20:53'), ('870', 'admin', '1', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 13:20:53'), ('871', 'admin', '1', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 13:20:53'), ('872', 'admin', '1', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 13:20:53'), ('873', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:20:53'), ('874', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:20:53'), ('875', 'admin', '1', 'StoreProduct', 'POMPEIANO', '2014-03-23 13:20:53'), ('876', 'admin', '1', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 13:20:53'), ('877', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 13:20:53'), ('878', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 13:20:53'), ('879', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:20:53'), ('880', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:20:53'), ('881', 'admin', '1', 'StoreProduct', 'R11 SILVER', '2014-03-23 13:20:53'), ('882', 'admin', '1', 'StoreProduct', 'R11 GOLD', '2014-03-23 13:20:53'), ('883', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:20:53'), ('884', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 13:20:53'), ('885', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 13:20:53'), ('886', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:20:53'), ('887', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 13:20:53'), ('888', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 13:20:53'), ('889', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:20:53'), ('890', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:20:53'), ('891', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 13:20:53'), ('892', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 13:20:54'), ('893', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 13:20:54'), ('894', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 13:20:54'), ('895', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 13:20:54'), ('896', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 13:20:54'), ('897', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 13:20:54'), ('898', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 13:20:54'), ('899', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 13:20:54'), ('900', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 13:20:54'), ('901', 'admin', '1', 'StoreProduct', 'SPECCHIO', '2014-03-23 13:20:54'), ('902', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:20:54'), ('903', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:20:54'), ('904', 'admin', '1', 'StoreProduct', 'OTHELLO whit', '2014-03-23 13:20:54'), ('905', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 13:20:54'), ('906', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 13:20:54'), ('907', 'admin', '1', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 13:20:54'), ('908', 'admin', '1', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 13:20:54'), ('909', 'admin', '1', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 13:20:54'), ('910', 'admin', '1', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 13:20:54'), ('911', 'admin', '1', 'StoreProduct', 'KOLOSSOS', '2014-03-23 13:20:54'), ('912', 'admin', '1', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 13:20:54'), ('913', 'admin', '1', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 13:20:54'), ('914', 'admin', '1', 'StoreProduct', 'VEGA BR', '2014-03-23 13:20:54'), ('915', 'admin', '1', 'StoreProduct', 'ORION', '2014-03-23 13:20:54'), ('916', 'admin', '1', 'StoreProduct', 'VITREX  neutro', '2014-03-23 13:20:54'), ('917', 'admin', '1', 'StoreProduct', 'SHINE CERA', '2014-03-23 13:20:54'), ('918', 'admin', '1', 'StoreProduct', 'CERA PETRA', '2014-03-23 13:20:55'), ('919', 'admin', '1', 'StoreProduct', 'UNO TOP', '2014-03-23 13:20:55'), ('920', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:20:55'), ('921', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:20:55'), ('922', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:20:55'), ('923', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:20:55'), ('924', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:20:55'), ('925', 'admin', '1', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 13:20:55'), ('926', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:20:55'), ('927', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:20:55'), ('928', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:20:55'), ('929', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:55'), ('930', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:55'), ('931', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:55'), ('932', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:20:55'), ('933', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:20:55'), ('934', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:20:55'), ('935', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:20:55'), ('936', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:20:55'), ('937', 'admin', '1', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 13:20:55'), ('938', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:20:55'), ('939', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:20:55'), ('940', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:20:55'), ('941', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:20:55'), ('942', 'admin', '2', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:32:26'), ('943', 'admin', '2', 'StoreProduct', 'ELISE', '2014-03-23 13:36:03'), ('944', 'admin', '2', 'StoreProduct', 'NAXOS gold', '2014-03-23 13:36:05'), ('945', 'admin', '2', 'StoreProduct', 'NAXOS silver', '2014-03-23 13:36:06'), ('946', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-23 13:36:07'), ('947', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-23 13:36:08'), ('948', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-23 13:36:09'), ('949', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-23 13:36:09'), ('950', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-03-23 13:36:10'), ('951', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-03-23 13:36:11'), ('952', 'admin', '2', 'StoreProduct', 'DIORA red', '2014-03-23 13:36:12'), ('953', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-03-23 13:36:13'), ('954', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-03-23 13:36:14'), ('955', 'admin', '2', 'StoreProduct', 'DIORA red', '2014-03-23 13:36:14'), ('956', 'admin', '2', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:36:15'), ('957', 'admin', '2', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:36:16'), ('958', 'admin', '2', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:36:17'), ('959', 'admin', '2', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:36:17'), ('960', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:38:52'), ('961', 'admin', '2', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:38:54'), ('962', 'admin', '2', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:38:54'), ('963', 'admin', '2', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:38:54'), ('964', 'admin', '2', 'StoreProduct', 'VELUR', '2014-03-23 13:38:54'), ('965', 'admin', '2', 'StoreProduct', 'VELUR', '2014-03-23 13:38:54'), ('966', 'admin', '2', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:38:54'), ('967', 'admin', '2', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:38:54'), ('968', 'admin', '2', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 13:38:54'), ('969', 'admin', '2', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 13:38:54'), ('970', 'admin', '2', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 13:38:55'), ('971', 'admin', '2', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 13:38:55'), ('972', 'admin', '2', 'StoreProduct', 'ARETINО', '2014-03-23 13:38:55'), ('973', 'admin', '2', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 13:38:55'), ('974', 'admin', '2', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 13:38:55'), ('975', 'admin', '2', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 13:38:55'), ('976', 'admin', '2', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 13:38:55'), ('977', 'admin', '2', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:38:55'), ('978', 'admin', '2', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:38:55'), ('979', 'admin', '2', 'StoreProduct', 'POMPEIANO', '2014-03-23 13:38:55'), ('980', 'admin', '2', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 13:38:55'), ('981', 'admin', '2', 'StoreProduct', 'TATTO', '2014-03-23 13:38:56'), ('982', 'admin', '2', 'StoreProduct', 'TATTO', '2014-03-23 13:38:56'), ('983', 'admin', '2', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:38:56'), ('984', 'admin', '2', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:38:56'), ('985', 'admin', '2', 'StoreProduct', 'R11 SILVER', '2014-03-23 13:38:56'), ('986', 'admin', '2', 'StoreProduct', 'R11 GOLD', '2014-03-23 13:38:56'), ('987', 'admin', '2', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:38:56'), ('988', 'admin', '2', 'StoreProduct', 'FUSION gold', '2014-03-23 13:38:56'), ('989', 'admin', '2', 'StoreProduct', 'FUSION copper', '2014-03-23 13:38:56'), ('990', 'admin', '2', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:38:56'), ('991', 'admin', '2', 'StoreProduct', 'FUSION gold', '2014-03-23 13:38:56'), ('992', 'admin', '2', 'StoreProduct', 'FUSION copper', '2014-03-23 13:38:56'), ('993', 'admin', '2', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:38:57'), ('994', 'admin', '2', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:38:57'), ('995', 'admin', '2', 'StoreProduct', 'PERLAGE', '2014-03-23 13:38:57'), ('996', 'admin', '2', 'StoreProduct', 'PERLAGE', '2014-03-23 13:38:57'), ('997', 'admin', '2', 'StoreProduct', 'MURINIO', '2014-03-23 13:38:57'), ('998', 'admin', '2', 'StoreProduct', 'MURINIO', '2014-03-23 13:38:57'), ('999', 'admin', '2', 'StoreProduct', 'METAL silver', '2014-03-23 13:38:57'), ('1000', 'admin', '2', 'StoreProduct', 'METAL gold', '2014-03-23 13:38:57'), ('1001', 'admin', '2', 'StoreProduct', 'METAL copper', '2014-03-23 13:38:57'), ('1002', 'admin', '2', 'StoreProduct', 'METAL silver', '2014-03-23 13:38:57'), ('1003', 'admin', '2', 'StoreProduct', 'METAL gold', '2014-03-23 13:38:57'), ('1004', 'admin', '2', 'StoreProduct', 'METAL copper', '2014-03-23 13:38:57'), ('1005', 'admin', '2', 'StoreProduct', 'SPECCHIO', '2014-03-23 13:38:58'), ('1006', 'admin', '2', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:38:58'), ('1007', 'admin', '2', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:38:58'), ('1008', 'admin', '2', 'StoreProduct', 'OTHELLO whit', '2014-03-23 13:38:58'), ('1009', 'admin', '2', 'StoreProduct', 'PETRA', '2014-03-23 13:38:58'), ('1010', 'admin', '2', 'StoreProduct', 'PETRA', '2014-03-23 13:38:58'), ('1011', 'admin', '2', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 13:38:58'), ('1012', 'admin', '2', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 13:38:58'), ('1013', 'admin', '2', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 13:38:58'), ('1014', 'admin', '2', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 13:38:58'), ('1015', 'admin', '2', 'StoreProduct', 'KOLOSSOS', '2014-03-23 13:38:59'), ('1016', 'admin', '2', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 13:38:59'), ('1017', 'admin', '2', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 13:38:59'), ('1018', 'admin', '2', 'StoreProduct', 'VEGA BR', '2014-03-23 13:38:59'), ('1019', 'admin', '2', 'StoreProduct', 'ORION', '2014-03-23 13:38:59'), ('1020', 'admin', '2', 'StoreProduct', 'VITREX  neutro', '2014-03-23 13:38:59'), ('1021', 'admin', '2', 'StoreProduct', 'SHINE CERA', '2014-03-23 13:38:59'), ('1022', 'admin', '2', 'StoreProduct', 'CERA PETRA', '2014-03-23 13:38:59'), ('1023', 'admin', '2', 'StoreProduct', 'UNO TOP', '2014-03-23 13:38:59'), ('1024', 'admin', '2', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:38:59'), ('1025', 'admin', '2', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:39:00'), ('1026', 'admin', '2', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:39:00'), ('1027', 'admin', '2', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:39:00'), ('1028', 'admin', '2', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:39:00'), ('1029', 'admin', '2', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 13:39:00'), ('1030', 'admin', '2', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:39:00'), ('1031', 'admin', '2', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:39:00'), ('1032', 'admin', '2', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:39:00'), ('1033', 'admin', '2', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:39:00'), ('1034', 'admin', '2', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:39:00'), ('1035', 'admin', '2', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:39:00'), ('1036', 'admin', '2', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:39:01'), ('1037', 'admin', '2', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:39:01'), ('1038', 'admin', '2', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:39:01'), ('1039', 'admin', '2', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:39:01'), ('1040', 'admin', '2', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:39:01'), ('1041', 'admin', '2', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 13:39:01'), ('1042', 'admin', '2', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:39:01'), ('1043', 'admin', '2', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:39:01'), ('1044', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:39:01'), ('1045', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:39:01'), ('1046', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:43:20'), ('1047', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:43:20'), ('1048', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 13:43:20'), ('1049', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 13:43:20'), ('1050', 'admin', '3', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 13:43:20'), ('1051', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:43:20'), ('1052', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:43:20'), ('1053', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:43:20'), ('1054', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:43:20'), ('1055', 'admin', '3', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 13:43:20'), ('1056', 'admin', '3', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 13:43:20'), ('1057', 'admin', '3', 'StoreProduct', 'VEGA BR', '2014-03-23 13:43:20'), ('1058', 'admin', '3', 'StoreProduct', 'ORION', '2014-03-23 13:43:20'), ('1059', 'admin', '3', 'StoreProduct', 'VITREX  neutro', '2014-03-23 13:43:20'), ('1060', 'admin', '3', 'StoreProduct', 'SHINE CERA', '2014-03-23 13:43:20'), ('1061', 'admin', '3', 'StoreProduct', 'CERA PETRA', '2014-03-23 13:43:20'), ('1062', 'admin', '3', 'StoreProduct', 'UNO TOP', '2014-03-23 13:43:20'), ('1063', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-03-23 13:43:20'), ('1064', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:43:20'), ('1065', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:43:20'), ('1066', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 13:43:20'), ('1067', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 13:43:20'), ('1068', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:43:20'), ('1069', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 13:43:20'), ('1070', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:43:20'), ('1071', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 13:43:20'), ('1072', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:43:20'), ('1073', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:43:20'), ('1074', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 13:43:20'), ('1075', 'admin', '3', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 13:43:20'), ('1076', 'admin', '3', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 13:43:35'), ('1077', 'admin', '3', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 13:43:35'), ('1078', 'admin', '3', 'StoreProduct', 'KOLOSSOS', '2014-03-23 13:43:35'), ('1079', 'admin', '3', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 13:43:35'), ('1080', 'admin', '3', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 13:43:35'), ('1081', 'admin', '3', 'StoreProduct', 'PETRA', '2014-03-23 13:43:35'), ('1082', 'admin', '3', 'StoreProduct', 'PETRA', '2014-03-23 13:43:35'), ('1083', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:43:35'), ('1084', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 13:43:35'), ('1085', 'admin', '3', 'StoreProduct', 'SPECCHIO', '2014-03-23 13:43:35'), ('1086', 'admin', '3', 'StoreProduct', 'OTHELLO whit', '2014-03-23 13:43:35'), ('1087', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-03-23 13:43:35'), ('1088', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 13:43:35'), ('1089', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 13:43:35'), ('1090', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 13:43:35'), ('1091', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 13:43:35'), ('1092', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 13:43:35'), ('1093', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 13:43:35'), ('1094', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 13:43:35'), ('1095', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 13:43:35'), ('1096', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 13:43:35'), ('1097', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:43:35'), ('1098', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 13:43:35'), ('1099', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 13:43:35'), ('1100', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 13:43:35'), ('1101', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 13:43:35'), ('1102', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:43:35'), ('1103', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 13:43:35'), ('1104', 'admin', '3', 'StoreProduct', 'R11 SILVER', '2014-03-23 13:43:35'), ('1105', 'admin', '3', 'StoreProduct', 'R11 GOLD', '2014-03-23 13:43:35'), ('1106', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 13:43:41'), ('1107', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:43:41'), ('1108', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:43:41'), ('1109', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 13:43:41'), ('1110', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:43:41'), ('1111', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:43:41'), ('1112', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 13:43:41'), ('1113', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 13:43:41'), ('1114', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 13:43:41'), ('1115', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:43:41'), ('1116', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 13:43:41'), ('1117', 'admin', '3', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 13:43:41'), ('1118', 'admin', '3', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 13:43:41'), ('1119', 'admin', '3', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 13:43:41'), ('1120', 'admin', '3', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 13:43:41'), ('1121', 'admin', '3', 'StoreProduct', 'ARETINО', '2014-03-23 13:43:41'), ('1122', 'admin', '3', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 13:43:41'), ('1123', 'admin', '3', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 13:43:41'), ('1124', 'admin', '3', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 13:43:41'), ('1125', 'admin', '3', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 13:43:41'), ('1126', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:43:41'), ('1127', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 13:43:41'), ('1128', 'admin', '3', 'StoreProduct', 'POMPEIANO', '2014-03-23 13:43:42'), ('1129', 'admin', '3', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 13:43:42'), ('1130', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 13:43:42'), ('1131', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:43:42'), ('1132', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 13:43:42'), ('1133', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 13:43:42'), ('1134', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 13:43:42'), ('1135', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:43:42'), ('1136', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-23 13:43:47'), ('1137', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-23 13:43:47'), ('1138', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-23 13:43:47'), ('1139', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 13:43:47'), ('1140', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 13:43:47'), ('1141', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 13:43:47'), ('1142', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 13:43:47'), ('1143', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 13:43:47'), ('1144', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 13:43:47'), ('1145', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 13:43:47'), ('1146', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 13:43:47'), ('1147', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 13:43:47'), ('1148', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 13:43:47'), ('1149', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 13:43:47'), ('1150', 'admin', '1', 'StoreProduct', 'sd', '2014-03-23 13:44:12'), ('1151', 'admin', '2', 'StoreProduct', 'sd', '2014-03-23 13:45:21'), ('1152', 'admin', '3', 'StoreProduct', 'sd', '2014-03-23 13:45:31'), ('1153', 'admin', '1', 'StoreProduct', 'sd', '2014-03-23 13:45:49'), ('1154', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-03-23 13:58:51'), ('1155', 'admin', '2', 'StoreCategory', 'MaxMeyer - декоративные краски', '2014-03-23 14:06:40'), ('1156', 'admin', '2', 'StoreCategory', 'BALDINI - декоративные краски', '2014-03-23 14:06:58'), ('1157', 'admin', '2', 'StoreCategory', 'SAIF - декоративные краски', '2014-03-23 14:07:16'), ('1158', 'admin', '2', 'StoreCategory', 'MaxMeyer - декоративные краски', '2014-03-23 14:07:24'), ('1159', 'admin', '2', 'StoreCategory', 'BALDINI - декоративные краски', '2014-03-23 14:07:29'), ('1160', 'admin', '2', 'StoreCategory', 'SAIF - декоративные краски', '2014-03-23 14:07:38'), ('1161', 'admin', '2', 'StoreCategory', 'ELEKTA - декоративные краски', '2014-03-23 14:07:48'), ('1162', 'admin', '2', 'StoreCategory', 'декоративные краски-MaxMeyer', '2014-03-23 14:08:22'), ('1163', 'admin', '2', 'StoreCategory', 'MaxMeyer - декоративные краски', '2014-03-23 14:08:36'), ('1164', 'admin', '2', 'StoreCategory', 'BALDINI - венецианские штукатурки', '2014-03-23 14:08:58'), ('1165', 'admin', '2', 'StoreCategory', 'BALDINI - венецианские штукатурки', '2014-03-23 14:09:09'), ('1166', 'admin', '1', 'StoreCategory', 'hello', '2014-03-23 14:10:43'), ('1167', 'admin', '2', 'StoreCategory', 'hello', '2014-03-23 14:10:48'), ('1168', 'admin', '2', 'StoreCategory', 'MaxMeyer', '2014-03-23 14:21:39'), ('1169', 'admin', '2', 'StoreCategory', 'BALDINI', '2014-03-23 14:23:33'), ('1170', 'admin', '2', 'StoreCategory', 'hello', '2014-03-23 14:23:54'), ('1171', 'admin', '2', 'StoreCategory', 'hello', '2014-03-23 14:24:19'), ('1172', 'admin', '1', 'StoreCategory', 'hello', '2014-03-23 14:24:25'), ('1173', 'admin', '2', 'StoreCategory', 'hello', '2014-03-23 14:24:33'), ('1174', 'admin', '2', 'StoreCategory', 'hello', '2014-03-23 14:24:48'), ('1175', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-23 14:36:23'), ('1176', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-23 14:36:23'), ('1177', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-23 14:36:23'), ('1178', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 14:36:23'), ('1179', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 14:36:23'), ('1180', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 14:36:23'), ('1181', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 14:36:23'), ('1182', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 14:36:23'), ('1183', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 14:36:23'), ('1184', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 14:36:23'), ('1185', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 14:36:23'), ('1186', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 14:36:23'), ('1187', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 14:36:23'), ('1188', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 14:36:23'), ('1189', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 14:36:23'), ('1190', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 14:36:23'), ('1191', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 14:36:23'), ('1192', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 14:36:23'), ('1193', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 14:36:23'), ('1194', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 14:36:23'), ('1195', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 14:36:23'), ('1196', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 14:36:23'), ('1197', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 14:36:24'), ('1198', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 14:36:24'), ('1199', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 14:36:24'), ('1200', 'admin', '1', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 14:36:24'), ('1201', 'admin', '1', 'StoreCategory', 'SAIF', '2014-03-23 14:36:24'), ('1202', 'admin', '1', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 14:36:24'), ('1203', 'admin', '1', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 14:36:24'), ('1204', 'admin', '1', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 14:36:24'), ('1205', 'admin', '1', 'StoreProduct', 'ARETINО', '2014-03-23 14:36:24'), ('1206', 'admin', '1', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 14:36:24'), ('1207', 'admin', '1', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 14:36:24'), ('1208', 'admin', '1', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 14:36:24'), ('1209', 'admin', '1', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 14:36:24'), ('1210', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 14:36:24'), ('1211', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 14:36:24'), ('1212', 'admin', '1', 'StoreProduct', 'POMPEIANO', '2014-03-23 14:36:24'), ('1213', 'admin', '1', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 14:36:24'), ('1214', 'admin', '1', 'StoreCategory', 'ELEKTA', '2014-03-23 14:36:24'), ('1215', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 14:36:24'), ('1216', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 14:36:24'), ('1217', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 14:36:24'), ('1218', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 14:36:24'), ('1219', 'admin', '1', 'StoreProduct', 'R11 SILVER', '2014-03-23 14:36:24'), ('1220', 'admin', '1', 'StoreProduct', 'R11 GOLD', '2014-03-23 14:36:24'), ('1221', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 14:36:24'), ('1222', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 14:36:25'), ('1223', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 14:36:25'), ('1224', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 14:36:25'), ('1225', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 14:36:25'), ('1226', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 14:36:25'), ('1227', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 14:36:25'), ('1228', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 14:36:25'), ('1229', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 14:36:25'), ('1230', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 14:36:25'), ('1231', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 14:36:25'), ('1232', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 14:36:25'), ('1233', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 14:36:25'), ('1234', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 14:36:25'), ('1235', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 14:36:25'), ('1236', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 14:36:25'), ('1237', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 14:36:25'), ('1238', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 14:36:25'), ('1239', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-03-23 14:36:25'), ('1240', 'admin', '1', 'StoreProduct', 'SPECCHIO', '2014-03-23 14:36:25'), ('1241', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 14:36:25'), ('1242', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 14:36:25'), ('1243', 'admin', '1', 'StoreProduct', 'OTHELLO whit', '2014-03-23 14:36:25'), ('1244', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 14:36:25'), ('1245', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 14:36:25'), ('1246', 'admin', '1', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 14:36:25'), ('1247', 'admin', '1', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 14:36:25'), ('1248', 'admin', '1', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 14:36:26'), ('1249', 'admin', '1', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 14:36:26'), ('1250', 'admin', '1', 'StoreProduct', 'KOLOSSOS', '2014-03-23 14:36:26'), ('1251', 'admin', '1', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 14:36:26'), ('1252', 'admin', '1', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 14:36:26'), ('1253', 'admin', '1', 'StoreProduct', 'VEGA BR', '2014-03-23 14:36:26'), ('1254', 'admin', '1', 'StoreProduct', 'ORION', '2014-03-23 14:36:26'), ('1255', 'admin', '1', 'StoreProduct', 'VITREX  neutro', '2014-03-23 14:36:26'), ('1256', 'admin', '1', 'StoreCategory', 'BALDINI', '2014-03-23 14:36:26'), ('1257', 'admin', '1', 'StoreProduct', 'SHINE CERA', '2014-03-23 14:36:26'), ('1258', 'admin', '1', 'StoreCategory', 'ELEKTA', '2014-03-23 14:36:26'), ('1259', 'admin', '1', 'StoreProduct', 'CERA PETRA', '2014-03-23 14:36:26'), ('1260', 'admin', '1', 'StoreProduct', 'UNO TOP', '2014-03-23 14:36:26'), ('1261', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 14:36:26'), ('1262', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 14:36:26'), ('1263', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 14:36:26'), ('1264', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 14:36:26'), ('1265', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 14:36:26'), ('1266', 'admin', '1', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 14:36:26'), ('1267', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 14:36:26'), ('1268', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 14:36:26'), ('1269', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 14:36:26'), ('1270', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:36:26'), ('1271', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:36:27'), ('1272', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:36:27'), ('1273', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:36:27'), ('1274', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 14:36:27'), ('1275', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 14:36:27'), ('1276', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 14:36:27'), ('1277', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 14:36:27'), ('1278', 'admin', '1', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 14:36:27'), ('1279', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 14:36:27'), ('1280', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 14:36:27'), ('1281', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 14:36:27'), ('1282', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 14:36:27'), ('1283', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 14:37:28'), ('1284', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 14:37:28'), ('1285', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 14:37:28'), ('1286', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 14:37:28'), ('1287', 'admin', '3', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 14:37:28'), ('1288', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 14:37:28'), ('1289', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 14:37:28'), ('1290', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 14:37:28'), ('1291', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-03-23 14:37:28'), ('1292', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 14:37:28'), ('1293', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 14:37:28'), ('1294', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-03-23 14:37:28'), ('1295', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-03-23 14:37:28'), ('1296', 'admin', '3', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 14:37:28'), ('1297', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 14:37:28'), ('1298', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 14:37:28'), ('1299', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 14:37:28'), ('1300', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:37:28'), ('1301', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:37:28'), ('1302', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:37:28'), ('1303', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 14:37:28'), ('1304', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 14:37:28'), ('1305', 'admin', '3', 'StoreProduct', 'UNO TOP', '2014-03-23 14:37:28'), ('1306', 'admin', '3', 'StoreProduct', 'CERA PETRA', '2014-03-23 14:37:28'), ('1307', 'admin', '3', 'StoreProduct', 'SHINE CERA', '2014-03-23 14:37:28'), ('1308', 'admin', '3', 'StoreProduct', 'VITREX  neutro', '2014-03-23 14:37:28'), ('1309', 'admin', '3', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 14:37:28'), ('1310', 'admin', '3', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 14:37:28'), ('1311', 'admin', '3', 'StoreProduct', 'KOLOSSOS', '2014-03-23 14:37:28'), ('1312', 'admin', '3', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 14:37:28'), ('1313', 'admin', '3', 'StoreProduct', 'SPECCHIO', '2014-03-23 14:37:34'), ('1314', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-03-23 14:37:34'), ('1315', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-03-23 14:37:34'), ('1316', 'admin', '3', 'StoreProduct', 'OTHELLO whit', '2014-03-23 14:37:34'), ('1317', 'admin', '3', 'StoreProduct', 'PETRA', '2014-03-23 14:37:34'), ('1318', 'admin', '3', 'StoreProduct', 'PETRA', '2014-03-23 14:37:34'), ('1319', 'admin', '3', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 14:37:34'), ('1320', 'admin', '3', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 14:37:34'), ('1321', 'admin', '3', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 14:37:34'), ('1322', 'admin', '3', 'StoreProduct', 'VEGA BR', '2014-03-23 14:37:34'), ('1323', 'admin', '3', 'StoreProduct', 'ORION', '2014-03-23 14:37:34'), ('1324', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 14:37:34'), ('1325', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 14:37:34'), ('1326', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-03-23 14:37:34'), ('1327', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-03-23 14:37:34'), ('1328', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 14:37:34'), ('1329', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 14:37:35'), ('1330', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 14:37:35'), ('1331', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-03-23 14:37:35'), ('1332', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-03-23 14:37:35'), ('1333', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 14:37:35'), ('1334', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-03-23 14:37:35'), ('1335', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 14:37:35'), ('1336', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-03-23 14:37:35'), ('1337', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 14:37:35'), ('1338', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-03-23 14:37:35'), ('1339', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 14:37:35'), ('1340', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-03-23 14:37:35'), ('1341', 'admin', '3', 'StoreProduct', 'R11 GOLD', '2014-03-23 14:37:35'), ('1342', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-03-23 14:37:35'), ('1343', 'admin', '3', 'StoreProduct', 'R11 SILVER', '2014-03-23 14:37:42'), ('1344', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 14:37:42'), ('1345', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-03-23 14:37:42'), ('1346', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 14:37:42'), ('1347', 'admin', '3', 'StoreProduct', 'TATTO', '2014-03-23 14:37:42'), ('1348', 'admin', '3', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 14:37:42'), ('1349', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 14:37:42'), ('1350', 'admin', '3', 'StoreProduct', 'POMPEIANO', '2014-03-23 14:37:42'), ('1351', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-03-23 14:37:42'), ('1352', 'admin', '3', 'StoreProduct', 'ARETINО', '2014-03-23 14:37:42'), ('1353', 'admin', '3', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 14:37:42'), ('1354', 'admin', '3', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 14:37:42'), ('1355', 'admin', '3', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 14:37:42'), ('1356', 'admin', '3', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 14:37:42'), ('1357', 'admin', '3', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 14:37:42'), ('1358', 'admin', '3', 'StoreProduct', 'ELISE', '2014-03-23 14:37:42'), ('1359', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-03-23 14:37:42'), ('1360', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-03-23 14:37:42'), ('1361', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 14:37:42'), ('1362', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 14:37:42'), ('1363', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-03-23 14:37:42'), ('1364', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-03-23 14:37:42'), ('1365', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 14:37:42'), ('1366', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 14:37:42'), ('1367', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 14:37:42'), ('1368', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 14:37:42'), ('1369', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 14:37:42'), ('1370', 'admin', '3', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 14:37:42'), ('1371', 'admin', '3', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 14:37:42'), ('1372', 'admin', '3', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 14:37:42'), ('1373', 'admin', '3', 'StoreProduct', 'sd', '2014-03-23 14:37:47'), ('1374', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 14:37:47'), ('1375', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-03-23 14:37:47'), ('1376', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-03-23 14:37:47'), ('1377', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-03-23 14:37:47'), ('1378', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 14:37:47'), ('1379', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 14:37:47'), ('1380', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 14:37:47'), ('1381', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-03-23 14:37:47'), ('1382', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 14:37:47'), ('1383', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 14:37:47'), ('1384', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 14:37:47'), ('1385', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 14:37:47'), ('1386', 'admin', '3', 'StoreProduct', 'VELUR', '2014-03-23 14:37:47'), ('1387', 'admin', '3', 'StoreCategory', 'hello', '2014-03-23 14:37:57'), ('1388', 'admin', '3', 'StoreCategory', 'hello', '2014-03-23 14:38:02'), ('1389', 'admin', '3', 'StoreCategory', 'hello2', '2014-03-23 14:38:07'), ('1390', 'admin', '3', 'StoreCategory', 'SAIF - декоративные краски', '2014-03-23 14:38:12'), ('1391', 'admin', '3', 'StoreCategory', 'ELEKTA - декоративные краски', '2014-03-23 14:38:18'), ('1392', 'admin', '3', 'StoreCategory', 'BALDINI - венецианские штукатурки', '2014-03-23 14:39:24'), ('1393', 'admin', '3', 'StoreCategory', 'BALDIN', '2014-03-23 14:39:28'), ('1394', 'admin', '1', 'StoreProduct', 'ELISE', '2014-03-23 15:00:21'), ('1395', 'admin', '1', 'StoreProduct', 'NAXOS gold', '2014-03-23 15:00:21'), ('1396', 'admin', '1', 'StoreProduct', 'NAXOS silver', '2014-03-23 15:00:22'), ('1397', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 15:00:22'), ('1398', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 15:00:22'), ('1399', 'admin', '1', 'StoreProduct', 'EDONI oro', '2014-03-23 15:00:22'), ('1400', 'admin', '1', 'StoreProduct', 'EDONI argento', '2014-03-23 15:00:22'), ('1401', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 15:00:22'), ('1402', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 15:00:22'), ('1403', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 15:00:22'), ('1404', 'admin', '1', 'StoreProduct', 'DIORA gold', '2014-03-23 15:00:22'), ('1405', 'admin', '1', 'StoreProduct', 'DIORA silver', '2014-03-23 15:00:22'), ('1406', 'admin', '1', 'StoreProduct', 'DIORA red', '2014-03-23 15:00:22'), ('1407', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 15:00:22'), ('1408', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 15:00:22'), ('1409', 'admin', '1', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 15:00:22'), ('1410', 'admin', '1', 'StoreProduct', 'SHARDANA silver', '2014-03-23 15:00:22'), ('1411', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 15:00:22'), ('1412', 'admin', '1', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 15:00:22'), ('1413', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 15:00:22'), ('1414', 'admin', '1', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 15:00:22'), ('1415', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 15:00:22'), ('1416', 'admin', '1', 'StoreProduct', 'VELUR', '2014-03-23 15:00:22'), ('1417', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 15:00:22'), ('1418', 'admin', '1', 'StoreProduct', 'CONTRAST pearly', '2014-03-23 15:00:22'), ('1419', 'admin', '1', 'StoreProduct', 'TERRE MEDICEE', '2014-03-23 15:00:22'), ('1420', 'admin', '1', 'StoreProduct', 'PERSEPOLIS Argento', '2014-03-23 15:00:22'), ('1421', 'admin', '1', 'StoreProduct', 'BISANTYUM argento', '2014-03-23 15:00:22'), ('1422', 'admin', '1', 'StoreProduct', 'BISANTYUM oro', '2014-03-23 15:00:22'), ('1423', 'admin', '1', 'StoreProduct', 'ARETINО', '2014-03-23 15:00:23'), ('1424', 'admin', '1', 'StoreProduct', 'NUVOLE М-04', '2014-03-23 15:00:23'), ('1425', 'admin', '1', 'StoreProduct', 'NUVOLE М-05', '2014-03-23 15:00:23'), ('1426', 'admin', '1', 'StoreProduct', 'NUVOLE М-08', '2014-03-23 15:00:23'), ('1427', 'admin', '1', 'StoreProduct', 'NUVOLE Incanto', '2014-03-23 15:00:23'), ('1428', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 15:00:23'), ('1429', 'admin', '1', 'StoreProduct', 'MILLELUCI', '2014-03-23 15:00:23'), ('1430', 'admin', '1', 'StoreProduct', 'POMPEIANO', '2014-03-23 15:00:23'), ('1431', 'admin', '1', 'StoreProduct', 'RIFLESSI FINITURA', '2014-03-23 15:00:23'), ('1432', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 15:00:23'), ('1433', 'admin', '1', 'StoreProduct', 'TATTO', '2014-03-23 15:00:23'), ('1434', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 15:00:23'), ('1435', 'admin', '1', 'StoreProduct', 'CRYSTAL', '2014-03-23 15:00:23'), ('1436', 'admin', '1', 'StoreProduct', 'R11 SILVER', '2014-03-23 15:00:23'), ('1437', 'admin', '1', 'StoreProduct', 'R11 GOLD', '2014-03-23 15:00:23'), ('1438', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 15:00:23'), ('1439', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 15:00:23'), ('1440', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 15:00:23'), ('1441', 'admin', '1', 'StoreProduct', 'FUSION platinum', '2014-03-23 15:00:23'), ('1442', 'admin', '1', 'StoreProduct', 'FUSION gold', '2014-03-23 15:00:23'), ('1443', 'admin', '1', 'StoreProduct', 'FUSION copper', '2014-03-23 15:00:23'), ('1444', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 15:00:23'), ('1445', 'admin', '1', 'StoreProduct', 'OSSIDIANA', '2014-03-23 15:00:23'), ('1446', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 15:00:23'), ('1447', 'admin', '1', 'StoreProduct', 'PERLAGE', '2014-03-23 15:00:23'), ('1448', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 15:00:23'), ('1449', 'admin', '1', 'StoreProduct', 'MURINIO', '2014-03-23 15:00:24'), ('1450', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 15:00:24'), ('1451', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 15:00:24'), ('1452', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 15:00:24'), ('1453', 'admin', '1', 'StoreProduct', 'METAL silver', '2014-03-23 15:00:24'), ('1454', 'admin', '1', 'StoreProduct', 'METAL gold', '2014-03-23 15:00:24'), ('1455', 'admin', '1', 'StoreProduct', 'METAL copper', '2014-03-23 15:00:24'), ('1456', 'admin', '1', 'StoreProduct', 'SPECCHIO', '2014-03-23 15:00:24'), ('1457', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 15:00:24'), ('1458', 'admin', '1', 'StoreProduct', 'OTHELLO TR', '2014-03-23 15:00:24'), ('1459', 'admin', '1', 'StoreProduct', 'OTHELLO whit', '2014-03-23 15:00:24'), ('1460', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 15:00:24'), ('1461', 'admin', '1', 'StoreProduct', 'PETRA', '2014-03-23 15:00:24'), ('1462', 'admin', '1', 'StoreProduct', 'MARMOFLOAT', '2014-03-23 15:00:24'), ('1463', 'admin', '1', 'StoreProduct', 'TRAVERTINO STONE', '2014-03-23 15:00:24'), ('1464', 'admin', '1', 'StoreProduct', 'RURALIS INTONACHINO', '2014-03-23 15:00:24'), ('1465', 'admin', '1', 'StoreProduct', 'PLASTIGRAF bianco', '2014-03-23 15:00:24'), ('1466', 'admin', '1', 'StoreProduct', 'KOLOSSOS', '2014-03-23 15:00:24'), ('1467', 'admin', '1', 'StoreProduct', 'MARMORINO MARBLE', '2014-03-23 15:00:24'), ('1468', 'admin', '1', 'StoreProduct', 'MEDITERRANEO', '2014-03-23 15:00:24'), ('1469', 'admin', '1', 'StoreProduct', 'VEGA BR', '2014-03-23 15:00:24'), ('1470', 'admin', '1', 'StoreProduct', 'ORION', '2014-03-23 15:00:24'), ('1471', 'admin', '1', 'StoreProduct', 'VITREX  neutro', '2014-03-23 15:00:24'), ('1472', 'admin', '1', 'StoreProduct', 'SHINE CERA', '2014-03-23 15:00:25'), ('1473', 'admin', '1', 'StoreProduct', 'CERA PETRA', '2014-03-23 15:00:25'), ('1474', 'admin', '1', 'StoreProduct', 'UNO TOP', '2014-03-23 15:00:25'), ('1475', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 15:00:25'), ('1476', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 15:00:25'), ('1477', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 15:00:25'), ('1478', 'admin', '1', 'StoreProduct', 'ACRYL 100', '2014-03-23 15:00:25'), ('1479', 'admin', '1', 'StoreProduct', 'UNO SATIN', '2014-03-23 15:00:25'), ('1480', 'admin', '1', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-03-23 15:00:25'), ('1481', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 15:00:25'), ('1482', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 15:00:25'), ('1483', 'admin', '1', 'StoreProduct', 'PRATIQUA PLUS', '2014-03-23 15:00:25'), ('1484', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 15:00:25'), ('1485', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 15:00:25'), ('1486', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 15:00:25'), ('1487', 'admin', '1', 'StoreProduct', 'OPALITH TECHNO', '2014-03-23 15:00:25'), ('1488', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 15:00:25'), ('1489', 'admin', '1', 'StoreProduct', 'QUARZO MEDIO', '2014-03-23 15:00:25'), ('1490', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 15:00:25'), ('1491', 'admin', '1', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-03-23 15:00:25'), ('1492', 'admin', '1', 'StoreProduct', 'RIEMPITIVO 104', '2014-03-23 15:00:25'), ('1493', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 15:00:25'), ('1494', 'admin', '1', 'StoreProduct', 'PRIMER WHITE', '2014-03-23 15:00:26'), ('1495', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 15:00:26'), ('1496', 'admin', '1', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-23 15:00:26'), ('1497', 'admin', '2', 'StoreProduct', 'VELUR', '2014-03-23 15:11:25'), ('1498', 'admin', '2', 'StoreProduct', 'VELUR', '2014-03-23 15:12:02'), ('1499', 'admin', '2', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 15:12:52'), ('1500', 'admin', '2', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-23 15:13:05'), ('1501', 'admin', '2', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 15:13:18'), ('1502', 'admin', '2', 'StoreProduct', 'NOBILI MANIE silver', '2014-03-23 15:13:31'), ('1503', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-03-23 15:15:07'), ('1504', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-03-23 15:15:19'), ('1505', 'admin', '2', 'StoreProduct', 'DIORA red', '2014-03-23 15:15:31'), ('1506', 'admin', '2', 'StoreProduct', 'DIORA red', '2014-03-23 15:15:42'), ('1507', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-23 15:16:45'), ('1508', 'admin', '2', 'StoreProduct', 'EDONI argento', '2014-03-23 15:16:52'), ('1509', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-23 15:17:01'), ('1510', 'admin', '2', 'StoreProduct', 'SHARDANA silver', '2014-03-23 15:18:02'), ('1511', 'admin', '2', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 15:18:09'), ('1512', 'admin', '2', 'StoreProduct', 'SHARDANA silver', '2014-03-23 15:18:18'), ('1513', 'admin', '2', 'StoreProduct', 'SHARDANA Gold', '2014-03-23 15:18:28'), ('1514', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-03-23 15:18:44'), ('1515', 'admin', '2', 'StoreProduct', 'NAXOS gold', '2014-03-23 15:19:17'), ('1516', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-23 15:20:03'), ('1517', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-03-23 15:20:42'), ('1518', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-03-23 15:21:18'), ('1519', 'admin', '2', 'StoreProduct', 'NAXOS silver', '2014-03-23 15:22:47'), ('1520', 'admin', '2', 'StoreProduct', 'MURINIO', '2014-03-23 15:47:54'), ('1521', 'admin', '2', 'StoreProduct', 'MURINIO', '2014-03-23 15:51:07'), ('1522', 'admin', '2', 'StoreProduct', 'MURINIO', '2014-03-23 15:51:35'), ('1523', 'admin', '2', 'StoreProduct', 'METAL silver', '2014-03-23 15:53:43'), ('1524', 'admin', '2', 'StoreProduct', 'METAL copper', '2014-03-23 15:53:50'), ('1525', 'admin', '2', 'StoreProduct', 'METAL gold', '2014-03-23 15:53:57'), ('1526', 'admin', '2', 'StoreProduct', 'METAL silver', '2014-03-23 15:54:04'), ('1527', 'admin', '2', 'StoreProduct', 'METAL copper', '2014-03-23 15:54:12'), ('1528', 'admin', '2', 'StoreProduct', 'METAL gold', '2014-03-23 15:54:19'), ('1529', 'admin', '2', 'StoreProduct', 'CRYSTAL', '2014-03-23 15:54:27'), ('1530', 'admin', '2', 'StoreProduct', 'CRYSTAL', '2014-03-23 15:54:33'), ('1531', 'admin', '2', 'StoreProduct', 'TATTO', '2014-03-23 15:54:40'), ('1532', 'admin', '2', 'StoreProduct', 'R11 SILVER', '2014-03-23 15:54:48'), ('1533', 'admin', '2', 'StoreProduct', 'FUSION copper', '2014-03-23 15:54:55'), ('1534', 'admin', '2', 'StoreProduct', 'FUSION gold', '2014-03-23 15:55:03'), ('1535', 'admin', '2', 'StoreProduct', 'R11 GOLD', '2014-03-23 15:55:10'), ('1536', 'admin', '2', 'StoreProduct', 'TATTO', '2014-03-23 15:55:19'), ('1537', 'admin', '2', 'StoreProduct', 'FUSION platinum', '2014-03-23 15:55:26'), ('1538', 'admin', '2', 'StoreProduct', 'FUSION gold', '2014-03-23 15:56:40'), ('1539', 'admin', '2', 'StoreProduct', 'FUSION copper', '2014-03-23 15:56:46'), ('1540', 'admin', '2', 'StoreProduct', 'OSSIDIANA', '2014-03-23 15:56:55'), ('1541', 'admin', '2', 'StoreProduct', 'OSSIDIANA', '2014-03-23 15:57:01'), ('1542', 'admin', '2', 'StoreProduct', 'PERLAGE', '2014-03-23 15:57:09'), ('1543', 'admin', '2', 'StoreProduct', 'PERLAGE', '2014-03-23 15:57:15'), ('1544', 'admin', '2', 'StoreProduct', 'MURINIO', '2014-03-23 15:57:22'), ('1545', 'admin', '2', 'StoreProduct', 'FUSION platinum', '2014-03-23 15:57:32'), ('1546', 'admin', '2', 'StoreProduct', 'FUSION platinum', '2014-03-23 15:57:56'), ('1547', 'admin', '2', 'StoreProduct', 'OTHELLO whit', '2014-03-23 16:02:45'), ('1548', 'admin', '2', 'StoreProduct', 'CERA PETRA', '2014-03-23 16:05:13'), ('1549', 'admin', '1', 'Page', 'wer', '2014-03-24 10:59:20'), ('1550', 'admin', '2', 'Page', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '2014-03-24 12:58:43'), ('1551', 'admin', '2', 'Page', 'wer', '2014-03-24 12:59:20'), ('1552', 'admin', '2', 'Page', 'wer', '2014-03-24 13:01:05'), ('1553', 'admin', '2', 'Page', 'wer', '2014-03-24 13:02:00'), ('1554', 'admin', '2', 'Page', 'wer', '2014-03-24 13:02:17'), ('1555', 'admin', '2', 'Page', 'wer', '2014-03-24 13:04:20'), ('1556', 'admin', '2', 'Page', 'wer', '2014-03-24 13:06:37'), ('1557', 'admin', '2', 'Page', 'wer', '2014-03-24 13:06:38'), ('1558', 'admin', '1', 'PageCategory', 'Акции и мероприятия', '2014-03-24 13:11:12'), ('1559', 'admin', '2', 'PageCategory', 'Акции и мероприятия', '2014-03-24 13:11:12'), ('1560', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-03-24 13:11:12'), ('1561', 'admin', '2', 'PageCategory', 'Блог', '2014-03-24 13:11:12'), ('1562', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-03-24 13:11:12'), ('1563', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-03-24 13:11:12'), ('1564', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-03-24 13:11:12'), ('1565', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-03-24 13:11:12'), ('1566', 'admin', '2', 'PageCategory', 'Новости', '2014-03-24 13:11:12'), ('1567', 'admin', '1', 'PageCategory', 'Акции', '2014-03-24 13:11:29'), ('1568', 'admin', '2', 'PageCategory', 'Акции и мероприятия', '2014-03-24 13:11:29'), ('1569', 'admin', '2', 'PageCategory', 'Акции', '2014-03-24 13:11:29'), ('1570', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-03-24 13:11:29'), ('1571', 'admin', '2', 'PageCategory', 'Блог', '2014-03-24 13:11:29'), ('1572', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-03-24 13:11:29'), ('1573', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-03-24 13:11:29'), ('1574', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-03-24 13:11:29'), ('1575', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-03-24 13:11:29'), ('1576', 'admin', '2', 'PageCategory', 'Новости', '2014-03-24 13:11:29'), ('1577', 'admin', '2', 'Page', 'МАСТЕР-КЛАСС 2010', '2014-03-24 13:21:54'), ('1578', 'admin', '2', 'Page', 'МАСТЕР-КЛАСС 2010', '2014-03-24 13:30:03'), ('1579', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-25 12:21:07'), ('1580', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-25 12:21:15'), ('1581', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-25 12:21:24'), ('1582', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-25 12:26:28'), ('1583', 'admin', '2', 'StoreProduct', 'DIORA silver', '2014-03-25 12:27:26'), ('1584', 'admin', '2', 'StoreProduct', 'DIORA gold', '2014-03-25 12:27:44'), ('1585', 'admin', '2', 'StoreProduct', 'DIORA red', '2014-03-25 12:27:49'), ('1586', 'admin', '2', 'StoreProduct', 'DIORA red', '2014-03-25 12:27:54'), ('1587', 'admin', '2', 'StoreProduct', 'EDONI oro', '2014-03-25 12:27:58'), ('1588', 'admin', '2', 'StoreProduct', 'VELUR', '2014-03-25 12:29:01'), ('1589', 'admin', '2', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-25 12:29:06'), ('1590', 'admin', '2', 'StoreProduct', 'NOBILI MANIE gold', '2014-03-25 12:29:11'), ('1591', 'admin', '2', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-03-25 12:38:54'), ('1592', 'admin', '2', 'Order', '15', '2014-03-25 13:16:20'), ('1593', 'admin', '2', 'Order', '15', '2014-03-25 13:16:20'), ('1594', 'admin', '2', 'Order', '15', '2014-03-25 13:16:20'), ('1595', 'admin', '2', 'Order', '15', '2014-03-25 13:16:20'), ('1596', 'admin', '2', 'StoreCurrency', 'Рубли', '2014-03-25 13:20:33'), ('1597', 'admin', '3', 'StoreCurrency', 'Доллары', '2014-03-25 13:20:50'), ('1598', 'admin', '2', 'StoreCurrency', 'Рубли', '2014-03-25 13:26:41'), ('1599', 'admin', '3', 'Order', '17', '2014-03-25 13:27:14'), ('1600', 'admin', '3', 'Order', '16', '2014-03-25 13:27:14'), ('1601', 'admin', '3', 'Order', '14', '2014-03-25 13:27:14'), ('1602', 'admin', '3', 'Order', '15', '2014-03-25 13:27:14'), ('1603', 'admin', '3', 'Order', '13', '2014-03-25 13:27:14'), ('1604', 'admin', '2', 'Order', '18', '2014-03-25 13:27:59'), ('1605', 'admin', '2', 'Order', '18', '2014-03-25 13:28:05'), ('1606', 'admin', '2', 'Order', '18', '2014-03-25 13:28:05'), ('1607', 'admin', '2', 'Order', '18', '2014-03-25 13:28:05'), ('1608', 'admin', '2', 'Order', '18', '2014-03-25 13:28:05'), ('1609', 'admin', '2', 'Order', '18', '2014-03-25 13:28:05'), ('1610', 'admin', '1', 'PageCategory', 'Портфолио', '2014-03-26 09:03:30'), ('1611', 'admin', '2', 'PageCategory', 'Акции и мероприятия', '2014-03-26 09:03:30'), ('1612', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-03-26 09:03:30'), ('1613', 'admin', '2', 'PageCategory', 'Блог', '2014-03-26 09:03:31'), ('1614', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-03-26 09:03:31'), ('1615', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-03-26 09:03:31'), ('1616', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-03-26 09:03:31'), ('1617', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-03-26 09:03:31'), ('1618', 'admin', '2', 'PageCategory', 'Новости', '2014-03-26 09:03:31'), ('1619', 'admin', '2', 'PageCategory', 'Портфолио', '2014-03-26 09:03:31'), ('1620', 'admin', '1', 'PageCategory', 'Фотогаллерея', '2014-03-26 09:05:57'), ('1621', 'admin', '2', 'PageCategory', 'Акции и мероприятия', '2014-03-26 09:05:57'), ('1622', 'admin', '2', 'PageCategory', 'Текстовая страница', '2014-03-26 09:05:57'), ('1623', 'admin', '2', 'PageCategory', 'Блог', '2014-03-26 09:05:57'), ('1624', 'admin', '2', 'PageCategory', 'Лакокрасочные изделия', '2014-03-26 09:05:57'), ('1625', 'admin', '2', 'PageCategory', 'Декоративная краска', '2014-03-26 09:05:57'), ('1626', 'admin', '2', 'PageCategory', 'Дизайн интерьера', '2014-03-26 09:05:57'), ('1627', 'admin', '2', 'PageCategory', 'Дизайн стен', '2014-03-26 09:05:57'), ('1628', 'admin', '2', 'PageCategory', 'Портфолио', '2014-03-26 09:05:57'), ('1629', 'admin', '2', 'PageCategory', 'Фотогаллерея', '2014-03-26 09:05:57'), ('1630', 'admin', '1', 'Page', 'Контакты', '2014-03-26 18:17:37'), ('1631', 'admin', '2', 'Page', 'Контакты', '2014-03-26 18:17:51'), ('1632', 'admin', '2', 'Page', 'Контакты', '2014-03-26 18:19:26'), ('1633', 'admin', '2', 'Page', 'Контакты', '2014-03-27 13:36:16'), ('1634', 'admin', '2', 'Page', 'Контакты', '2014-03-27 13:37:58'), ('1635', 'admin', '2', 'Page', 'Контакты', '2014-03-27 13:40:39'), ('1636', 'admin', '2', 'Page', 'Контакты', '2014-03-27 13:43:33'), ('1637', 'admin', '2', 'Page', 'Контакты', '2014-03-27 13:47:01'), ('1638', 'admin', '2', 'Page', 'Контакты', '2014-03-27 14:01:41'), ('1639', 'admin', '2', 'Page', 'Контакты', '2014-03-27 14:05:24'), ('1640', 'admin', '2', 'Page', 'Контакты', '2014-03-27 14:06:28'), ('1641', 'admin', '2', 'Page', 'Контакты', '2014-03-27 14:07:47'), ('1642', 'admin', '2', 'Page', 'Контакты', '2014-03-27 14:08:26'), ('1643', 'admin', '1', 'OrderStatus', 'Принят в работу', '2014-03-27 15:20:08'), ('1644', 'admin', '2', 'OrderStatus', 'Принят в работу', '2014-03-27 15:20:15'), ('1645', 'admin', '2', 'OrderStatus', 'Доставлен', '2014-03-27 15:20:19'), ('1646', 'admin', '2', 'Page', 'МАСТЕР-КЛАСС 2010', '2014-03-27 19:16:20'), ('1647', 'admin', '2', 'Page', 'МАСТЕР-КЛАСС 2010', '2014-03-27 19:17:20'), ('1648', 'admin', '2', 'Page', 'Контакты', '2014-03-27 19:17:35'), ('1649', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-07-30 14:55:20'), ('1650', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-07-30 14:55:20'), ('1651', 'admin', '3', 'StoreProduct', 'R11 SILVER', '2014-07-30 14:55:20'), ('1652', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-07-30 14:55:20'), ('1653', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-07-30 14:55:20'), ('1654', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-07-30 14:55:20'), ('1655', 'admin', '3', 'StoreProduct', 'FUSION platinum', '2014-07-30 14:55:20'), ('1656', 'admin', '3', 'StoreProduct', 'FUSION gold', '2014-07-30 14:55:20'), ('1657', 'admin', '3', 'StoreProduct', 'FUSION copper', '2014-07-30 14:55:20'), ('1658', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-07-30 14:55:20'), ('1659', 'admin', '3', 'StoreProduct', 'OSSIDIANA', '2014-07-30 14:55:20'), ('1660', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-07-30 14:55:20'), ('1661', 'admin', '3', 'StoreProduct', 'PERLAGE', '2014-07-30 14:55:20'), ('1662', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-07-30 14:55:20'), ('1663', 'admin', '3', 'StoreProduct', 'MURINIO', '2014-07-30 14:55:20'), ('1664', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-07-30 14:55:21'), ('1665', 'admin', '3', 'StoreProduct', 'METAL gold', '2014-07-30 14:55:21'), ('1666', 'admin', '3', 'StoreProduct', 'METAL copper', '2014-07-30 14:55:21'), ('1667', 'admin', '3', 'StoreProduct', 'METAL silver', '2014-07-30 14:55:21'), ('1668', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-07-30 14:55:21'), ('1669', 'admin', '3', 'StoreProduct', 'MILLELUCI', '2014-07-30 14:55:21'), ('1670', 'admin', '3', 'StoreProduct', 'POMPEIANO', '2014-07-30 14:55:21'), ('1671', 'admin', '3', 'StoreProduct', 'RIFLESSI FINITURA', '2014-07-30 14:55:21'), ('1672', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-07-30 14:55:21'), ('1673', 'admin', '3', 'StoreProduct', 'CRYSTAL', '2014-07-30 14:55:21'), ('1674', 'admin', '3', 'StoreProduct', 'TATTO', '2014-07-30 14:55:21'), ('1675', 'admin', '3', 'StoreProduct', 'TATTO', '2014-07-30 14:55:21'), ('1676', 'admin', '3', 'StoreProduct', 'NUVOLE М-05', '2014-07-30 14:55:21'), ('1677', 'admin', '3', 'StoreProduct', 'NUVOLE М-04', '2014-07-30 14:55:21'), ('1678', 'admin', '3', 'StoreProduct', 'ARETINО', '2014-07-30 14:55:21'), ('1679', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-07-30 14:55:29'), ('1680', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-07-30 14:55:29'), ('1681', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-07-30 14:55:29'), ('1682', 'admin', '3', 'StoreProduct', 'DIORA red', '2014-07-30 14:55:29'), ('1683', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-07-30 14:55:29');
INSERT INTO `ActionLog` VALUES ('1684', 'admin', '3', 'StoreProduct', 'NAXOS silver', '2014-07-30 14:55:29'), ('1685', 'admin', '3', 'StoreProduct', 'NAXOS gold', '2014-07-30 14:55:29'), ('1686', 'admin', '3', 'StoreProduct', 'ELISE', '2014-07-30 14:55:29'), ('1687', 'admin', '3', 'StoreProduct', 'DIORA gold', '2014-07-30 14:55:29'), ('1688', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-07-30 14:55:29'), ('1689', 'admin', '3', 'StoreProduct', 'EDONI argento', '2014-07-30 14:55:29'), ('1690', 'admin', '3', 'StoreProduct', 'EDONI oro', '2014-07-30 14:55:29'), ('1691', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-07-30 14:55:29'), ('1692', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-07-30 14:55:29'), ('1693', 'admin', '3', 'StoreProduct', 'SHARDANA silver', '2014-07-30 14:55:29'), ('1694', 'admin', '3', 'StoreProduct', 'SHARDANA Gold', '2014-07-30 14:55:29'), ('1695', 'admin', '3', 'StoreProduct', 'DIORA silver', '2014-07-30 14:55:29'), ('1696', 'admin', '3', 'StoreCategory', 'MaxMeyer', '2014-07-30 14:55:29'), ('1697', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-07-30 14:55:29'), ('1698', 'admin', '3', 'StoreProduct', 'TERRE MEDICEE', '2014-07-30 14:55:29'), ('1699', 'admin', '3', 'StoreProduct', 'CONTRAST pearly', '2014-07-30 14:55:29'), ('1700', 'admin', '3', 'StoreProduct', 'VELUR', '2014-07-30 14:55:29'), ('1701', 'admin', '3', 'StoreProduct', 'VELUR', '2014-07-30 14:55:29'), ('1702', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-07-30 14:55:29'), ('1703', 'admin', '3', 'StoreProduct', 'NOBILI MANIE gold', '2014-07-30 14:55:29'), ('1704', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-07-30 14:55:29'), ('1705', 'admin', '3', 'StoreProduct', 'NOBILI MANIE silver', '2014-07-30 14:55:29'), ('1706', 'admin', '3', 'StoreCategory', 'BALDINI', '2014-07-30 14:55:29'), ('1707', 'admin', '3', 'StoreProduct', 'BISANTYUM oro', '2014-07-30 14:55:29'), ('1708', 'admin', '3', 'StoreProduct', 'NUVOLE М-08', '2014-07-30 14:55:29'), ('1709', 'admin', '3', 'StoreProduct', 'NUVOLE Incanto', '2014-07-30 14:55:29'), ('1710', 'admin', '3', 'StoreProduct', 'BISANTYUM argento', '2014-07-30 14:55:29'), ('1711', 'admin', '3', 'StoreProduct', 'PERSEPOLIS Argento', '2014-07-30 14:55:29'), ('1712', 'admin', '3', 'StoreCategory', 'SAIF', '2014-07-30 14:55:29'), ('1713', 'admin', '3', 'StoreProduct', 'R11 GOLD', '2014-07-30 14:55:29'), ('1714', 'admin', '3', 'StoreCategory', 'ELEKTA', '2014-07-30 14:55:29'), ('1715', 'admin', '3', 'StoreCategory', 'Декоративные краски', '2014-07-30 14:55:29'), ('1716', 'admin', '3', 'StoreProduct', 'PETRA', '2014-07-30 14:55:40'), ('1717', 'admin', '3', 'StoreProduct', 'PETRA', '2014-07-30 14:55:40'), ('1718', 'admin', '3', 'StoreCategory', 'ELEKTA', '2014-07-30 14:55:40'), ('1719', 'admin', '3', 'StoreProduct', 'OTHELLO whit', '2014-07-30 14:55:40'), ('1720', 'admin', '3', 'StoreProduct', 'SPECCHIO', '2014-07-30 14:55:40'), ('1721', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-07-30 14:55:40'), ('1722', 'admin', '3', 'StoreProduct', 'OTHELLO TR', '2014-07-30 14:55:40'), ('1723', 'admin', '3', 'StoreCategory', 'BALDINI', '2014-07-30 14:55:40'), ('1724', 'admin', '3', 'StoreCategory', 'Венецианские штукатурки', '2014-07-30 14:55:40'), ('1725', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-07-30 14:55:45'), ('1726', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-07-30 14:55:45'), ('1727', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-07-30 14:55:45'), ('1728', 'admin', '3', 'StoreProduct', 'GREEN COAT BRILLIANTE', '2014-07-30 14:55:45'), ('1729', 'admin', '3', 'StoreProduct', 'ACRYL 100', '2014-07-30 14:55:45'), ('1730', 'admin', '3', 'StoreProduct', 'UNO SATIN', '2014-07-30 14:55:45'), ('1731', 'admin', '3', 'StoreProduct', 'UNO TOP', '2014-07-30 14:55:45'), ('1732', 'admin', '3', 'StoreCategory', 'SAIF', '2014-07-30 14:55:45'), ('1733', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-07-30 14:55:45'), ('1734', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-07-30 14:55:45'), ('1735', 'admin', '3', 'StoreProduct', 'PRATIQUA PLUS', '2014-07-30 14:55:45'), ('1736', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-07-30 14:55:45'), ('1737', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-07-30 14:55:45'), ('1738', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-07-30 14:55:45'), ('1739', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-07-30 14:55:45'), ('1740', 'admin', '3', 'StoreProduct', 'OPALITH TECHNO', '2014-07-30 14:55:45'), ('1741', 'admin', '3', 'StoreProduct', 'QUARZO MEDIO', '2014-07-30 14:55:45'), ('1742', 'admin', '3', 'StoreCategory', 'BALDINI', '2014-07-30 14:55:45'), ('1743', 'admin', '3', 'StoreCategory', 'Краски на водной основе', '2014-07-30 14:55:45'), ('1744', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-07-30 14:55:50'), ('1745', 'admin', '3', 'StoreProduct', 'RIEMPITIVO 104', '2014-07-30 14:55:50'), ('1746', 'admin', '3', 'StoreProduct', 'FISSATIVO ACRILICO ALL ACQUA SUPERCONCENTRATO', '2014-07-30 14:55:50'), ('1747', 'admin', '3', 'StoreCategory', 'BALDINI', '2014-07-30 14:55:50'), ('1748', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-07-30 14:55:50'), ('1749', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-07-30 14:55:50'), ('1750', 'admin', '3', 'StoreProduct', 'SAIFSEAL BETON QUARTZ', '2014-07-30 14:55:50'), ('1751', 'admin', '3', 'StoreProduct', 'PRIMER WHITE', '2014-07-30 14:55:50'), ('1752', 'admin', '3', 'StoreCategory', 'SAIF', '2014-07-30 14:55:50'), ('1753', 'admin', '3', 'StoreCategory', 'Подготовка поверхности', '2014-07-30 14:55:50'), ('1754', 'admin', '3', 'StoreProduct', 'SHINE CERA', '2014-07-30 14:56:13'), ('1755', 'admin', '3', 'StoreCategory', 'BALDINI', '2014-07-30 14:56:13'), ('1756', 'admin', '3', 'StoreProduct', 'CERA PETRA', '2014-07-30 14:56:13'), ('1757', 'admin', '3', 'StoreCategory', 'ELEKTA', '2014-07-30 14:56:13'), ('1758', 'admin', '3', 'StoreCategory', 'Ваксы', '2014-07-30 14:56:13'), ('1759', 'admin', '1', 'StoreCategory', 'Тест', '2014-07-30 15:01:53'), ('1760', 'admin', '1', 'StoreProduct', 'Тестовый продукт', '2014-07-30 15:02:49'), ('1761', 'admin', '1', 'StoreProduct', 'Тестовый про №2', '2014-07-30 15:03:45'), ('1762', 'admin', '1', 'StoreCategory', 'Категория 2', '2014-07-30 15:04:11'), ('1763', 'admin', '2', 'StoreCategory', 'Категория 2', '2014-07-30 15:04:14'), ('1764', 'admin', '2', 'StoreCategory', 'Категория 2', '2014-07-30 15:04:16'), ('1765', 'admin', '2', 'StoreCategory', 'Тест', '2014-07-30 15:04:20'), ('1766', 'admin', '2', 'StoreProduct', 'Тестовый про №2', '2014-07-30 15:04:34'), ('1767', 'admin', '3', 'StoreProduct', 'MARMOFLOAT', '2014-07-30 15:05:12'), ('1768', 'admin', '3', 'StoreProduct', 'TRAVERTINO STONE', '2014-07-30 15:05:12'), ('1769', 'admin', '3', 'StoreProduct', 'RURALIS INTONACHINO', '2014-07-30 15:05:12'), ('1770', 'admin', '3', 'StoreCategory', 'BALDINI', '2014-07-30 15:05:12'), ('1771', 'admin', '3', 'StoreProduct', 'PLASTIGRAF bianco', '2014-07-30 15:05:12'), ('1772', 'admin', '3', 'StoreCategory', 'VIERO', '2014-07-30 15:05:12'), ('1773', 'admin', '3', 'StoreProduct', 'KOLOSSOS', '2014-07-30 15:05:12'), ('1774', 'admin', '3', 'StoreProduct', 'MARMORINO MARBLE', '2014-07-30 15:05:13'), ('1775', 'admin', '3', 'StoreCategory', 'ELEKTA', '2014-07-30 15:05:13'), ('1776', 'admin', '3', 'StoreProduct', 'MEDITERRANEO', '2014-07-30 15:05:13'), ('1777', 'admin', '3', 'StoreProduct', 'ORION', '2014-07-30 15:05:13'), ('1778', 'admin', '3', 'StoreProduct', 'VITREX  neutro', '2014-07-30 15:05:13'), ('1779', 'admin', '3', 'StoreProduct', 'VEGA BR', '2014-07-30 15:05:13'), ('1780', 'admin', '3', 'StoreCategory', 'SAIF', '2014-07-30 15:05:13'), ('1781', 'admin', '3', 'StoreCategory', 'Декоративные штукатурки', '2014-07-30 15:05:13'), ('1782', 'admin', '1', 'StoreCategory', 'Категория 1', '2014-07-30 15:05:21'), ('1783', 'admin', '2', 'StoreCategory', 'Категория 1', '2014-07-30 15:05:26'), ('1784', 'admin', '1', 'StoreCategory', 'Каталог', '2014-07-30 15:05:46'), ('1785', 'admin', '2', 'StoreCategory', 'Каталог', '2014-07-30 15:05:49'), ('1786', 'admin', '1', 'StoreProduct', 'Продукт №3', '2014-07-30 15:06:30'), ('1787', 'admin', '2', 'StoreProduct', 'Продукт №3', '2014-08-04 10:48:32'), ('1788', 'admin', '2', 'StoreProduct', 'Продукт №3', '2014-08-04 10:50:19'), ('1789', 'admin', '2', 'StoreProduct', 'Продукт №3', '2014-08-04 11:07:12'), ('1790', 'admin', '2', 'StoreProduct', 'Продукт №3', '2014-08-04 11:08:33'), ('1791', 'admin', '1', 'StoreProduct', '123', '2014-08-04 11:12:10'), ('1792', 'admin', '2', 'StoreProduct', 'Тестовый про №2', '2014-08-04 18:51:20'), ('1793', 'admin', '2', 'StoreProduct', 'Продукт №3', '2014-08-04 18:51:27'), ('1794', 'admin', '2', 'StoreProduct', '123', '2014-08-04 18:51:35'), ('1795', 'admin', '3', 'Order', '18', '2014-08-04 18:57:33'), ('1796', 'admin', '2', 'StoreProduct', '123', '2014-08-07 12:40:58'), ('1797', 'admin', '2', 'StoreProduct', '123', '2014-08-07 12:44:12'), ('1798', 'admin', '2', 'StoreProduct', '123', '2014-08-07 12:46:41'), ('1799', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-07 12:47:47'), ('1800', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-07 12:48:18'), ('1801', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-07 12:48:18'), ('1802', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-07 12:48:45'), ('1803', 'admin', '2', 'StoreProduct', 'Шляпа №3', '2014-08-07 12:49:00'), ('1804', 'admin', '2', 'StoreProduct', 'Шляпа №2', '2014-08-07 12:49:10'), ('1805', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-07 12:49:26'), ('1806', 'admin', '2', 'StoreProduct', 'Шляпа №2', '2014-08-07 12:49:34'), ('1807', 'admin', '2', 'StoreProduct', 'Шляпа №3', '2014-08-07 12:49:42'), ('1808', 'admin', '2', 'StoreProduct', 'Шляпа №4', '2014-08-07 12:50:07'), ('1809', 'admin', '2', 'StoreProduct', 'Шляпа №3', '2014-08-07 12:50:31'), ('1810', 'admin', '1', 'StoreCategory', 'Головные уборы', '2014-08-07 12:51:18'), ('1811', 'admin', '1', 'StoreCategory', 'Шляпы', '2014-08-07 12:51:30'), ('1812', 'admin', '2', 'StoreCategory', 'Шляпы', '2014-08-07 12:51:33'), ('1813', 'admin', '1', 'StoreCategory', 'Разные', '2014-08-07 12:52:00'), ('1814', 'admin', '2', 'StoreCategory', 'Разные', '2014-08-07 12:52:03'), ('1815', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-07 12:52:17'), ('1816', 'admin', '2', 'StoreProduct', 'Шляпа №2', '2014-08-07 12:52:22'), ('1817', 'admin', '2', 'StoreProduct', 'Шляпа №3', '2014-08-07 12:52:28'), ('1818', 'admin', '2', 'StoreProduct', 'Шляпа №4', '2014-08-07 12:52:33'), ('1819', 'admin', '3', 'StoreCategory', 'Каталог', '2014-08-07 12:53:16'), ('1820', 'admin', '3', 'StoreCategory', 'Категория 1', '2014-08-07 12:53:16'), ('1821', 'admin', '3', 'StoreCategory', 'Тест', '2014-08-07 12:53:20'), ('1822', 'admin', '3', 'StoreCategory', 'Категория 2', '2014-08-07 12:53:20'), ('1823', 'admin', '1', 'StoreProduct', 'Шапка с усами', '2014-08-07 12:56:17'), ('1824', 'admin', '2', 'StoreProduct', 'Шапка с усами', '2014-08-07 12:56:27'), ('1825', 'admin', '1', 'StoreCategory', 'Электро техника', '2014-08-08 09:16:46'), ('1826', 'admin', '3', 'StoreCategory', 'Электро техника', '2014-08-08 09:17:08'), ('1827', 'admin', '1', 'StoreCategory', 'Компьютеры', '2014-08-08 09:17:25'), ('1828', 'admin', '1', 'StoreCategory', 'Компьютерные комплектующие', '2014-08-08 09:17:54'), ('1829', 'admin', '1', 'StoreCategory', 'Ноутбуки', '2014-08-08 09:18:22'), ('1830', 'admin', '2', 'StoreCategory', 'Ноутбуки', '2014-08-08 09:18:25'), ('1831', 'admin', '1', 'StoreCategory', 'Стационарные компьютеры', '2014-08-08 09:18:49'), ('1832', 'admin', '2', 'StoreCategory', 'Стационарные компьютеры', '2014-08-08 09:18:53'), ('1833', 'admin', '2', 'StoreCategory', 'Стационарные компьютеры', '2014-08-08 09:19:08'), ('1834', 'admin', '1', 'StoreCategory', 'HDD', '2014-08-08 09:19:37'), ('1835', 'admin', '2', 'StoreCategory', 'HDD', '2014-08-08 09:19:42'), ('1836', 'admin', '1', 'StoreCategory', 'Материнские платы', '2014-08-08 09:20:20'), ('1837', 'admin', '2', 'StoreCategory', 'Материнские платы', '2014-08-08 09:20:23'), ('1838', 'admin', '1', 'StoreCategory', 'Опертивная память', '2014-08-08 09:20:39'), ('1839', 'admin', '2', 'StoreCategory', 'Опертивная память', '2014-08-08 09:20:43'), ('1840', 'admin', '1', 'StoreCategory', 'Корпуса', '2014-08-08 09:21:04'), ('1841', 'admin', '2', 'StoreCategory', 'Корпуса', '2014-08-08 09:34:56'), ('1842', 'admin', '2', 'StoreProduct', 'Шляпа', '2014-08-19 16:53:06');
COMMIT;

-- ----------------------------
--  Table structure for `AuthAssignment`
-- ----------------------------
DROP TABLE IF EXISTS `AuthAssignment`;
CREATE TABLE `AuthAssignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `AuthAssignment`
-- ----------------------------
BEGIN;
INSERT INTO `AuthAssignment` VALUES ('Admin', '1', null, null), ('Authenticated', '57', null, 'N;'), ('Authenticated', '58', null, 'N;'), ('Orders.Orders.*', '56', null, 'N;'), ('Orders.Statuses.*', '56', null, 'N;'), ('Authenticated', '56', null, 'N;'), ('Authenticated', '62', null, 'N;'), ('Authenticated', '63', null, 'N;'), ('Authenticated', '2', null, 'N;'), ('Authenticated', '3', null, 'N;'), ('Authenticated', '4', null, 'N;'), ('Authenticated', '5', null, 'N;');
COMMIT;

-- ----------------------------
--  Table structure for `AuthItem`
-- ----------------------------
DROP TABLE IF EXISTS `AuthItem`;
CREATE TABLE `AuthItem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `AuthItem`
-- ----------------------------
BEGIN;
INSERT INTO `AuthItem` VALUES ('Admin', '2', null, null, 'N;'), ('Authenticated', '2', null, null, 'N;'), ('Guest', '2', null, null, 'N;'), ('Orders.Orders.*', '1', null, null, 'N;'), ('Orders.Statuses.*', '1', null, null, 'N;'), ('Orders.Orders.Index', '0', null, null, 'N;'), ('Orders.Orders.Create', '0', null, null, 'N;'), ('Orders.Orders.Update', '0', null, null, 'N;'), ('Orders.Orders.AddProductList', '0', null, null, 'N;'), ('Orders.Orders.AddProduct', '0', null, null, 'N;'), ('Orders.Orders.RenderOrderedProducts', '0', null, null, 'N;'), ('Orders.Orders.JsonOrderedProducts', '0', null, null, 'N;'), ('Orders.Orders.Delete', '0', null, null, 'N;'), ('Orders.Orders.DeleteProduct', '0', null, null, 'N;'), ('Orders.Statuses.Index', '0', null, null, 'N;'), ('Orders.Statuses.Create', '0', null, null, 'N;'), ('Orders.Statuses.Update', '0', null, null, 'N;'), ('Orders.Statuses.Delete', '0', null, null, 'N;');
COMMIT;

-- ----------------------------
--  Table structure for `AuthItemChild`
-- ----------------------------
DROP TABLE IF EXISTS `AuthItemChild`;
CREATE TABLE `AuthItemChild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `Comments`
-- ----------------------------
DROP TABLE IF EXISTS `Comments`;
CREATE TABLE `Comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT '0',
  `class_name` varchar(100) DEFAULT '',
  `object_pk` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `email` varchar(255) DEFAULT '',
  `name` varchar(50) DEFAULT '',
  `text` text,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `ip_address` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `class_name_index` (`class_name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `Discount`
-- ----------------------------
DROP TABLE IF EXISTS `Discount`;
CREATE TABLE `Discount` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `active` tinyint(1) DEFAULT NULL,
  `sum` varchar(10) DEFAULT '',
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `roles` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `active` (`active`),
  KEY `start_date` (`start_date`),
  KEY `end_date` (`end_date`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `Discount`
-- ----------------------------
BEGIN;
INSERT INTO `Discount` VALUES ('1', 'Скидка на всю технику Apple', '1', '5%', '2014-01-15 09:12:43', '2015-01-01 12:00:00', null);
COMMIT;

-- ----------------------------
--  Table structure for `DiscountCategory`
-- ----------------------------
DROP TABLE IF EXISTS `DiscountCategory`;
CREATE TABLE `DiscountCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_id` (`discount_id`),
  KEY `category_id` (`category_id`)
) ENGINE=MyISAM AUTO_INCREMENT=317 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `DiscountCategory`
-- ----------------------------
BEGIN;
INSERT INTO `DiscountCategory` VALUES ('308', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `DiscountManufacturer`
-- ----------------------------
DROP TABLE IF EXISTS `DiscountManufacturer`;
CREATE TABLE `DiscountManufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `discount_id` int(11) DEFAULT NULL,
  `manufacturer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `discount_id` (`discount_id`),
  KEY `manufacturer_id` (`manufacturer_id`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `News`
-- ----------------------------
DROP TABLE IF EXISTS `News`;
CREATE TABLE `News` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `content` text,
  `date_created` int(10) unsigned DEFAULT NULL,
  `status` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `News`
-- ----------------------------
BEGIN;
INSERT INTO `News` VALUES ('4', 'Мы полностью обновили наш сайт!', 'Добро пожаловать на новый сайт компании \"Домино\".', '1392928828', '1'), ('6', 'Программа лояльности для постоянных партнеров', 'Выгодное предложение для постоянных клиентов\"Ателье красок \"Домино\": уникальный материал FUSION теперь можно приобрести со скидкой! Подробная информация в салонах \"Ателье красок \"Домино\".\r\n<br><br>\r\nFUSOIN - уникальный, не имеющий аналогов материал. Своего рода новый вид венецианской штукатурки с бархатисто-сатиновым внешним видом и металлизированным глубоким эффектом. Классическая цветовая гамма: золото, платина, медь позволяет воплотить в реальность любые задумки по оформлению интерьера. А сейчас постоянные клиенты \"Ателье красок \"Домино\" имеют возможность значительно увеличить свою скидку при покупке этого популярнейшего в Европе материала. И поверьте, наше предложение вас приятно порадует. До встречи в салонах \"Ателье красок \"Домино\"!', '1392931099', '1'), ('7', 'Презентация новых материалов Итальянских фабрик', '«Ателье красок «Домино»первыми на отечественном рынке представляет новую коллекцию эксклюзивных отделочных материалов от законодателей мировых тенденций в области создания декоративных материалов - итальянских фабрик-производителей Gruppo GA NI vernici, MaxMeyer & Baldini vernici . Подробности узнайте в салонах «Ателье красок «Домино».\r\n<br><br>\r\n Опираясь на современные научные исследования и достижения технологии, руководствуясь многолетним опытом создания высококачественных продуктов, из которых подобно музыке рождается неповторимый интерьер, и рассматривая финишные отделочные материалы как возможность каждому человеку выразить себя в своем пространстве, итальянские законодатели моды в области декоративной отделки создали новейшую и современнейшую коллекцию 2010 года, которую \"Ателье красок \"Домино\", как их официальный представитель, предлагает российскому потребителю.\r\n<br><br>\r\n<b>График проведения презентационного тура:</b><br>\r\n15.07.10    г. Санкт-Петербург<br>\r\n15.07.10    г. Новороссийск<br>\r\n16.07.10    г. Анапа<br>\r\n19.07.10    г. Ростов-на-Дону<br>\r\n21.07.10    г. Краснодар<br>\r\n22.07.10    г. Пятигорск<br>\r\n23.07.10    г. Ставрополь<br>\r\n23.07.10    г. Сочи<br>\r\n27.07.10    г. Туапсе<br>', '1392931229', '1');
COMMIT;

-- ----------------------------
--  Table structure for `Order`
-- ----------------------------
DROP TABLE IF EXISTS `Order`;
CREATE TABLE `Order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `secret_key` varchar(10) DEFAULT '',
  `delivery_id` int(11) DEFAULT NULL,
  `delivery_price` float(10,2) DEFAULT NULL,
  `total_price` float(10,2) DEFAULT NULL,
  `status_id` int(11) DEFAULT NULL,
  `paid` tinyint(1) DEFAULT NULL,
  `user_name` varchar(100) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL COMMENT 'delivery address',
  `user_phone` varchar(30) DEFAULT NULL,
  `user_comment` varchar(500) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `admin_comment` text,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `secret_key` (`secret_key`),
  KEY `delivery_id` (`delivery_id`),
  KEY `status_id` (`status_id`)
) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `Order`
-- ----------------------------
BEGIN;
INSERT INTO `Order` VALUES ('20', '1', 'innl65swus', '15', '0.00', '132.00', '1', null, 'admin', 'admin@admin.ru', null, '111', null, '37.213.65.208', '2014-08-04 19:02:52', '2014-08-04 19:02:52', null, null), ('19', '1', 'cifnztw8a9', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '2341234123', null, '37.213.65.208', '2014-08-04 18:51:59', '2014-08-04 18:51:59', null, null), ('21', '1', 'vwu2wwmrcf', '15', '0.00', '384.00', '1', null, 'admin', 'admin@admin.ru', null, '111', null, '37.213.65.208', '2014-08-04 19:03:06', '2014-08-04 19:03:06', null, null), ('22', '1', 'z4pfcr36e0', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '111', null, '37.213.65.208', '2014-08-04 19:03:18', '2014-08-04 19:03:18', null, null), ('23', null, 'kosrav93sw', '15', '0.00', '65.00', '1', null, '13', 'QQ@QQ.QQ', null, '22', null, '37.213.65.208', '2014-08-04 19:12:08', '2014-08-04 19:12:08', null, null), ('24', '1', '5r5xszsdnq', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '111', null, '37.213.65.208', '2014-08-04 19:12:34', '2014-08-04 19:12:34', null, null), ('25', null, 'eibdq6j3d4', '15', '0.00', '65.00', '1', null, 'admin', 'we@gmail.com', null, '111', null, '37.213.30.194', '2014-08-16 14:01:50', '2014-08-16 14:01:50', null, null), ('26', null, 'zvdouvwjve', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '2341234123', null, '37.213.30.194', '2014-08-16 14:02:30', '2014-08-16 14:02:30', null, null), ('27', null, 'idx8lzwiik', '15', '0.00', '65.00', '1', null, 'liefdjhss', 'kjahsgdlf@ujhg.fdfd', null, ',jkga', null, '37.213.91.107', '2014-08-16 22:08:49', '2014-08-16 22:08:49', null, null), ('28', '1', 'q3y9me37vz', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '6655555', null, '37.213.79.249', '2014-08-20 17:44:41', '2014-08-20 17:44:41', null, null), ('29', '1', 'n75t0y2quc', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '6230101', null, '86.57.189.55', '2014-08-20 17:44:56', '2014-08-20 17:44:57', null, null), ('30', '1', 'u9hv5m2vsz', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '89723683476598243', null, '37.213.79.249', '2014-08-20 17:45:05', '2014-08-20 17:45:05', null, null), ('31', '1', '7ud1bjoaji', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '245243523452345', null, '37.213.79.249', '2014-08-20 17:46:20', '2014-08-20 17:46:20', null, null), ('32', '1', '50irh3f3bh', '15', '0.00', '65.00', '1', null, 'admin', 'admin@admin.ru', null, '23452345234', null, '37.213.79.249', '2014-08-20 17:47:59', '2014-08-20 17:47:59', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `OrderHistory`
-- ----------------------------
DROP TABLE IF EXISTS `OrderHistory`;
CREATE TABLE `OrderHistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `handler` varchar(255) DEFAULT NULL,
  `data_before` text,
  `data_after` text,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_index` (`order_id`),
  KEY `created_index` (`created`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `OrderProduct`
-- ----------------------------
DROP TABLE IF EXISTS `OrderProduct`;
CREATE TABLE `OrderProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `configurable_id` int(11) DEFAULT NULL,
  `name` text,
  `configurable_name` text COMMENT 'Store name of configurable product',
  `configurable_data` text,
  `variants` text,
  `quantity` smallint(6) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `price` float(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `configurable_id` (`configurable_id`)
) ENGINE=MyISAM AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `OrderProduct`
-- ----------------------------
BEGIN;
INSERT INTO `OrderProduct` VALUES ('127', '21', '580', '0', 'Тестовый продукт', null, null, null, '1', '', '254.00'), ('126', '21', '582', '0', 'Продукт №3', null, null, null, '2', '', '65.00'), ('125', '20', '583', '0', '123', null, null, null, '1', '', '132.00'), ('124', '19', '582', '0', 'Продукт №3', null, null, null, '1', '', '65.00'), ('128', '22', '582', '0', 'Продукт №3', null, null, null, '1', '', '65.00'), ('129', '23', '582', '0', 'Продукт №3', null, null, null, '1', '', '65.00'), ('130', '24', '582', '0', 'Продукт №3', null, null, null, '1', '', '65.00'), ('131', '25', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('132', '26', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('133', '27', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('134', '28', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('135', '29', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('136', '30', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('137', '31', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00'), ('138', '32', '582', '0', 'Шляпа №2', null, null, null, '1', '', '65.00');
COMMIT;

-- ----------------------------
--  Table structure for `OrderStatus`
-- ----------------------------
DROP TABLE IF EXISTS `OrderStatus`;
CREATE TABLE `OrderStatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `position` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `position` (`position`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `OrderStatus`
-- ----------------------------
BEGIN;
INSERT INTO `OrderStatus` VALUES ('1', 'Новый', '0'), ('5', 'Доставлен', '2'), ('6', 'Принят в работу', '1');
COMMIT;

-- ----------------------------
--  Table structure for `Page`
-- ----------------------------
DROP TABLE IF EXISTS `Page`;
CREATE TABLE `Page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `publish_date` datetime DEFAULT NULL,
  `status` varchar(255) DEFAULT '',
  `layout` varchar(2555) DEFAULT '',
  `view` varchar(255) DEFAULT '',
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `url` (`url`),
  KEY `created` (`created`),
  KEY `updated` (`updated`),
  KEY `publish_date` (`publish_date`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `Page`
-- ----------------------------
BEGIN;
INSERT INTO `Page` VALUES ('8', '1', null, 'help', '2012-06-10 22:35:51', '2012-07-07 11:47:09', '2012-06-10 22:35:29', 'published', '', '', null), ('9', '1', null, 'how-to-create-order', '2012-06-10 22:36:50', '2012-07-07 11:45:53', '2012-06-10 22:36:27', 'published', '', '', null), ('10', '1', null, 'garantiya', '2012-06-10 22:37:06', '2012-07-07 11:45:38', '2012-06-10 22:36:50', 'published', '', '', null), ('11', '1', null, 'dostavka-i-oplata', '2012-06-10 22:37:18', '2012-07-07 11:41:49', '2012-06-10 22:37:07', 'published', '', '', null), ('15', '1', null, 'o-kompanii', '2014-02-11 20:10:34', '2014-02-11 20:39:55', '2014-02-11 20:10:22', 'published', '', '', null), ('16', '1', '13', 'baldini', '2014-02-20 10:44:28', '2014-02-20 10:44:28', '2014-02-20 10:43:59', 'published', '', '', null), ('17', '1', '13', 'elekta', '2014-02-20 10:47:53', '2014-02-20 10:47:53', '2014-02-20 10:47:33', 'published', '', '', null), ('18', '1', '13', 'maxmeyer', '2014-02-20 10:48:22', '2014-02-20 10:48:22', '2014-02-20 10:47:56', 'published', '', '', null), ('19', '1', '13', 'saif', '2014-02-20 10:48:50', '2014-02-20 10:48:50', '2014-02-20 10:48:36', 'published', '', '', null), ('20', '1', '13', 'viero', '2014-02-20 10:49:21', '2014-02-20 10:49:21', '2014-02-20 10:49:00', 'published', '', '', null), ('21', '1', '15', 'vidi-krasok-kakuyu-krasku-vibrat-dlya-pokraski-sten-i-potolka', '2014-02-20 18:22:50', '2014-03-24 12:58:06', '2014-02-20 18:20:56', 'published', '', '', null), ('22', '1', '18', 'originalnii-dizain-sten', '2014-02-20 22:19:49', '2014-02-20 22:20:43', '2014-02-20 22:16:46', 'published', '', '', null), ('23', '1', '20', 'master-klass-2010', '2014-03-24 10:59:20', '2014-03-27 19:17:20', '2014-03-24 10:59:11', 'published', '', '', '1395655597.jpg'), ('24', '1', '13', 'contacts', '2014-03-26 18:17:37', '2014-03-27 19:17:35', '2014-03-26 18:16:34', 'published', '', '', null);
COMMIT;

-- ----------------------------
--  Table structure for `PageCategory`
-- ----------------------------
DROP TABLE IF EXISTS `PageCategory`;
CREATE TABLE `PageCategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `url` varchar(255) DEFAULT '',
  `full_url` text,
  `layout` varchar(255) DEFAULT '',
  `view` varchar(255) DEFAULT '',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `page_size` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `url` (`url`),
  KEY `created` (`created`),
  KEY `updated` (`updated`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `PageCategory`
-- ----------------------------
BEGIN;
INSERT INTO `PageCategory` VALUES ('13', null, 'tekstovaya-stranica', 'tekstovaya-stranica', '', '', '2014-02-11 20:10:17', '2014-02-11 20:10:17', null), ('10', null, 'tesstovya2', 'tesstovya2', '', '', '2013-05-21 23:59:34', '2013-05-21 23:59:34', null), ('14', null, 'blog', 'blog', '', '', '2014-02-20 18:13:55', '2014-02-20 18:13:55', null), ('15', '14', 'lakokrasochnie-izdeliya', 'blog/lakokrasochnie-izdeliya', '', '', '2014-02-20 18:40:10', '2014-02-20 18:40:10', null), ('16', '14', 'dekorativnaya-kraska', 'blog/dekorativnaya-kraska', '', '', '2014-02-20 21:19:19', '2014-02-20 21:19:19', null), ('17', '14', 'dizain-interera', 'blog/dizain-interera', '', '', '2014-02-20 21:19:48', '2014-02-20 21:19:48', null), ('18', '14', 'dizain-sten', 'blog/dizain-sten', '', '', '2014-02-20 21:20:01', '2014-02-20 21:20:01', null), ('23', '14', 'fotogallereya', 'blog/fotogallereya', '', '', '2014-03-26 09:05:57', '2014-03-26 09:05:57', null), ('20', null, 'akcii-i-meropriyatiya', 'akcii-i-meropriyatiya', '', '', '2014-03-24 13:11:12', '2014-03-24 13:11:12', null), ('22', '14', 'portfolio', 'blog/portfolio', '', '', '2014-03-26 09:03:30', '2014-03-26 09:03:30', null);
COMMIT;

-- ----------------------------
--  Table structure for `PageCategoryTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `PageCategoryTranslate`;
CREATE TABLE `PageCategoryTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `PageCategoryTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `PageCategoryTranslate` VALUES ('13', '7', '1', 'Новости', '', '', '', ''), ('15', '11', '1', 'sdfsdf', '', '', '', ''), ('41', '20', '1', 'Акции и мероприятия', '', '', '', ''), ('17', '12', '1', 'Тесстовя2', '', '', '', ''), ('40', '19', '10', null, null, null, null, null), ('19', '13', '1', 'Текстовая страница', '', '', '', ''), ('39', '18', '10', null, null, null, null, null), ('21', '14', '1', 'Блог', '', '', '', ''), ('38', '17', '10', null, null, null, null, null), ('23', '15', '1', 'Лакокрасочные изделия', '', '', '', ''), ('37', '16', '10', null, null, null, null, null), ('25', '16', '1', 'Декоративная краска', '', '', '', ''), ('36', '15', '10', null, null, null, null, null), ('27', '17', '1', 'Дизайн интерьера', '', '', '', ''), ('35', '14', '10', null, null, null, null, null), ('29', '18', '1', 'Дизайн стен', '', '', '', ''), ('34', '10', '10', null, null, null, null, null), ('31', '19', '1', 'Новости', '', '', '', ''), ('33', '13', '10', null, null, null, null, null), ('42', '20', '10', 'Акции и мероприятия', '', '', '', ''), ('43', '21', '1', 'Акции', '', '', '', ''), ('44', '21', '10', 'Акции', '', '', '', ''), ('45', '22', '1', 'Портфолио', '', '', '', ''), ('46', '22', '10', 'Портфолио', '', '', '', ''), ('47', '23', '1', 'Фотогаллерея', '', '', '', ''), ('48', '23', '10', 'Фотогаллерея', '', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `PageTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `PageTranslate`;
CREATE TABLE `PageTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT '',
  `short_description` text,
  `full_description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `PageTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `PageTranslate` VALUES ('15', '8', '1', 'Помощь', 'Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации \"Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..\" Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам \"lorem ipsum\" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).', '', '', '', ''), ('17', '9', '1', 'Как сделать заказ', '<p>Есть много вариантов Lorem Ipsum, но большинство из них имеет не всегда приемлемые модификации, например, юмористические вставки или слова, которые даже отдалённо не напоминают латынь. Если вам нужен Lorem Ipsum для серьёзного проекта, вы наверняка не хотите какой-нибудь шутки, скрытой в середине абзаца. Также все другие известные генераторы Lorem Ipsum используют один и тот же текст, который они просто повторяют, пока не достигнут нужный объём. Это делает предлагаемый здесь генератор единственным настоящим Lorem Ipsum генератором. Он использует словарь из более чем 200 латинских слов, а также набор моделей предложений. В результате сгенерированный Lorem Ipsum выглядит правдоподобно, не имеет повторяющихся абзацей или \"невозможных\" слов.</p>', '', '', '', ''), ('19', '10', '1', 'Гарантия', '<p>Многие думают, что Lorem Ipsum - взятый с потолка псевдо-латинский набор слов, но это не совсем так. Его корни уходят в один фрагмент классической латыни 45 года н.э., то есть более двух тысячелетий назад. Ричард МакКлинток, профессор латыни из колледжа Hampden-Sydney, штат Вирджиния, взял одно из самых странных слов в Lorem Ipsum, \"consectetur\", и занялся его поисками в классической латинской литературе.</p>\r\n<p>В результате он нашёл неоспоримый первоисточник Lorem Ipsum в разделах 1.10.32 и 1.10.33 книги \"de Finibus Bonorum et Malorum\" (\"О пределах добра и зла\"), написанной Цицероном в 45 году н.э. Этот трактат по теории этики был очень популярен в эпоху Возрождения. Первая строка Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", происходит от одной из строк в разделе 1.10.32 Классический текст Lorem Ipsum, используемый с XVI века, приведён ниже. Также даны разделы 1.10.32 и 1.10.33 \"de Finibus Bonorum et Malorum\" Цицерона и их английский перевод, сделанный H. Rackham, 1914 год.</p>', '', '', '', ''), ('21', '11', '1', 'Доставка и оплата', '<p>Давно выяснено, что при оценке дизайна и композиции читаемый текст мешает сосредоточиться. Lorem Ipsum используют потому, что тот обеспечивает более или менее стандартное заполнение шаблона, а также реальное распределение букв и пробелов в абзацах, которое не получается при простой дубликации \"Здесь ваш текст.. Здесь ваш текст.. Здесь ваш текст..\" Многие программы электронной вёрстки и редакторы HTML используют Lorem Ipsum в качестве текста по умолчанию, так что поиск по ключевым словам \"lorem ipsum\" сразу показывает, как много веб-страниц всё ещё дожидаются своего настоящего рождения. За прошедшие годы текст Lorem Ipsum получил много версий. Некоторые версии появились по ошибке, некоторые - намеренно (например, юмористические варианты).</p>', '', '', '', ''), ('29', '15', '1', 'О компании', '', 'ООО «Фирма «<strong>Домино</strong>» с 1994 года является поставщиком декоративных отделочных материалов европейского уровня на российский рынок. В течение 16-ти лет фирма Домино представляет лучшие декоративные отделочные материалы итальянских фабрик-производителей <strong>Saif</strong>, <strong>Baldini Vernici</strong>, а также поставляет продукцию фабрики <strong>Gruppo GANI vernici</strong> и высококлассный инструмент фабрики <strong>Boldrini</strong>, являясь официальным представителем итальянской лакокрасочной продукции на отечественном рынке.<div>&nbsp;<div>C 2010 года компания \"Домино\" с гордостью представляет в своем ассортименте линию профессиональных материалов премиум-класса фабрики <strong>MaxMeyer</strong>!&nbsp;</div><div><br></div><div>Продукция, представляемая Домино, отвечает высоким стандартам — имеет международный сертификат качества ISO 9002. Часть продукции отмечена знаком «Эколейбл», что говорит об экологической чистоте выпускаемого товара. Внедрение продукции <strong>SAIF</strong> и <strong>Baldini vernici</strong> на российский рынок – очередной шаг к повышению потребительской культуры выбора качественных товаров и заботе о здоровье российских потребителей.&nbsp;</div><div><br></div><div>\r\nПрямые поставки из <strong>Италии</strong> от фабрик-производителей позволяют существенно выигрывать не только в оригинальности, а порой и полном отсутствии аналогов продукции, но и в её стоимости. Длительное сотрудничество, полные доверительные и практически дружеские отношения с поставщиками позволяют получать самые выгодные условия работы для компании, а, следовательно, и самые приемлемые ценовые условия для потребителя на российском рынке.&nbsp;</div><div><br></div><div>Политика компании Домино направлена, прежде всего, на внедрение новейших западных технологий, высокое качество и широкий ассортимент представляемой продукции, а также на максимальный комфорт и удобство покупателей. Именно поэтому фирма Домино создала свою фирменную сеть элитных салонов «Ателье красок «Домино» на юге России, а также в г. Санкт-Петербурге. Салонов, в которых Вы можете приобрести высококачественные декоративные материалы таких производителей как <strong>Saif</strong>, <strong>Baldini</strong> <strong>Vernici</strong>, <strong>Gruppo</strong> <strong>GANI</strong> <strong>vernici</strong> и профессиональный инструмент фабрики <strong>Boldrini</strong>.&nbsp;</div><div><br></div><div>Уютные салоны, отделанные лучшими итальянскими декоративными материалами, представленными нашей фирмой,\r\nогромный, постоянно пополняющийся и расширяющийся ассортимент материалов, а также:&nbsp;</div><div><ul><li>привлекательные цены,&nbsp;</li><li>дисконтная программа,&nbsp;</li><li>регулярно проводимые мастер-классы по обучению работе с декоративными материалами,&nbsp;</li><li>компьютерный подбор цвета,&nbsp;</li><li>индивидуальный подход к каждому клиенту - вот что предлагают каждому своему клиенту салоны Домино.&nbsp;</li></ul></div><div>Квалифицированные специалисты всегда рады помочь в выборе продукции и дать персональные рекомендации каждому клиенту по вопросам, касающимся особенностей декоративных материалов, специфики их нанесения, техническим характеристикам, как в наших салонах, так и непосредственно на объектах, используя уникальные знания, технологии и инструменты.&nbsp;</div><div><br></div><div>Опыт работы специалистов компании Домино с высокодекоративными материалами и технологиями их применения широко используется мастерами и дизайнерами, которые помогают проектировать индивидуальные решения в интерьерах и экстерьерах, корректировать требования и пожелания заказчика и претворять творческие замыслы в жизнь.&nbsp;</div><div><br></div><div>В Ателье красок «Домино» вы найдете все, что нужно для вашего ремонта и отделки, будь то простой косметический ремонт, или создание нового современного интерьера, или же решение задач по выполнению эксклюзивных художественных работ.&nbsp;</div><div><br></div><div><strong>Мы всегда рады видеть Вас у нас в гостях и готовы помочь Вам в обустройстве Вашего дома.</strong>  </div>  </div>', '', '', ''), ('31', '16', '1', 'Baldini', '', 'Содержание страницы', '', '', ''), ('33', '17', '1', 'Elekta', '', 'Текст страницы', '', '', ''), ('35', '18', '1', 'Maxmeyer', '', 'Описание страницы', '', '', ''), ('37', '19', '1', 'Saif', '', 'Описание страницы', '', '', ''), ('39', '20', '1', 'Viero', '', 'Описание страницы', '', '', ''), ('41', '21', '1', 'Виды красок. Какую краску выбрать для покраски стен и потолка.', '<div><img src=\"/uploads/blog/designer-home-decor-1024x576.jpg\" style=\"border:0px  #000000;width:600px;height:337px;vertical-align:baseline\"><br></div><div><br></div>Акриловые и латексные краски. Какую краску выбрать для покраски стен и потолка? Рано или поздно наступает такое время, когда хочется обновить вид своего интерьера. Вы можете поменять мебель, сменить гардины, повесить новую люстру, а можно поменять цвет стен. Вот этим мы и займемся, будем выбирать краски для покраски стен в вашей квартире. Расскажем про все виды красок – водорастворимые краски, акриловые краски, латексные краски и экологические краски.', '<div><br></div><div> Современные производители красок предлагают, очень большой ассортимент красок, и приходя в магазин, покупатель теряется от такого изобилия. В этой статья мы поможем Вам сделать правильный выбор.&nbsp;</div><div><br></div><div>Какие требования предъявляются к современной интерьерной краске: – это возможность колероваться в различные цвета, должна разбавляться водой, быть стойкой к истираемости, и к влажной уборке, быть экологически безопасной. Это особенно важно, если Вы собираетесь красить стены в спальне и детской. Теперь давайте выберем краску, которая будет удовлетворять нашим требованиям.</div><div><br></div><div> Сначала расскажем, что такое современная, интерьерная краска. В настоящее время для покраски стен используют различные водорастворимые краски, это самый распространенный вариант красок. В состав этих красок входит много различных компонентов, каждый из которых отвечает за свою задачу. Одним из основных компонентов является связующая основа, она бывает 3 видов: поливинилацетатными (ПВА), акриловыми, бутадион (латексные).&nbsp;</div><div><br></div><div>Водоразбовимые краски на основе ПВА. Краски на этой основе являются самыми дешевыми, но имеет такие минусы, как слабая стойкость к истираемости и влажной уборке. Основная область ее применения это потолки, но потолки в сухих помещениях. Потолки в ваннах, кухнях или во влажных помещениях нельзя красить краской такого типа. Краски на основе ПВА боятся влаги! Еще одним из минусов красок такого типа является скудная палитра цветов, в которые ее можно отколеровать. Эти краски не колеруются на компьютерных колеровочных машинах. Колеровка красок на основе ПВА происходит вручную, путем добавлением в нее колеровочного пигмента. При таком методе колеровки можно получить только светлые цвета. И всего этого следует, что основная область использования красок на основе ПВА это покраска потолков в сухих помещениях, в белый или в светлые оттенки. Основным производителем этих красок являются отечественные компании, в ассортиментной линейке иностранных компаний они занимают небольшую часть.&nbsp;</div><div><br></div><div>Основными водоразбовимыми красками для покраски стен внутри помещений являются акриловые и латексные краски. Эти краски дают больше возможностей для творчества. Расскажем о них подробнее.&nbsp;</div><div><br></div><div>Акриловые краски.&nbsp;</div><div>Водоразбовимые краски на акриловой основе являются самыми популярными, эти краски имеют более широкий диапазон использования, значит ими можно красить не только потолок, но и стены. Они не боятся влажной уборки, имеют хорошую стойкость к истираемости – значит этой краской можно красить во влажных помещениях. Но не стоит забывать, что использование акриловой краски в местах, где возможно попадания на нее воды не рекомендуется, для этого есть другие материалы. Еще одним преимуществом акриловых красок перед красками на основе ПВА большой выбор цветов для колеровки, от светлых до темных, и возможность, колеровать краску, как ручным методом, так и с применением компьютеризированных колеровочных систем.&nbsp;</div><div><br></div><div>Если вы зайдете в любой строительный магазин, то на прилавке увидите большой ассортимент акриловых красок, даже в линейке одного производителя. Чем – же они отличаются? Зачем одному производителю выпускать несколько видов акриловой краски? Отличию заключаются в характеристиках краски. Краска, предназначенная для покраски потолка должна иметь белоснежный цвет и хорошую укрывистость, но такой краски совершенно не нужно иметь хорошую стойкость к мытью и ее нельзя колеровать в насыщенные цвета. Краска, предназначенная для покраски стен в спальнях, будет более стойкая к истираемости и ее можно отколеровать в большее количество цветов. Кроме этого существует много узкоспециализированных красок, предназначенных для покраски поверхностей, на которых есть жировые пятна, следы копоти и никотина.&nbsp;</div><div><br></div><div>При покраске стен акриловыми красками получается благородное матовое покрытие, а если необходимо получит покрытия с небольшим глянцем или необходимо покрасить стены с высоким износом? Для выполнения таких задач существуют латексные краски.</div><div><br></div><div> Латексные краски.&nbsp;</div><div>Еще одним продуктом лакокрасочной индустрии является латексные краски. Их основным преимуществом перед другими водорастворимыми красками является: повышенная стойкость к влажному истиранию, возможность получения шелковисто матовых поверхностей, и возможность покрывать основание тонким слоем. Все это дает возможность, производить окраску стен по обоям под покраску сохраняю их фактуру. Почему для этого лучше подходят латексные краски? По своему химическому составу латексные при высыхании образуют тонкую пленку, которая способна покрыть любую фактуру поверхности. По этому если выкупили обои под покраску или используете фактурную штукатурку, наилучшей краской будет – латексная краска, она придаст Вашей поверхности необходимый цвет и сохранит фактуру.&nbsp;</div><div><br></div><div>Латексные краски иностранных производителей разделяются на несколько видов. Различие заключается в степени глянца готовой поверхности, обычно это обозначается цифрой на этикетке или названием самой краски. Например, у компании Caparol есть латексные краски Samtex3, Samtex7, Samtex20. Латексная краска Samtex3 – является матовой; Samtex7 – шелковисто-матовой, а Samtex20 – шелковисто-глянцевой. Различие будет заключаться не только в во внешнем виде, но и в характеристиках поверхности, краска с большей степенью глянца является более стойкой к влажной уборке. Но у более глянцевой краски есть минус, она выявляет все неровности стены, поэтому если вы решили использовать такую краску, необходимо позаботиться о хорошей подготовки стены.</div><div><br></div><div> Экологические краски.&nbsp;</div><div>В последнее время в линейке каждого производителя появляются материалы, изготовленные с использованием экологически чистых компонентов, так называемые «Зеленые краски» эти материалы при работе с ними совершенно не выделяют вредных испарений. Эти краски рекомендуют использовать в помещениях, где будет жить ребенок или человек страдающий аллергией. Во многих европейских странах действует целый ряд законодательных и нормативных актов, ограничивающих применение материалов изготовленных с использованием вредных веществ, для внутренних работ. Особенно жесткие ограничения действуют в Швеции, Финляндии, Норвегии, Дании. Поэтому большинство серьезных производителей постепенно исключают вредные компоненты (пусть, даже если их влияние еще недостаточно изучено) из состава своей продукции. Во всяком случае, держа в руках краску для внутренних работ, изготовленную в Западной Европе или США, вы можете смело утверждать, что в ней отсутствуют ртуть, цинковые белила, свинец, соединения кадмия, хлорированные фенолы – словом, вещества, крайне негативно влияющие на самочувствие и здоровье человека. Большинство производителей, при производстве экологической серии краски, присваивают ей специальное обозначение, помогающее потребителю понять, что перед ним экологически безопасный продукт. Компания Caparol – экологически чистые краски, обозначает знаком E.L.F.&nbsp;</div><div><br></div><div>Колеровка краски.&nbsp;</div><div>Мало выбрать необходимую краску, ее еще надо отколеровать, придать ей необходимый Вам цвет. ....&nbsp;</div><div><br></div><div style=\"color:#cccccc\">Автор: Илья Сафронов \r\nИсточник: www.vsemoremonte.ru  </div>', '', '', ''), ('43', '22', '1', 'Оригинальный дизайн стен', '<div><img src=\"/uploads/blog/83417386_2.jpg\" style=\"width:699px;height:525px\"><br></div><div><br></div>Создать оригинальный дизайн невозможно без знания характерных черт различных стилевых направлений.\r\nВ общей стилистике декоративные стены являются всего лишь одним из элементов общего декора, но весьма важным.', 'Для Того, чтобы дизайн стен вписывался в общий стиль декоративного оформления зададим четкие контуры этих направлений:&nbsp;<div><br><div><strong>Античный стиль</strong> – благородство, величие, соразмерность человеческим пропорциям.&nbsp;</div><div><strong>Древнеегипетский стиль</strong> – монументальность, грандиозность, сложность, космичность, мотивы папируса, стеблей лотоса.&nbsp;</div><div><strong>Романский стиль</strong> – крепостная суровость, массивность, прямолинейность.&nbsp;</div><div><strong>Готический стиль</strong> – каркасность , динамика, экспрессия, стрельчатые формы.&nbsp;</div><div><strong>Ренессанс</strong> – организованность, ясность, симметрия, подражение античности.&nbsp;</div><div><strong>Классицизм</strong> – подражание античности, геометрия форм, симметрия, акцент на тектоник.</div><div><strong>Модерн</strong> – образная символика, орнаментальный ритм, стилизованный растительный узор, гибкость, текучие формы.&nbsp;</div><div><br></div><div>Японское направление:</div><div><br></div><div><strong>Фен-шуй </strong>– гармоничность, включенность в среду, близость к природе, символика философских категорий в архитектурных формах, естественность материалов.</div><div><br></div><div><strong>Средиземноморское направление</strong> – типичная южная итальянская атмосфера, открытые солнечные зоны (террасы, балконы, веранды), затененность внутреннего пространства, терракотовая теплая, природная цветовая гамма, использование природного камня, дерева, минеральных природных материалов.&nbsp;</div><div>Отделочные материалы фирмы «Хагери» позволят создать дизайн интерьера помещений в самом разнообразном стилевом спектре: античность, ренессанс, модерн, классика, хай тек и т.д. Предлагаемая декоративная отделка стен, поможет воспроизвести национальный дух того или иного народа, эпохи, направления в искусстве.  </div></div>', '', '', ''), ('45', '22', '10', null, null, null, null, null, null), ('46', '21', '10', null, null, null, null, null, null), ('47', '20', '10', null, null, null, null, null, null), ('48', '19', '10', null, null, null, null, null, null), ('49', '18', '10', null, null, null, null, null, null), ('50', '17', '10', null, null, null, null, null, null), ('51', '16', '10', null, null, null, null, null, null), ('52', '15', '10', null, null, null, null, null, null), ('53', '11', '10', null, null, null, null, null, null), ('54', '10', '10', null, null, null, null, null, null), ('55', '9', '10', null, null, null, null, null, null), ('56', '8', '10', null, null, null, null, null, null), ('57', '23', '1', 'МАСТЕР-КЛАСС 2010', '\"Ателье красок \"Домино\" проводит Мастер-класс по обучению технологиям нанесения итальянских декоративных отделочных материалов во всех своих салонах и представительствах.', 'Записаться в группы можно в салонах-магазинах \"Ателье красок \"Домино\" по адресам в соответствующих городах http://www.dominogroup.ru/contacts/\r\nКрасота, сотворенная руками человека, - что может быть чудеснее!\r\nУ Вас есть уникальная возможность приобщиться к прекрасному и научиться реализовывать свои творческие замыслы и дизайнерские идеи. \r\nПосетите Мастер- класс «Ателье красок «DOMINO», во время которого\r\nопытные специалисты научат Вас особенностям работы и техникам нанесения итальянских декоративных отделочных материалов.\r\nПо окончании семинара Вы получите Диплом и видеоматериалы по технологии нанесения, а также дисконтную карту сети салонов «Ателье красок «DOMINO».', '', '\"Ателье красок \"Домино\" проводит Мастер-класс по обучению технологиям нанесения итальянских декоративных отделочных материалов во всех своих салонах и представительствах.', '\"Ателье красок \"Домино\" проводит Мастер-класс по обучению технологиям нанесения итальянских декоративных отделочных материалов во всех своих салонах и представительствах.'), ('58', '23', '10', 'wer', 'werw', 'er', '', '', ''), ('59', '24', '1', 'Контакты', '', '<table>\r\n<tbody>\r\n<tr>\r\n<td>г. Москва<br>\r\n+7(495) 210-18-80<br>\r\n<a href=\"mailto:region@dominogroup.ru\">region@dominogroup.ru</a><br><br></td>\r\n<td>\r\nг. Краснодар - ул. Уральская, 13<br>\r\nРежим работы: пн-пт 9.00-18.00, сб 10.00-18.00, вс 10.00-15.00<br>\r\n+7 (861) 2344-010<br>\r\n<a href=\"mailto:u-lkm@dominogroup.ru\">u-lkm@dominogroup.ru</a><br><br>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>г. Грозный - ул. Б.Хмельницкого, 142<br>\r\n+7 (8712) 22-48-19<br>\r\n +7 928 892-0002<br>\r\n<a href=\"mailto:gr-lkm@dominogroup.ru\">gr-lkm@dominogroup.ru</a><br><br></td>\r\n<td>\r\nг. Пятигорск - ул. Ермолова, 16, стр. 1<br>\r\nРежим работы: пн-сб 9.00-18.00, вс 10.00-15.00<br>\r\n +7 (8793) 39-92-59<br>\r\n +7-988-096-80-08<br>\r\n <a href=\"mailto:dominokmw@gmail.com\">dominokmw@gmail.com</a><br><br>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>г. Тюмень - ул. Газовиков, 29/163<br>\r\n +7 (345) 220 22 98<br>\r\n +7 922 481-73-61<br>\r\n<a href=\"mailto:mironchuk@sibintel.ru\">mironchuk@sibintel.ru</a><br><br></td>\r\n<td>\r\nг. Воронеж - ул. Текстильщиков, 2<br>\r\n +7 (4732) 46 24 05\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>г. Сочи - Курортный проспект, 88<br>\r\nРежим работы: пн-пт 9.00-18.00, сб 10.00-18.00, вс 10.00-15.00<br>\r\n +7 (8622) 33-22-34<br>\r\n +7 (918) 111-98-38<br>\r\n +7 (8622) 41-21-55<br>\r\n<a href=\"mailto:s-lkm@dominogroup.ru\">s-lkm@dominogroup.ru</a><br><br></td>\r\n<td>\r\nг. Ростов-на-Дону - ул. Ларина, 15/2<br>\r\nРежим работы: пн-пт 9.00-18.00, сб 10.00-18.00<br>\r\n +7 (863) 2302-888<br>\r\n<a href=\"mailto:r-lkm@dominogroup.ru\">r-lkm@dominogroup.ru</a><br><br>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>г. Новороссийск - ул. Карла Маркса, 51<br>\r\n +7 8617 72-05-72<br>\r\n<a href=\"mailto:domino@utov.ru\">domino@utov.ru</a><br><br></td>\r\n<td>\r\nг. Пермь - ул. Полины Осипенко, 53<br>\r\n +7 (342) 244-00-86<br>\r\n +7-919-479-40-49<br>\r\n<a href=\"mailto:dominoperm@mail.ru\">dominoperm@mail.ru</a><br><br>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td>г. Екатеринбург - ул. Радищева, 33<br>\r\nСалон «Chocolate»<br>\r\n +7 922 291-79-63<br>\r\n<a href=\"mailto: suhanova1979@mail.ru\"> suhanova1979@mail.ru</a><br><br></td>\r\n<td>\r\n</td>\r\n</tr>\r\n</tbody></table>\r\n<iframe src=\"https://mapsengine.google.com/map/u/0/embed?mid=z-86wz5L5boE.kfCOhzAOPNsM\" width=\"100%\" height=\"480\"></iframe>', '', 'Контакты', 'Контакты'), ('60', '24', '10', 'Контакты', '', '<iframe src=\"https://mapsengine.google.com/map/u/0/embed?mid=z-86wz5L5boE.kfCOhzAOPNsM\" width=\"100%\" height=\"480\"></iframe>', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `Rights`
-- ----------------------------
DROP TABLE IF EXISTS `Rights`;
CREATE TABLE `Rights` (
  `itemname` varchar(64) NOT NULL,
  `type` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  PRIMARY KEY (`itemname`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `StoreAttribute`
-- ----------------------------
DROP TABLE IF EXISTS `StoreAttribute`;
CREATE TABLE `StoreAttribute` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `type` tinyint(4) DEFAULT NULL,
  `display_on_front` tinyint(1) DEFAULT '1',
  `use_in_filter` tinyint(1) DEFAULT NULL,
  `use_in_variants` tinyint(1) DEFAULT NULL,
  `use_in_compare` tinyint(1) DEFAULT '0',
  `select_many` tinyint(1) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `required` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `use_in_filter` (`use_in_filter`),
  KEY `display_on_front` (`display_on_front`),
  KEY `position` (`position`),
  KEY `use_in_variants` (`use_in_variants`),
  KEY `use_in_compare` (`use_in_compare`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreAttribute`
-- ----------------------------
BEGIN;
INSERT INTO `StoreAttribute` VALUES ('23', 'rashod', '1', '1', '1', '0', '0', '0', '0', '0'), ('22', 'fasovka', '1', '1', '1', '0', '0', '0', '0', '0');
COMMIT;

-- ----------------------------
--  Table structure for `StoreAttributeOption`
-- ----------------------------
DROP TABLE IF EXISTS `StoreAttributeOption`;
CREATE TABLE `StoreAttributeOption` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `position` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attribute_id` (`attribute_id`),
  KEY `position` (`position`)
) ENGINE=MyISAM AUTO_INCREMENT=185 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreAttributeOption`
-- ----------------------------
BEGIN;
INSERT INTO `StoreAttributeOption` VALUES ('182', '22', null), ('170', '23', null), ('169', '22', null), ('181', '23', null), ('173', '22', null), ('172', '23', null), ('168', '23', null), ('167', '22', null), ('166', '22', null), ('165', '23', null);
COMMIT;

-- ----------------------------
--  Table structure for `StoreAttributeOptionTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StoreAttributeOptionTranslate`;
CREATE TABLE `StoreAttributeOptionTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language_id` int(11) DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `value` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `language_id` (`language_id`),
  KEY `object_id` (`object_id`)
) ENGINE=MyISAM AUTO_INCREMENT=369 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreAttributeOptionTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StoreAttributeOptionTranslate` VALUES ('362', '10', '181', '2k'), ('346', '10', '173', '2'), ('338', '10', '169', '1л'), ('337', '1', '169', '1л'), ('345', '1', '173', '2'), ('336', '10', '168', '5-6'), ('335', '1', '168', '5-6'), ('344', '10', '172', '33 – 39'), ('334', '10', '167', '0.5л'), ('343', '1', '172', '33 – 39'), ('364', '10', '182', '3k'), ('363', '1', '182', '3k'), ('361', '1', '181', '2k'), ('340', '10', '170', '10 – 12'), ('339', '1', '170', '10 – 12'), ('333', '1', '167', '0.5л'), ('332', '10', '166', '0,5 л'), ('331', '1', '166', '0,5 л'), ('330', '10', '165', '3-4'), ('329', '1', '165', '3-4');
COMMIT;

-- ----------------------------
--  Table structure for `StoreAttributeTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StoreAttributeTranslate`;
CREATE TABLE `StoreAttributeTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreAttributeTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StoreAttributeTranslate` VALUES ('105', '23', '10', 'Расход, кв.м. / упаковка'), ('104', '23', '1', 'Расход, кв.м. / упаковка'), ('103', '22', '10', 'Фасовка'), ('102', '22', '1', 'Фасовка');
COMMIT;

-- ----------------------------
--  Table structure for `StoreCategory`
-- ----------------------------
DROP TABLE IF EXISTS `StoreCategory`;
CREATE TABLE `StoreCategory` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `lft` int(10) unsigned DEFAULT NULL,
  `rgt` int(10) unsigned DEFAULT NULL,
  `level` smallint(5) unsigned DEFAULT NULL,
  `url` varchar(255) DEFAULT '',
  `full_path` varchar(255) DEFAULT '',
  `layout` varchar(255) DEFAULT '',
  `view` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `lft` (`lft`),
  KEY `rgt` (`rgt`),
  KEY `level` (`level`),
  KEY `url` (`url`),
  KEY `full_path` (`full_path`)
) ENGINE=MyISAM AUTO_INCREMENT=300 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreCategory`
-- ----------------------------
BEGIN;
INSERT INTO `StoreCategory` VALUES ('1', '1', '32', '1', 'root', '', '', '', null), ('292', '16', '21', '2', 'kompyuteri', 'kompyuteri', '', '', ''), ('293', '22', '31', '2', 'kompyuternie-komplektuyushie', 'kompyuternie-komplektuyushie', '', '', ''), ('294', '17', '18', '3', 'noutbuki', 'kompyuteri/noutbuki', '', '', ''), ('295', '19', '20', '3', 'stacionarnie-kompyuteri', 'kompyuteri/stacionarnie-kompyuteri', '', '', ''), ('296', '23', '24', '3', 'hdd', 'kompyuternie-komplektuyushie/hdd', '', '', ''), ('297', '25', '26', '3', 'materinskie-plati', 'kompyuternie-komplektuyushie/materinskie-plati', '', '', ''), ('298', '27', '28', '3', 'opertivnaya-pamyat', 'kompyuternie-komplektuyushie/opertivnaya-pamyat', '', '', ''), ('288', '10', '15', '2', 'golovnie-ubori', 'golovnie-ubori', '', '', ''), ('289', '11', '12', '3', 'shlyapi', 'golovnie-ubori/shlyapi', '', '', ''), ('290', '13', '14', '3', 'raznie', 'golovnie-ubori/raznie', '', '', ''), ('299', '29', '30', '3', 'korpusa', 'kompyuternie-komplektuyushie/korpusa', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `StoreCategoryTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StoreCategoryTranslate`;
CREATE TABLE `StoreCategoryTranslate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` varchar(255) DEFAULT '',
  `meta_description` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=205 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreCategoryTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StoreCategoryTranslate` VALUES ('1', '1', '1', 'root', '', '', '', null), ('22', '219', '1', 'Планшеты', '', '', '', null), ('24', '220', '1', 'Ассортимент', '', '', '', null), ('193', '294', '1', 'Ноутбуки', '', '', '', ''), ('194', '294', '10', 'Ноутбуки', '', '', '', ''), ('192', '293', '10', 'Компьютерные комплектующие', '', '', '', ''), ('189', '292', '1', 'Компьютеры', '', '', '', ''), ('190', '292', '10', 'Компьютеры', '', '', '', ''), ('191', '293', '1', 'Компьютерные комплектующие', '', '', '', ''), ('198', '296', '10', 'HDD', '', '', '', ''), ('199', '297', '1', 'Материнские платы', '', '', '', ''), ('196', '295', '10', 'Стационарные компьютеры', '', '', '', ''), ('197', '296', '1', 'HDD', '', '', '', ''), ('181', '288', '1', 'Головные уборы', '', '', '', ''), ('182', '288', '10', 'Головные уборы', '', '', '', ''), ('183', '289', '1', 'Шляпы', '', '', '', ''), ('184', '289', '10', 'Шляпы', '', '', '', ''), ('195', '295', '1', 'Стационарные компьютеры', '', '', '', ''), ('186', '290', '10', 'Разные', '', '', '', ''), ('185', '290', '1', 'Разные', '', '', '', ''), ('202', '298', '10', 'Опертивная память', '', '', '', ''), ('200', '297', '10', 'Материнские платы', '', '', '', ''), ('201', '298', '1', 'Опертивная память', '', '', '', ''), ('133', '1', '10', null, null, null, null, null), ('203', '299', '1', 'Корпуса', '', '', '', ''), ('204', '299', '10', 'Корпуса', '', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `StoreCurrency`
-- ----------------------------
DROP TABLE IF EXISTS `StoreCurrency`;
CREATE TABLE `StoreCurrency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `iso` varchar(10) DEFAULT '',
  `symbol` varchar(10) DEFAULT '',
  `rate` float(10,3) DEFAULT NULL,
  `main` tinyint(1) DEFAULT NULL,
  `default` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreCurrency`
-- ----------------------------
BEGIN;
INSERT INTO `StoreCurrency` VALUES ('2', 'Рубли', 'RUR', 'руб.', '1.000', '1', '1');
COMMIT;

-- ----------------------------
--  Table structure for `StoreDeliveryMethod`
-- ----------------------------
DROP TABLE IF EXISTS `StoreDeliveryMethod`;
CREATE TABLE `StoreDeliveryMethod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `price` float(10,2) DEFAULT '0.00',
  `free_from` float(10,2) DEFAULT '0.00',
  `position` smallint(6) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `position` (`position`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreDeliveryMethod`
-- ----------------------------
BEGIN;
INSERT INTO `StoreDeliveryMethod` VALUES ('14', '10.00', '1000.00', '0', '1'), ('15', '0.00', '0.00', '1', '1'), ('16', '30.00', '0.00', '2', '1');
COMMIT;

-- ----------------------------
--  Table structure for `StoreDeliveryMethodTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StoreDeliveryMethodTranslate`;
CREATE TABLE `StoreDeliveryMethodTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreDeliveryMethodTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StoreDeliveryMethodTranslate` VALUES ('1', '14', '1', 'Курьером', ''), ('3', '15', '1', 'Самовывоз', ''), ('9', '16', '10', null, null), ('5', '16', '1', 'Адресная доставка по стране', ''), ('7', '14', '10', null, null), ('8', '15', '10', null, null);
COMMIT;

-- ----------------------------
--  Table structure for `StoreDeliveryPayment`
-- ----------------------------
DROP TABLE IF EXISTS `StoreDeliveryPayment`;
CREATE TABLE `StoreDeliveryPayment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `delivery_id` int(11) DEFAULT NULL,
  `payment_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COMMENT='Saves relations between delivery and payment methods ';

-- ----------------------------
--  Records of `StoreDeliveryPayment`
-- ----------------------------
BEGIN;
INSERT INTO `StoreDeliveryPayment` VALUES ('24', '12', '14'), ('23', '10', '16'), ('22', '10', '13'), ('21', '10', '14'), ('34', '11', '16'), ('33', '11', '13'), ('25', '12', '15'), ('26', '12', '16'), ('78', '14', '21'), ('77', '14', '19'), ('76', '14', '20'), ('75', '14', '18'), ('82', '15', '20'), ('81', '15', '18'), ('55', '16', '17'), ('56', '16', '18'), ('57', '16', '20'), ('58', '16', '19'), ('74', '14', '17');
COMMIT;

-- ----------------------------
--  Table structure for `StoreManufacturer`
-- ----------------------------
DROP TABLE IF EXISTS `StoreManufacturer`;
CREATE TABLE `StoreManufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT '',
  `layout` varchar(255) DEFAULT '',
  `view` varchar(255) DEFAULT '',
  `img` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `url` (`url`)
) ENGINE=MyISAM AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreManufacturer`
-- ----------------------------
BEGIN;
INSERT INTO `StoreManufacturer` VALUES ('24', 'viero', '', '', '1392142240.jpg'), ('23', 'elekta', '', '', '1392147565.jpg'), ('22', 'saif', '', '', '1392145222.jpg'), ('21', 'baldini', '', '', '1392143783.jpg'), ('20', 'maxmeyer', '', '', '1392144871.jpg');
COMMIT;

-- ----------------------------
--  Table structure for `StoreManufacturerTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StoreManufacturerTranslate`;
CREATE TABLE `StoreManufacturerTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  `meta_title` varchar(255) DEFAULT '',
  `meta_keywords` varchar(255) DEFAULT '',
  `meta_description` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=80 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreManufacturerTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StoreManufacturerTranslate` VALUES ('63', '21', '1', 'BALDINI', '', '', '', ''), ('65', '22', '1', 'SAIF', '', '', '', ''), ('67', '23', '1', 'ELEKTA', '', '', '', ''), ('69', '24', '1', 'VIERO', '', '', '', ''), ('76', '23', '10', null, null, null, null, null), ('77', '22', '10', null, null, null, null, null), ('78', '21', '10', null, null, null, null, null), ('79', '20', '10', null, null, null, null, null), ('75', '24', '10', null, null, null, null, null), ('61', '20', '1', 'MaxMeyer', '', '', '', '');
COMMIT;

-- ----------------------------
--  Table structure for `StorePaymentMethod`
-- ----------------------------
DROP TABLE IF EXISTS `StorePaymentMethod`;
CREATE TABLE `StorePaymentMethod` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `currency_id` int(11) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `payment_system` varchar(100) DEFAULT '',
  `position` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `currency_id` (`currency_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StorePaymentMethod`
-- ----------------------------
BEGIN;
INSERT INTO `StorePaymentMethod` VALUES ('17', '1', '0', 'webmoney', '0'), ('18', '2', '1', '', '2'), ('19', '1', '0', 'robokassa', '1'), ('20', '2', '1', '', '3'), ('21', '2', '0', 'qiwi', '4');
COMMIT;

-- ----------------------------
--  Table structure for `StorePaymentMethodTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StorePaymentMethodTranslate`;
CREATE TABLE `StorePaymentMethodTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `description` text,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StorePaymentMethodTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StorePaymentMethodTranslate` VALUES ('1', '17', '1', 'WebMoney', 'WebMoney — это универсальное средство для расчетов в Сети, целая среда финансовых взаимоотношений, которой сегодня пользуются миллионы людей по всему миру.'), ('2', '17', '9', 'English payment1', 'russian description'), ('3', '18', '1', 'Наличная', 'Наличная оплата осуществляется только в рублях при доставке товара курьером покупателю.'), ('7', '20', '1', 'Безналичная', ' Стоимость товара при безналичной оплате без ПДВ равна розничной цене товара + 2% '), ('8', '20', '9', 'Безналичная', ''), ('4', '18', '9', 'Наличка', '<p>ыла оылдао ылдао ылдоа ылдва<br />ыаол ывла оывалд ыова</p>'), ('5', '19', '1', 'Robokassa', 'Многими пользователями электронные платежные системы расцениваются в качестве наиболее удобного средства оплаты товаров и услуг в Интернете.'), ('6', '19', '9', 'Robokassa', '<p>Description goes here</p>'), ('9', '21', '1', 'Qiwi', 'Оплата с помощью Qiwi'), ('10', '21', '9', 'Qiwi', 'Оплата с помощью Qiwi');
COMMIT;

-- ----------------------------
--  Table structure for `StoreProduct`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProduct`;
CREATE TABLE `StoreProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturer_id` int(11) DEFAULT NULL,
  `type_id` int(11) DEFAULT NULL,
  `use_configurations` tinyint(1) NOT NULL DEFAULT '0',
  `url` varchar(255) NOT NULL,
  `price` float(10,2) DEFAULT NULL,
  `max_price` float(10,2) NOT NULL DEFAULT '0.00',
  `is_active` tinyint(1) DEFAULT NULL,
  `layout` varchar(255) DEFAULT NULL,
  `view` varchar(255) DEFAULT NULL,
  `sku` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT '0',
  `availability` tinyint(2) DEFAULT '1',
  `auto_decrease_quantity` tinyint(2) DEFAULT '1',
  `views_count` int(11) unsigned DEFAULT '0',
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  `added_to_cart_count` int(11) unsigned DEFAULT '0',
  `votes` int(11) unsigned DEFAULT '0',
  `rating` int(11) unsigned DEFAULT NULL COMMENT '0',
  `discount` varchar(255) DEFAULT NULL,
  `video` text,
  `is_index` tinyint(3) unsigned DEFAULT NULL,
  `small_wholesale` decimal(10,2) DEFAULT NULL,
  `wholesale` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `manufacturer_id` (`manufacturer_id`),
  KEY `type_id` (`type_id`),
  KEY `price` (`price`),
  KEY `max_price` (`max_price`),
  KEY `is_active` (`is_active`),
  KEY `sku` (`sku`),
  KEY `created` (`created`),
  KEY `updated` (`updated`),
  KEY `added_to_cart_count` (`added_to_cart_count`),
  KEY `views_count` (`views_count`)
) ENGINE=MyISAM AUTO_INCREMENT=585 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreProduct`
-- ----------------------------
BEGIN;
INSERT INTO `StoreProduct` VALUES ('580', null, '7', '0', 'testovii-produkt', '54.00', '0.00', '1', '', '', '', '0', '1', '1', '3', '2014-07-30 15:02:49', '2014-08-07 12:52:33', '3', '0', null, '', null, '0', '32.00', '25.00'), ('581', null, '7', '0', 'testovii-pro-2', '1.00', '0.00', '1', '', '', '', '0', '1', '1', '1', '2014-07-30 15:03:45', '2014-08-07 12:52:28', '0', '0', null, '', null, '0', '134.00', '43.00'), ('582', null, '7', '0', 'produkt-3', '65.00', '0.00', '1', '', '', '', '0', '1', '1', '34', '2014-07-30 15:06:30', '2014-08-07 12:52:22', '2', '0', null, '', null, '0', '65.00', '24.00'), ('583', null, '7', '0', '123', '50.00', '0.00', '1', '', '', '', '0', '1', '1', '33', '2014-08-04 11:12:10', '2014-08-19 16:53:06', '0', '0', null, '', null, '0', '34.00', '23.00'), ('584', null, '7', '0', 'shapka-s-usami', '56.00', '0.00', '1', null, null, null, '0', '1', '1', '6', '2014-08-07 12:56:17', '2014-08-07 12:56:27', '0', '0', null, null, null, '0', '45.00', '23.00');
COMMIT;

-- ----------------------------
--  Table structure for `StoreProductAttributeEAV`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductAttributeEAV`;
CREATE TABLE `StoreProductAttributeEAV` (
  `entity` int(11) unsigned NOT NULL,
  `attribute` varchar(250) DEFAULT '',
  `value` text,
  KEY `ikEntity` (`entity`),
  KEY `attribute` (`attribute`),
  KEY `value` (`value`(50))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `StoreProductCategoryRef`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductCategoryRef`;
CREATE TABLE `StoreProductCategoryRef` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product` int(11) DEFAULT NULL,
  `category` int(11) DEFAULT NULL,
  `is_main` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  KEY `product` (`product`),
  KEY `is_main` (`is_main`)
) ENGINE=MyISAM AUTO_INCREMENT=1314 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreProductCategoryRef`
-- ----------------------------
BEGIN;
INSERT INTO `StoreProductCategoryRef` VALUES ('1310', '582', '289', '1'), ('1311', '581', '289', '1'), ('1309', '583', '289', '1'), ('1313', '584', '290', '1'), ('1312', '580', '289', '1');
COMMIT;

-- ----------------------------
--  Table structure for `StoreProductConfigurableAttributes`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductConfigurableAttributes`;
CREATE TABLE `StoreProductConfigurableAttributes` (
  `product_id` int(11) NOT NULL COMMENT 'Attributes available to configure product',
  `attribute_id` int(11) NOT NULL,
  UNIQUE KEY `product_attribute_index` (`product_id`,`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `StoreProductConfigurations`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductConfigurations`;
CREATE TABLE `StoreProductConfigurations` (
  `product_id` int(11) NOT NULL COMMENT 'Saves relations beetwen product and configurations',
  `configurable_id` int(11) NOT NULL,
  UNIQUE KEY `idsunique` (`product_id`,`configurable_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `StoreProductImage`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductImage`;
CREATE TABLE `StoreProductImage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `is_main` tinyint(1) DEFAULT '0',
  `uploaded_by` int(11) DEFAULT NULL,
  `date_uploaded` datetime DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=249 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreProductImage`
-- ----------------------------
BEGIN;
INSERT INTO `StoreProductImage` VALUES ('246', '581', '581_3210055690.jpg', '1', '1', '2014-08-07 12:50:31', ''), ('247', '584', '584_1014915358.jpg', '1', '1', '2014-08-07 12:56:17', ''), ('248', '584', '584_540693660.jpg', '0', '1', '2014-08-07 12:56:27', null), ('244', '582', '582_3998464189.jpg', '1', '1', '2014-08-07 12:48:45', ''), ('245', '580', '580_1490430502.jpg', '1', '1', '2014-08-07 12:50:07', ''), ('243', '583', '583_1267350578.jpg', '1', '1', '2014-08-07 12:46:41', '');
COMMIT;

-- ----------------------------
--  Table structure for `StoreProductTranslate`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductTranslate`;
CREATE TABLE `StoreProductTranslate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `language_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT '',
  `short_description` text,
  `full_description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keywords` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `object_id` (`object_id`),
  KEY `language_id` (`language_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1439 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreProductTranslate`
-- ----------------------------
BEGIN;
INSERT INTO `StoreProductTranslate` VALUES ('1429', '580', '1', 'Шляпа №4', 'Краткое описание', 'Полное описание', '', '', ''), ('1430', '580', '10', 'Тестовый продукт', 'Краткое описание', 'Полное описание', '', '', ''), ('1431', '581', '1', 'Шляпа №3', 'Краткое описание', 'Полное описание', '', '', ''), ('1432', '581', '10', 'Тестовый про №2', 'Краткое описание', 'Полное описание', '', '', ''), ('1433', '582', '1', 'Шляпа №2', 'Краткое описание', 'Полное описание', '', '', ''), ('1434', '582', '10', 'Продукт №3', 'Краткое описание', 'Полное описание', '', '', ''), ('1435', '583', '1', 'Шляпа', 'Краткое описание', 'Полное описание', '', '', ''), ('1436', '583', '10', '123', '', '', '', '', ''), ('1437', '584', '1', 'Шапка с усами', 'Полное описание\r\nШапка с усами', 'Полное описание.\r\n\r\nШапка с усами', null, null, null), ('1438', '584', '10', 'Шапка с усами', 'Полное описание\r\nШапка с усами', 'Полное описание.\r\n\r\nШапка с усами', null, null, null);
COMMIT;

-- ----------------------------
--  Table structure for `StoreProductType`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductType`;
CREATE TABLE `StoreProductType` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `categories_preset` text,
  `main_category` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreProductType`
-- ----------------------------
BEGIN;
INSERT INTO `StoreProductType` VALUES ('7', 'Общий тип', null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `StoreProductVariant`
-- ----------------------------
DROP TABLE IF EXISTS `StoreProductVariant`;
CREATE TABLE `StoreProductVariant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `attribute_id` int(11) DEFAULT NULL,
  `option_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `price` float(10,2) DEFAULT '0.00',
  `price_type` tinyint(1) DEFAULT NULL,
  `sku` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `attribute_id` (`attribute_id`),
  KEY `option_id` (`option_id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=298 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `StoreRelatedProduct`
-- ----------------------------
DROP TABLE IF EXISTS `StoreRelatedProduct`;
CREATE TABLE `StoreRelatedProduct` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `related_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=utf8 COMMENT='Handle related products';

-- ----------------------------
--  Records of `StoreRelatedProduct`
-- ----------------------------
BEGIN;
INSERT INTO `StoreRelatedProduct` VALUES ('65', '583', '580'), ('64', '583', '581'), ('63', '583', '582');
COMMIT;

-- ----------------------------
--  Table structure for `StoreTypeAttribute`
-- ----------------------------
DROP TABLE IF EXISTS `StoreTypeAttribute`;
CREATE TABLE `StoreTypeAttribute` (
  `type_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  PRIMARY KEY (`type_id`,`attribute_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreTypeAttribute`
-- ----------------------------
BEGIN;
INSERT INTO `StoreTypeAttribute` VALUES ('7', '22'), ('7', '23');
COMMIT;

-- ----------------------------
--  Table structure for `StoreWishlist`
-- ----------------------------
DROP TABLE IF EXISTS `StoreWishlist`;
CREATE TABLE `StoreWishlist` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(10) DEFAULT '',
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `key` (`key`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `StoreWishlist`
-- ----------------------------
BEGIN;
INSERT INTO `StoreWishlist` VALUES ('1', 'l5gdnwcxac', '1'), ('2', 'tnth8iek95', '4');
COMMIT;

-- ----------------------------
--  Table structure for `StoreWishlistProducts`
-- ----------------------------
DROP TABLE IF EXISTS `StoreWishlistProducts`;
CREATE TABLE `StoreWishlistProducts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wishlist_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `wishlist_id` (`wishlist_id`),
  KEY `product_id` (`product_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `SystemLanguage`
-- ----------------------------
DROP TABLE IF EXISTS `SystemLanguage`;
CREATE TABLE `SystemLanguage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `code` varchar(25) DEFAULT '',
  `locale` varchar(100) DEFAULT '',
  `default` tinyint(1) DEFAULT NULL,
  `flag_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `code` (`code`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `SystemLanguage`
-- ----------------------------
BEGIN;
INSERT INTO `SystemLanguage` VALUES ('1', 'Русский', 'ru', 'ru', '1', 'ru.png'), ('10', 'Английский', 'en', 'en_us', '0', 'england.png');
COMMIT;

-- ----------------------------
--  Table structure for `SystemModules`
-- ----------------------------
DROP TABLE IF EXISTS `SystemModules`;
CREATE TABLE `SystemModules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT '',
  `enabled` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=59 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `SystemModules`
-- ----------------------------
BEGIN;
INSERT INTO `SystemModules` VALUES ('7', 'users', '1'), ('9', 'pages', '1'), ('11', 'core', '1'), ('12', 'rights', '1'), ('16', 'orders', '1'), ('15', 'store', '1'), ('17', 'comments', '1'), ('37', 'feedback', '1'), ('38', 'discounts', '1'), ('39', 'newsletter', '1'), ('40', 'csv', '1'), ('41', 'logger', '1'), ('52', 'accounting1c', '1'), ('53', 'yandexMarket', '1'), ('54', 'notifier', '1'), ('55', 'statistics', '1'), ('56', 'sitemap', '1'), ('58', 'news', '1');
COMMIT;

-- ----------------------------
--  Table structure for `SystemSettings`
-- ----------------------------
DROP TABLE IF EXISTS `SystemSettings`;
CREATE TABLE `SystemSettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category` varchar(255) DEFAULT '',
  `key` varchar(255) DEFAULT '',
  `value` text,
  PRIMARY KEY (`id`),
  KEY `category` (`category`),
  KEY `key` (`key`)
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `SystemSettings`
-- ----------------------------
BEGIN;
INSERT INTO `SystemSettings` VALUES ('9', 'feedback', 'max_message_length', '1000'), ('8', 'feedback', 'enable_captcha', '1'), ('7', 'feedback', 'admin_email', 'admin@localhost.local'), ('10', '17_WebMoneyPaymentSystem', 'LMI_PAYEE_PURSE', 'Z123456578811'), ('11', '17_WebMoneyPaymentSystem', 'LMI_SECRET_KEY', 'theSercretPassword'), ('12', '18_WebMoneyPaymentSystem', 'LMI_PAYEE_PURSE', '1'), ('13', '18_WebMoneyPaymentSystem', 'LMI_SECRET_KEY', '2'), ('14', '19_RobokassaPaymentSystem', 'login', 'login'), ('15', '19_RobokassaPaymentSystem', 'password1', 'password1value'), ('16', '19_RobokassaPaymentSystem', 'password2', 'password2value'), ('22', 'accounting1c', 'password', 'f880fefe2aff8130bb31d480f08e47c03e655b60'), ('21', 'accounting1c', 'ip', '127.0.0.1'), ('23', 'accounting1c', 'tempdir', 'application.runtime'), ('24', 'yandexMarket', 'name', 'Демо магазин'), ('25', 'yandexMarket', 'company', 'Демо кампания'), ('26', 'yandexMarket', 'url', 'http://demo-company.loc/'), ('27', 'yandexMarket', 'currency_id', '2'), ('28', 'core', 'siteName', 'pozitivmag.ru'), ('29', 'core', 'productsPerPage', '12,18,24'), ('30', 'core', 'productsPerPageAdmin', '30'), ('31', 'core', 'theme', 'default'), ('32', '20_QiwiPaymentSystem', 'shop_id', ''), ('33', '20_QiwiPaymentSystem', 'password', ''), ('34', '21_QiwiPaymentSystem', 'shop_id', '211182'), ('35', '21_QiwiPaymentSystem', 'password', 'xsi100500'), ('36', 'core', 'editorTheme', 'compant'), ('37', 'core', 'editorHeight', '150'), ('38', 'core', 'editorAutoload', '0'), ('39', 'images', 'path', 'webroot.uploads.product'), ('40', 'images', 'thumbPath', 'webroot.assets.productThumbs'), ('41', 'images', 'url', '/uploads/product/'), ('42', 'images', 'thumbUrl', '/assets/productThumbs/'), ('43', 'images', 'maxFileSize', '10485760'), ('44', 'images', 'max_sizes', '1800х1600'), ('45', 'images', 'maximum_image_size', '1935x947'), ('46', 'images', 'watermark_image', ''), ('47', 'images', 'watermark_active', '1'), ('48', 'images', 'watermark_position', 'bottomRight'), ('49', 'images', 'watermark_position_vertical', 'bottom'), ('50', 'images', 'watermark_position_horizontal', 'right'), ('51', 'images', 'watermark_opacity', '100'), ('52', 'status', 'site', '0');
COMMIT;

-- ----------------------------
--  Table structure for `accounting1c`
-- ----------------------------
DROP TABLE IF EXISTS `accounting1c`;
CREATE TABLE `accounting1c` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `object_id` int(11) DEFAULT NULL,
  `object_type` int(11) DEFAULT NULL,
  `external_id` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `object_type` (`object_type`),
  KEY `external_id` (`external_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `grid_view_filter`
-- ----------------------------
DROP TABLE IF EXISTS `grid_view_filter`;
CREATE TABLE `grid_view_filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `grid_id` varchar(100) DEFAULT '',
  `name` varchar(100) DEFAULT '',
  `data` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `grid_view_filter`
-- ----------------------------
BEGIN;
INSERT INTO `grid_view_filter` VALUES ('1', '1', 'loggerListGrid', '1', '{\"ActionLog[model_name]\":\"StoreProduct\",\"ActionLog[id]\":\"\",\"ActionLog[username]\":\"\",\"ActionLog[event]\":\"\",\"ActionLog[model_title]\":\"\",\"ActionLog[datetime]\":\"\"}');
COMMIT;

-- ----------------------------
--  Table structure for `notifications`
-- ----------------------------
DROP TABLE IF EXISTS `notifications`;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_id` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Table structure for `tbl_migration`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_migration`;
CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `tbl_migration`
-- ----------------------------
BEGIN;
INSERT INTO `tbl_migration` VALUES ('m000000_000000_base', '1361214193'), ('m130218_190341_add_description_to_store_category', '1361214373'), ('m130218_190818_add_description_to_store_category_translate', '1361214547'), ('m130420_194012_add_roles_to_discounts', '1366487103'), ('m130420_204956_add_personal_discount_to_user', '1366491054'), ('m130421_095545_add_personal_discount_to_product', '1366538394'), ('m130504_170119_add_discout_to_order', '1367686940'), ('m130504_183903_add_title_to_store_product_image', '1367692811'), ('m130507_103455_add_banned_to_user', '1367923070'), ('m130507_104810_unban_all_users', '1367923771'), ('m130624_155800_add_new_fields_to_product', '1372089566'), ('m130714_114907_add_admin_comment_to_orders', '1373802668'), ('m130726_042212_create_order_history_table', '1374814430');
COMMIT;

-- ----------------------------
--  Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT '',
  `password` varchar(255) DEFAULT '',
  `email` varchar(255) DEFAULT '',
  `created_at` datetime DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT NULL,
  `recovery_key` varchar(20) DEFAULT NULL,
  `recovery_password` varchar(100) DEFAULT NULL,
  `discount` varchar(255) DEFAULT NULL,
  `banned` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Saves user accounts';

-- ----------------------------
--  Records of `user`
-- ----------------------------
BEGIN;
INSERT INTO `user` VALUES ('1', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'admin@admin.ru', '2014-01-15 09:12:43', '2014-08-20 18:20:57', '86.57.189.55', null, null, null, '0'), ('2', 'mark', 'd37c5a7d2de8b5b598e7138644eed10eab066a12', 'mark.moskalenko@gmail.com', '2014-02-21 12:10:16', '2014-02-21 12:10:16', '127.0.0.1', '', '', null, '0'), ('3', 'markmoskalenko', '6970c722fc6f000e5b46b7f303f20275a712ceed', 'mark.moskalenko1@gmail.com', '2014-03-25 12:50:40', '2014-03-25 12:51:50', '127.0.0.1', null, null, null, '0'), ('4', 'test', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'test@gmail.com', '2014-05-19 12:55:02', '2014-05-19 12:55:02', '127.0.0.1', null, null, null, '0'), ('5', 'test2', '7c4a8d09ca3762af61e59520943dc26494f8941b', '1mark.moskalenko@gmail.com', '2014-05-19 15:08:28', '2014-05-19 15:08:28', '127.0.0.1', null, null, null, '0');
COMMIT;

-- ----------------------------
--  Table structure for `user_profile`
-- ----------------------------
DROP TABLE IF EXISTS `user_profile`;
CREATE TABLE `user_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `full_name` varchar(255) DEFAULT '',
  `phone` varchar(20) DEFAULT NULL,
  `delivery_address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
--  Records of `user_profile`
-- ----------------------------
BEGIN;
INSERT INTO `user_profile` VALUES ('1', '1', 'admin', null, null), ('2', '2', 'Mark Olegovich Moskalenko', '6230101', 'GRODNO, zapreydnaya 35'), ('3', '3', 'Mark Olegovich Moskalenko', '6230101', 'GRODNO, zapreydnaya 35'), ('4', '4', 'Дмитрий Иванович', '6230101', 'г. Гродно ул Запрудная д 35'), ('5', '5', 'Mark Olegovich Moskalenko', '6230101', 'GRODNO, zapreydnaya 35');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
