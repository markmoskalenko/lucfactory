<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = \Yii::t('app','Пополнение баланса');
?>

<section class="yandexMoney-pay">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div class="bgmainb2" id="scrollheight">
                <h2>Оплата в автоматическом режиме (Средства зачисляются сразу):</h2>
                <a href="<?= Url::to('/yandex/pay') ?>"><img src="img/pop_yandex.png" alt="Яндекс Деньги"/></a>
                <a href="<?= Url::to('/payeer') ?>"><img src="img/pop_payeer.png" alt="Payeer"/></a>
                <h2>Оплата в ручном режиме (Cредства зачисляются после создания тикета, в течении суток):</h2>
                <a href="<?= Url::to('webmoney/rules') ?>"><img src="img/pop_merchant.png" alt="WebMoney"/></a>
                <br/>
                <b style="  color:red">После перевода средств необходимо создать
                    <a href="<?= Url::to('/webmoney/create') ?>" target="_blank">тикет</a>, в
                    <a href="<?= Url::to('/webmoney/create') ?>" target="_blank">тикете</a>
                    пишите - номер своего кошелька, cумму и время. Иначе, деньги НЕ БУДУТ зачислены!!!<br>
                    Если администрация просит вас прислать фото (скриншот, скрин) чека, то делаем фото или
                    <a href="https://ru.wikipedia.org/wiki/%D0%A1%D0%BD%D0%B8%D0%BC%D0%BE%D0%BA_%D1%8D%D0%BA%D1%80%D0%B0%D0%BD%D0%B0" target="_blank">скриншот</a>, загружаем его на сайт <a href="http://funkyimg.com" target="_blank">funkyimg.com</a> и полученную ссылку пишем в
                    <a href="<?= Url::to('/webmoney/create') ?>" target="_blank">тикете</a>.
                </b>
            </div>
        </div>
</section>