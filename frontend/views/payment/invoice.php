<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;


/**
 * @var frontend\controllers\ProfileController $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\User $model
 */

$this->title = \Yii::t('app','Пополнение баланса');

?>

<section class="invoice">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <h1><?= Html::encode(\Yii::t('app',$this->title)) ?></h1>
<!--            <div id="accound_form" class="row">-->
<!---->
<!--                <div class="col-md-10 col-md-offset-2">-->
<!---->
<!--                    <div class="accoundForm_1 accound_register">-->
<!--                        <p style="color:red;">-->
<!--                            --><?//= Yii::$app->session->getFlash('error'); ?>
<!--                        </p>-->
<!---->
<!--                        --><?php //$form = ActiveForm::begin(['id' => 'invoice-form']); ?>
<!--                        <div id="input_sum" class="col-md-12">-->
<!--                            --><?//= $form->field($model, 'sum')->textInput(['placeholder' => \Yii::t('app', 'Сумма пополнения')]) ?>
<!--                        </div>-->
<!--                        <div id="add" class="text-center">-->
<!--                            --><?//= Html::submitButton(\Yii::t('app','Пополнить'), ['class' => 'btn btn-primary btn-lg add', 'name' => 'login-button']) ?>
<!--                        </div>-->
<!--                        --><?php //ActiveForm::end(); ?>
<!---->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
            <p>Ввод средств позволяет приобрести USD с помощью различных платежных систем: Yandex Деньги, банковских карт, SMS, терминалов и т.д. Оплата и зачисление USD на баланс производится в автоматическом режиме.</p>
            <div style="clear:both; margin-top: 8px; margin-bottom: 14px; text-align: center;">Курс игровой валюты: <b>1 Руб. = 100 USD</b></div>
            <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                <form action="" method="post">
                    <tr>
                        <input type="hidden" name="m" value="" />
                        <td  align="right" valign="top" style="padding-top: 10px;" width="330">
                            Введите сумму для пополнения (Руб.):
                        </td>
                        <td align="left" valign="top" style="padding-top: 10px; padding-left: 10px;">
                            <input type="text" class="b_textbox" name="sum" id="sum" value="1000" onkeyup="GetSumIns();" style="margin: 0px; width: 60px;" />
                        </td>
                    </tr>
                    <tr>
                        <td  align="right" valign="top" style="padding-top: 10px;" width="330">
                            Вы получите USD:
                        </td>
                        <td align="left" valign="top" style="padding-top: 10px; padding-left: 10px;">
                            <span id="res_sum" name="res">0.00</span>
                            <input type="hidden" name="per" id="convert" value="100" disabled="disabled" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" style="padding-left: 275px; padding-top: 10px;" height="80">
                            <input type="submit" id="submit" value="Пополнить баланс" style="height: 30px; margin-top: 10px;" />
                        </td>
                    </tr>
                    <tr>
                        <td valign="top" colspan="2" height="134">
                            <div style="text-align: center;">Ссылки на учебные материалы: <a href="#" target="_blank">создание счета в Payeer</a> и <a href="#" target="_blank">вывод средств из Payeer</a>.</div><br />
                            <p><i>Если вы не находите нужного способа пополнения, то создайте аккаунт в системе Payeer, он вам все-равно понадобится чтобы выводить заработанные деньги. После того как создадите кошелек в системе Payeer, вы сможете его пополнить 150 способами, среди которых есть ЯД, Qiwi и другие. А при пополнении аккаунта в игре просто выбираете способ пополнения "с кошелька Payeer", за который не взимаются проценты системы.</i></p>
                            <p><i>Если вы хотите пополнить с WebMoney или других платежных систем, пополнение которых невозможно напрямую или через аккаунт Payeer, то можно сделать пополнение через промежуточные системы. Для примера, можно попробовать завести единый кошелек W1, пополнить его с WebMoney, а далее сделать пополнения своего кошелька Payeer c W1, после чего можно будет пополнить игру с вашего кошелька Payeer.</i></p>
                        </td>
                    </tr>
                </form>
            </table>
            <script language="javascript">GetSumIns();</script>

        </div>
    </div>



</section>
