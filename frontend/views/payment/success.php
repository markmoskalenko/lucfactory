<?php
use yii\helpers\Html;

$this->title = \Yii::t('app','Успешное поплнение баланса');
?>

<section class="yandexMoney-pay">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <h1><?= Html::encode(\Yii::t('app',$this->title)) ?></h1>
            <div class="bgmainb2" id="scrollheight">
                <p>Пополнение баланса прошло успешно.</p>
                <p>Ваш баланс:<strong> <?= number_format($model->in_balance,0,'.',' ')?> USD</strong></p>
        </div>
    </div>
</section>