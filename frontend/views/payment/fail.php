<?php
use yii\helpers\Html;

$this->title = \Yii::t('app','Ошибка пополнения баланса');
?>

<section class="yandexMoney-pay">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <h1><?= Html::encode(\Yii::t('app',$this->title)) ?></h1>
            <div class="bgmainb2" id="scrollheight">
                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <p><?= Yii::$app->session->getFlash('error') ?></p>
                <?php else: ?>
                    <p>Ваш баланс не был пополнен. Если возникли вопросы, Вы можете обратиться в тех. поддержку <a href="mailto:<?= Yii::$app->params['supportEmail'] ?>"><?= Yii::$app->params['supportEmail'] ?></a></p>
                <?php endif; ?>
            </div>
        </div>
</section>