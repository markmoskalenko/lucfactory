<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 16:07
 */

use yii\helpers\Html;

$this->title = 'Заказать выплату';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">
            <p>Выплаты осуществляются в ручном режиме. Из платежной системы вы можете вывести свои средства на счета любых распространенных платежных систем и международных банков.<p>
            <div style="clear:both; margin-top: 8px; margin-bottom: 14px; text-align: center;">Ссылки на учебные материалы: <a href="http://payeeer.ru/create"  style="color: #2ac0ff" target="_blank">создание счета в Payeer</a> и <a href="https://payeer.com/ru/withdraw/" style="color: #2ac0ff"  target="_blank">вывод средств из Payeer</a>.</div>
            <div style="clear:both; margin-top: 8px; margin-bottom: 14px; text-align: center; color: red;" id="error" class="h-title"><b><font color="#000">Заказ выплаты</font></b></div>
            <script type="text/javascript">document.getElementById('error').innerHTML = '<br /><br /><b>Вывод денежных средств станет доступен только после пополнения баланса не менее чем на 150 RUB. Платёжных баллов у нас нет!</b>';</script>
        </div>
        <div style="clear:both;"></div>
    </div>
</div>