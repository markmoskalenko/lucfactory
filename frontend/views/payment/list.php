<?php

use yii\helpers\Html;
use yii\grid\GridView;

//ProfileAsset::register($this);
$this->title = 'Выплаты';

/* @var $this yii\web\View */
/* @var $searchModel common\models\Invoice */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>


<section class="invoice">
    <div class="accoundContent" ng-app="profileApp">
        <div id="accountDetails" class="container">
            <div id="accound_form" class="row">
                <h1><?= Html::encode($this->title) ?></h1>
                <hr/>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'layout'=>'{items}',
                        'columns' => [
                            [
                                'attribute' => 'user_id',
                                'value' => function($value){
                                    return $value->user->username;
                                }
                            ],
                            [
                                'attribute' =>  'sum',
                                'value' => function($value){
                                    return   $value->sum.' RUB';
                                }
                            ],
                            [
                                'attribute' =>  'purse',
                                'format' =>'html',
                                'value' => function($value){
                                    return  mb_substr($value->purse, 0,5).' <span style="color:red">XXX</span> ';
                                }
                            ],
                            [
                                'attribute' =>  'created_at',
                                'value' => function($value){
                                    return  $value->created_at ;
                                }
                            ],
                        ],
                    ]); ?>
            </div>
        </div>
</section>
