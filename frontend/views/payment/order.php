<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 06.11.2014
 * Time: 14:21
 */
use frontend\widgets\Alert;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
<div id="left_wrapper">
    <div class="review">
        <?= $this->render('/partials/_profileMenu'); ?>
        <div style="clear: both"></div>
    </div>
    <!-- Right wrapper end -->
</div>
<div class="right_wrapper">
    <p>Выплаты осуществляются в ручном режиме. Из платежной системы вы можете вывести свои средства на счета любых распространенных платежных систем и международных банков.<p>
    <div style="clear:both; margin-top: 8px; margin-bottom: 14px; text-align: center;">Ссылки на учебные материалы: <a href="http://payeeer.ru/create"  style="color: #2ac0ff" target="_blank">создание счета в Payeer</a> и <a href="https://payeer.com/ru/withdraw/" style="color: #2ac0ff"  target="_blank">вывод средств из Payeer</a>.</div>
    <div style="clear:both; margin-top: 8px; margin-bottom: 14px; text-align: center; color: red;" id="error" class="h-title"><b><font color="#000">Заказ выплаты</font></b></div>
</div>

<?= Alert::widget();?>

<?php $form = ActiveForm::begin(['id' => 'payment-form']); ?>
<div id="input_purse" class="col-md-12" >

    <?= $form->field($model, 'purse')->textInput(['placeholder' => 'P1523XXX', 'class'=>'purse']) ?>
</div>
<div id="input_sum" class="col-md-12">
    <?= $form->field($model, 'sum')->textInput(['placeholder' => 'Сумма выплаты',  'class'=>'sum']) ?>
</div>

<div id="form_payment_btn">
    <?= Html::submitButton(\Yii::t('app','Заказать'), ['class' => 'btn btn-primary btn-lg']) ?>
</div>
<?php ActiveForm::end(); ?>


<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'=>'{items}',
    'columns' => [
        [
            'label'=>'USD',
            'attribute' =>  'sum',
            'value' => function($value){
                return   $value->sum;
            }
        ],
        [
            'attribute' => 'user_id',
            'value' => function($value){
                return $value->user->username;
            }
        ],

        [
            'attribute' =>  'purse',
            'value' => function($value){
                return  $value->purse;
            }
        ],
        [
            'attribute' =>  'created_at',
            'value' => function($value){
                return  $value->created_at ;
            }
        ],
        [
            'label'=>'Статус',
            'attribute' =>  'status',
            'value' => function($value){
                return  ($value->status) ? 'Выплачено':'Обрабатывается';
            }
        ],
    ],
]); ?>