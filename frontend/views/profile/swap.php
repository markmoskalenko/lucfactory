<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 16:08
 */
use frontend\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Обменик';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">
            <p> В игре существует два счета на которые поступает выручка за проданное топливо - счет с которого можно только выводить (конвертируя USD в реальные деньги) и счет предназначенный для покупок внутри системы. В обменном пункте вы можете обменять USD для вывода на USD для покупок. Обмен возможен только в одну сторону. При обмене между счетами вы получаете +30% на счет для покупок. Обменная операция дает возможность реинвестировать большую сумму денег, чем вы имеете в данный момент на счете для вывода.</p>
            <div style="clear:both;"></div>
            <?= Alert::widget();?>
            <?php $form = ActiveForm::begin(['id' => 'change-form']); ?>
            <div>  <p class="str" style="width: 200px;margin-top: 10px">Со счета на вывод (мин. 1000):</p><?= $form->field($oUser, 'swap')->textInput( ['class'=>'form-input-swap',  'onkeypress'=>"counter(this)", 'onkeyup'=>"counter(this)", 'onchange'=>"counter(this)"])?></div>
            <div class="button" style="margin-left: 47%;"><?= Html::submitButton(\Yii::t('app','Обменять'), ['class' => 'btn'])?></div>
            <div style="clear:both;"></div>
            <p class="str" style="width: 400px">На счет для покупок (+30%): <span id="res_sum" name="res">0</span></p>


            <?php ActiveForm::end(); ?>

            <script language="javascript">

            </script>
            <br />
        </div>
    </div>
</div>