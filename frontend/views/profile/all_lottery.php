<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 12.11.2014
 * Time: 10:30
 */
use yii\grid\GridView;

?>
<div id="left_wrapper">
    <div class="review">
        <?= $this->render('/partials/_profileMenu'); ?>
        <div style="clear: both"></div>
    </div>
    <!-- Right wrapper end -->
</div>
<br/>
<h1>Список победителей прошлых лотерей</h1>
<?=   GridView::widget([
    'dataProvider' => $dataProvider,
    'layout'=>'{pager} {items} {pager}',
    'columns' => [
        'attribute' => 'number',
        [
            'attribute' =>  'user_id',
            'value' =>function($value){
                return Yii::t('app', $value->user->username);
            }
        ],
        'date'
    ],

]); ?>
<br />