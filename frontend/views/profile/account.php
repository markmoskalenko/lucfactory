<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 15:40
 */
$this->title = 'Общая статистика';
?>
<div class="row">
    <div id="left_wrapper">
        <div class="review">
            <?= $this->render('/partials/_profileMenu'); ?>
            <div style="clear: both"></div>
        </div>
        <!-- Right wrapper end -->
    </div>

    <div class="right_wrapper">


        <div class="textblock" >
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left">&nbsp;</td>
                    <td align="center" style="padding-top: 8px;">Ваша дата регистрации:&nbsp;<font color="#000000"><?php if(!empty($oUser->created_at)) echo $oUser->created_at?></font></td>
                </tr>
            </table>
            <table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr><td colspan="2" align="center">&nbsp;</td></tr>
                <tr>
                    <td align="left" style="padding:3px;">ID</td>
                    <td align="left" style="padding:3px;"><font color="#000000"><?php if(!empty($oUser->id)) echo $oUser->id?></font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">Имя</td>
                    <td align="left" style="padding:3px;"><font color="#000000"><?php if(!empty( $oUser->username)) echo $oUser->username?></font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">E-Mail</td>
                    <td align="left" style="padding:3px;"><font color="#000000"><?php if(!empty( $oUser->email)) echo $oUser->email?></font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">Баланс для покупок</td>
                    <td align="left" style="padding:3px;"><font color="#000000"><?= (float)$oUser->in_balance?> USD</font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">Баланс на вывод</td>
                    <td align="left" style="padding:3px;"><font color="#000000"><?= (float)$oUser->out_balance?> USD</font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">Заработано на рефералах</td>
                    <td align="left" style="padding:3px;"><font color="#000000">
                            <?php $sum =0;
                            if($oUserReferrals){
                                foreach($oUserReferrals as $ref){
                                    foreach($ref->invoice as $invoice){
                                        $sum += ($invoice->sum*100*20)/100;
                                    }
                                }
                                echo $sum." USD";
                            }else{
                                echo 0.00." USD";
                            }
                            ?></font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">Выплачено</td>
                    <td align="left" style="padding:3px;"><font color="#000000">
                            <?php $summ = 0;
                            if($oUserPayment){
                                foreach ($oUserPayment as $payment){
                                    $summ += $payment->sum;
                                }
                                echo $summ." руб";
                            }else{
                                echo 0.00." руб";
                            }?></font></td>
                </tr>
                <tr>
                    <td align="left" style="padding:3px;">Проект существует</td>
                    <td align="left" style="padding:3px;"><font color="#000000">
                            <?php
                            $startDay = new \DateTime("2014-10-01 00:00:00");
                            $currentDay = new \DateTime(date('Y-m-d H:i:s'));
                            echo $startDay->diff($currentDay)->days;
                            ?> дня</font></td>
                </tr>
                <tr align="left">
                    <td colspan="2" style="padding:3px;">&nbsp;</td>
                </tr>
                <?php
                if($userReferral):?>
                    <tr>
                        <td align="left" style="padding:3px;">Вас пригласил:</td>
                        <td align="left" style="padding:3px;"><font color="#000000"><?=$userReferral->username?> его ID <?=$userReferral->id?></font></td>
                    </tr>
                <?php endif;?>
            </table>
        </div>
    </div>
</div>