<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 16:08
 */
use frontend\widgets\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Лотерея';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">
            <p>Принцип лотереи очень прост: для каждой новой лотереи имеется 10 билетов. Участники покупают эти билеты и после того, как все билеты будут раскуплены,
                состоится розыгрыш счастливых билетов. Система случайным образом выберет 3 номера счастливых билетов и зачислит призы их обладателям. Список завершенных лотерей можно посмотреть
                <a href="<?= \yii\helpers\Url::toRoute(['profile/all-lottery'])?>">здесь</a>.</p>
            <?= Alert::widget();?>
            <table width="738" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="padding-left: 40px; padding-top: 10px;"  width="130" valign="top">
                        <div id="error" class="h-title" style="color: red; float: left;">&nbsp;</div>
                        <div style="clear:both; margin-bottom: 12px;"></div>
                        1 место - 50% от общего банка (5000 USD).<br />
                        2 место - 25% от общего банка (2500 USD).<br />
                        3 место - 20% от общего банка (2000 USD).<br /><br />
                        Комиссия системы - 5%<br /><br />
                        Стоимость билета = 1000 USD.
                    </td>
                    <td valign="middle" width="130"  valign="top">
                        <form action="" method="post">
                            <a type="submit" style=" margin-top: 10px;" class="btn" href="<?= \yii\helpers\Url::toRoute(['pay/buy-lottery']) ?>">Купить билет</a>
                        </form>
                    </td>
                </tr>
            </table>
            <?=   GridView::widget([
                'dataProvider' => $dataProvider,
                'layout'=>'{pager} {items} {pager}',
                'columns' => [
                    'attribute' => 'number',
                    [
                        'attribute' =>  'user_id',
                        'value' =>function($value){
                            return Yii::t('app', $value->user->username);
                        }
                    ],
                    'date'
                ],

            ]); ?>
            <br />
        </div>
    </div>
</div>
<div style="clear:both;"></div>