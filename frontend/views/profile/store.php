<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 15:58
 */

use frontend\widgets\Alert;
use yii\helpers\Html;

$this->title = 'Фабрика';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">



            <p><img src="/img/6-1.png" width="300" height="185" border="0" align="left" style="margin-right: 14px;" />
                Все ваши нефтяные станции перекачивают нефть на эту фабрику по переработке топливо. Каждые 10 минут происходит перерасчет. Нефть постоянно накапливается в резервуарах фабрики и ее можно переработать в любое время, получив топливо. Не обязательно перерабатывать каждые 10 мин. Заходите в игру и перерабатывайте в удобное для вас время (резервуары для накопления нефти имеют бесконечную ёмкость). Полученное топливо можно будет в дальнейшем продать на рынке.
            </p>
            <div style="clear:both;"></div>

            <form action="" method="post">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right"><div id="error" class="h-title" style="color: red;">&nbsp;</div></td>
                        <td align="right" style="padding: 20px 24px 20px 14px;">
                            <a type="submit" style=" margin-top: 10px;" class="btn btn-primary btn-lg" href="<?= \yii\helpers\Url::toRoute(['pay/lutz']) ?>">Переработать нефть</a>
                        </td>
                    </tr>
                </table>
            </form>

            <?= Alert::widget();?>

            <?php foreach ($oStation as $station):?>
                <div class="sm-line farm"  >
                    <img src="/img/<?= $station->img?>" width="280" height="240" border="0" alt="" />
                    <div style=" display: block;color: #000000;">
                        Кол-во станций <?php if($station->userStation)  echo $station->category;?>: <?= ($station->userStation) ? $station->userStation->count_stations_purchased:0;?> <br />
                        Выкачано нефти: <?= ($station->userStation) ? $station->userStation->woter:0;?> бар.<br />
                        Топливо получено: <?= ($station->userStation) ? $station->userStation->lutz:0;?> gm.
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
<div style="clear:both;"></div>