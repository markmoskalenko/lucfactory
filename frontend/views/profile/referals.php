<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 16:08
 */

use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Партнерская программа';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>
        <div class="right_wrapper">
            <p>Приглашайте в игру своих друзей и знакомых. Вы будете получать 20% от каждого пополнения баланса вашими рефералами. Доход ни чем не ограничен и даже несколько приглашенных могут обеспечивать вас хорошим, пассивным, доходом навсегда. Ниже представлены варианты ваших, индивидуальных, оформлений ссылки для превлечения рефералов.</p>

            <table width="744" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                    <td style="margin-top: 16px;" width="514" valign="top">

                        <p><b>Ваша ссылка (URL):</b> <?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?><br /></p>

                        <p><b>Код для форумов (BBcode):</b></p>
                        <p><textarea rows="2" name="text" cols="85">[url=<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>]Экономическая игра с выводом денег[/url]</textarea></p>

                        <p><b>HTML код:</b></p>
                        <p><textarea rows="2" name="text" cols="85"><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank">Экономическая игра с выводом денег</a></textarea></p>



                        <a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"><img src="/100x100.gif" alt="Экономическая игра с выводом денег" width="100" height="100" border="0" /></a>
                        <p><b>HTML код баннера 100x100:</b></p>
                        <p><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"></a></p>
                        <p><textarea rows="3" name="text" cols="85"><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"><img src="<?= \Yii::$app->params['domain'] ?>/100x100.gif" alt="Экономическая игра с выводом денег" width="100" height="100" border="0" /></a></textarea></p>


                        <a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"><img src="/banner300.gif" alt="Экономическая игра с выводом денег" width="200" height="300" border="0" /></a>
                        <p><b>HTML код баннера 200x300:</b></p>
                        <p><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"></a></p>
                        <p><textarea rows="3" cols="85" name="text" cols="35"><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"><img src="<?= \Yii::$app->params['domain'] ?>/banner300.gif" alt="Экономическая игра с выводом денег" width="200" height="300" border="0" /></a></textarea></p>

                        <a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"><img src="/moreoil.gif" alt="Экономическая игра с выводом денег"  border="0" /></a>
                        <p><b>HTML код баннера 468x60:</b></p>
                        <p><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"></a></p>
                        <p><textarea rows="3" name="text" cols="85"><a href="<?= \Yii::$app->params['domain'] ?>/user/referral?id=<?=$oUser->id?>" target="_blank"><img src="<?= \Yii::$app->params['domain'] ?>/moreoil.gif" alt="Экономическая игра с выводом денег" width="468" height="60" border="0" /></a></textarea></p>

                    </td>
                </tr>
            </table>


            <a name="ref"></a>

            <?=GridView::widget([
                'dataProvider' => $dataProvider,
                'layout'=>'{pager} {items} {pager}',
                'columns' => [
                    'username',
                    'created_at'
                ],
            ]); ?>
            <br />
        </div>
        <div style="clear:both;"></div>

    </div>
</div>
</div>