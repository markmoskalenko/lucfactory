<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 15:58
 */
use frontend\widgets\Alert;
use yii\helpers\Html;

$this->title = 'Машинная отделение';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">
            <p> &nbsp;&nbsp;В машинном отделении вы можете приобрести различные нефтяные станции. Производительность нефтяных станции зависит от её категории. Нефтяные станции используются для выкачивания нефти, которую впоследствии можно переработать на специальной фабрике в топливо. Топливо можно продать за USD (местная валюта сайта), которые можно конвертировать в реальные деньги и/или использовать для покупки новых нефтяных станций. Вы можете покупать любое количество нефтяных станций,которые всегда будут приносить доход.</p>
            <center><div id="error" class="h-title" style="color: red;"><span style="color: #fff; font-weight: bold;">100 галлонов воды = 100 грамм луца = 1 чатл -> 100 USD = 1 рубль</span></div></center>
            <br />
            <?= Alert::widget();?>
            <table cellpadding="5" cellspacing="5" border="0" width="650" align="center">
                <?php foreach ($oStation as $station):?>
                    <tr>
                        <!--                <td width="300" align="center" valign="middle"></td>-->
                        <td align="left" valign="top" class="farm"  >
                            <img src="/img/<?= $station->img?>" width="280" height="240" border="0" alt="" />
                            <div style="float: right; display: block;width: 296px;color: #000000;line-height: 18px;">
                                <div style="margin: 50px 0px 0px 0px; font-weight: bold;"><?=$station->category?><br /><?=$station->name?></div>
                                Производительность:  <?=$station->performance?> баррелей в час<br />
                                Стоимость: <?=$station->price?> USD<br />
                                Куплено: <?= ($station->userStation) ? $station->userStation->count_stations_purchased:0;?> шт.<br />
                                <form action="" method="post">
                                    <a type="submit" style=" margin-top: 10px;" class="btn" href="<?= \yii\helpers\Url::toRoute(['pay/buy','station_id'=>$station->id]) ?>">Приобрести</a>
                                </form>
                            </div>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>
        </div>
    </div>
    <div style="clear:both;"></div>