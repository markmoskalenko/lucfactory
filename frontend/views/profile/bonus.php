<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 16:07
 */
use frontend\widgets\Alert;
use yii\grid\GridView;
use yii\helpers\Html;

$this->title = 'Ежедневный бонус';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">
            <center>
                Бонус выдется 1 раз в 24 часа. Бонус выдается USD на счет для покупок.
                Сумма бонуса генерируется случайно от <b>10</b> до <b>100</b> USD.
            </center>
            <?= Alert::widget();?>
            Последние 20 бонусов
            <?=   GridView::widget([
                'dataProvider' => $dataProvider,
                'layout'=>'{pager} {items} {pager}',
                'columns' => [
                    [
                        'label' => 'ID',
                        'attribute' => 'user_id',
                    ],
                    [
                        'attribute' =>  'user_id',
                        'value' =>function($value){
//                            var_dump($value->user[0]->attributes);
                            return Yii::t('app', $value->user->username);
                        }
                    ],
                    'money',
                    'date'
                ],

            ]); ?>
            <br />
        </div>
    </div>
</div>