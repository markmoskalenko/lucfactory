<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 15:59
 */
use frontend\widgets\Alert;
use yii\helpers\Html;

$this->title = 'Луцеколонка';
?>
<div class="site-about">
    <div class="row">
        <div id="left_wrapper">
            <div class="review">
                <?= $this->render('/partials/_profileMenu'); ?>
                <div style="clear: both"></div>
            </div>
            <!-- Right wrapper end -->
        </div>

        <div class="right_wrapper">
            <p><img src="/img/7-1.png" width="385" height="170" border="0" align="left" style="margin-right: 14px;" />
                Рынок - станция, на которой можно продать топливо и получить за него USD. USD, в дальнейшем, можно обменять на реальные деньги и вывести из игры. USD с продажи топливо распределяются между двумя счетами в пропорциях: 30% на счет предназначенный для покупок и 70% на вывод.
            </p>
            <div style="clear:both;"></div>
            <form action="" method="post">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="right"><div id="error" class="h-title" style="color: red;"><font color="#fff">Курс обмена: 100 грамм луца = 1 USD.</font></div></td>
                        <td align="right" style="padding: 20px 24px 20px 14px;">
                            <a type="submit" style=" margin-top: 10px;" class="btn btn-primary btn-lg" href="<?= \yii\helpers\Url::toRoute(['pay/sell-lutz']) ?>">Продать топливо</a>
                        </td>
                    </tr>
                </table>
                <?= Alert::widget();?>
                <br />
                <table width="744" border="0" cellpadding="2" cellspacing="2">
                    <tr>
                        <td style="padding-left: 40px;" valign="middle" width="200"><strong>Тип насоса</strong></td>
                        <td valign="middle" width="210"><strong>У вас в наличии</strong></td>
                        <td valign="middle"><strong>На сумму</strong></td>
                    </tr>
                    <?php foreach ($oStation as $station):?>
                        <tr>
                            <td style="padding-left: 40px;" valign="middle"><?= $station->category;?></td>
                            <td valign="middle"><?= ($station->userStation) ? (float)$station->userStation->lutz:0;?> топливо
                            </td>
                            <td valign="middle"><?= ($station->userStation) ? $station->userStation->lutz/100:0;?> USD</td>
                        </tr>
                    <?php endforeach;?>
                    <tr height="120">
                        <td colspan="3">&nbsp;</td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
</div>