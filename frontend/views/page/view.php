<?php
use frontend\widgets\Alert;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;


/**
 * @var frontend\controllers\ProfileController $this
 * @var yii\widgets\ActiveForm $form
 * @var \common\models\User $model
 */

$this->title = $model->title;
$this->registerMetaTag([
    'name' => 'description',
    'content' => $model->meta_d,
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $model->meta_k,
]);

?>
<div class="site-<?= $model->slug ?> container">
    <div class="row">
        <div class="col-lg-12">
            <h1><?= Html::encode($this->title) ?></h1>
            <hr/>
            <?= $model->content ?>

            <div style="clear: both"></div>

            <?php if($model->contact_feedback_form == $model->id):?>
                <?= Alert::widget();?>
                <h3>Форма обратной связи</h3>
                <?php $form = ActiveForm::begin(['id' => 'feedback-form']); ?>
                <?= $form->field($feedback, 'username')->textInput()?>
                <?= $form->field($feedback, 'email')->textInput()?>
                <?= $form->field($feedback, 'description')->textarea()?>
                <div style="clear:both;"></div>
                <div id="input_capcha" class="col-md-12">
                    <?= $form->field($feedback, 'captcha')->widget(\yii\captcha\Captcha::className()) ?>
                </div>
                <?= Html::submitButton(\Yii::t('app','Отправить'), ['class' => 'btn'])?>

                <?php ActiveForm::end(); ?>
            <?php endif;?>
        </div>
    </div>
</div>
