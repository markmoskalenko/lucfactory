<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 15:20
 */
use yii\helpers\Html;

$this->title = 'Новости';
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <table width="100%" align="center" cellpadding="5" cellspacing="5" border="0">
        <tr>
            <td align="left"><b>Открытие сайта!</b></td>
            <td align="right"><b>10.08.2014</b></td>
        </tr>
        <tr>
            <td colspan="2"><p>Поздравляю с открытием проекта, желаю высоких заработков всем! Не забывайте о существовании 20% реферальной системы, приглашайте знакомых, чтобы существенно увеличить свои доходы и помочь заработать другим.</p></td>
        </tr>
    </table>
    <br />
</div>
<div style="clear:both;"></div>
</div>
</div>
