<?php
namespace frontend\controllers;

use common\models\Invoice;
use frontend\models\Payeer;
use common\models\UserInvoice;
use Yii;
use yii\helpers\Url;
use yii\filters\AccessControl;

class PayeerController extends BaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'=>['index'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $order = UserInvoice::getOrderByUser(Yii::$app->user->id);

        if ($order->load(Yii::$app->request->post()) && $order->save()){
            return $this->redirect('http://payeer.com/merchant/'.$this->generateParams(array(
                    'm_shop' => Yii::$app->params['payeer']['m_shop'],
                    'm_orderid' => Payeer::getNewOrderId(),
                    'm_amount' => number_format($order->sum, 2, '.', ''),
                    'm_curr' => Yii::$app->params['payeer']['m_curr'],
                    'm_desc' => Payeer::getDesc(),
                    'm_sign' => Payeer::getSign(number_format($order->sum, 2, '.', '')),
                    'm_process' => 'Пополнить баланс',
                )));
//            return $this->redirect(Url::to(array('http://payeer.com/merchant/',
//                'm_shop' => Yii::$app->params['payeer']['m_shop'],
//                'm_orderid' => Payeer::getNewOrderId(),
//                'm_amount' => number_format($order->sum, 2, '.', ''),
//                'm_curr' => Yii::$app->params['payeer']['m_curr'],
//                'm_desc' => Payeer::getDesc(),
//                'm_sign' => Payeer::getSign(),
//                'm_process' => 'Пополнить баланс',
//            ), true));
        }

        return $this->render('index', [
            'model' => $order,
        ]);
    }

    private function generateParams($params)
    {
        $loop = 0;
        $s = '?';
        foreach($params as $key=>$value){
            $loop++;
            if ($loop != 1){
                $s .= '&';
            }
            $s .= $key.'='.$value;
        }

        return $s;
    }

    public function actionSuccess()
    {
        if (isset($_GET['m_operation_id']) && isset($_GET['m_sign']))
        {
            $m_key = \Yii::$app->params['payeer']['m_key'];
            $arHash = array($_GET['m_operation_id'],
                $_GET['m_operation_ps'],
                $_GET['m_operation_date'],
                $_GET['m_operation_pay_date'],
                $_GET['m_shop'],
                $_GET['m_orderid'],
                $_GET['m_amount'],
                $_GET['m_curr'],
                $_GET['m_desc'],
                $_GET['m_status'],
                $m_key);
            $sign_hash = strtoupper(hash('sha256', implode(':', $arHash)));

            if ($_GET['m_sign'] == $sign_hash && $_GET['m_status'] == 'success')
            {
                Invoice::addInvoice($_GET['m_amount'], 'Пополнение баланса: Payeer', Invoice::STATUS_SUCCESS, Invoice::TYPE_INCREMTNT);
                return $this->redirect('/payment/success');
            }
        }
        return $this->redirect('/payment/fail');
    }

    public function actionFail()
    {
        return $this->redirect('/payment/fail');
    }
}