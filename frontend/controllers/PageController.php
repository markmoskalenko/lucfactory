<?php

namespace frontend\controllers;

use common\models\Feedback;
use common\models\Pages;
use yii\web\NotFoundHttpException;
use Yii;

class PageController extends BaseController
{
    public function actionView($slug)
    {
        $page = Pages::find()->andWhere(['slug'=>$slug])->one();

        $oFeedback = new Feedback();

        if( !$page )
            throw new NotFoundHttpException(\Yii::t('app', 'Запрошенная страница не существует.'));


        if ( $oFeedback->load($_POST)){
            if($oFeedback->save()){
                Yii::$app->session->setFlash('success', 'Сообщение отправлено');
            }else{
                return $this->render('view', [
                    'model' => $page,
                    'feedback' => $oFeedback,
                ]);
            }
        }


        return $this->render('view', [
            'model' => $page,
            'feedback' => $oFeedback,
        ]);
    }

}
