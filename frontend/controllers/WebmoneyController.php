<?php
namespace frontend\controllers;

use common\models\Ticket;
use common\models\TicketSearch;
use Yii;
use yii\filters\AccessControl;

class WebmoneyController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'=>['index', 'create', 'tickets'],
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'tickets'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionCreate()
    {
        $model = new Ticket;

        $oUserTicket = new TicketSearch();
        $dataProvider = $oUserTicket->searchUserTicked();
        if ($model->load(Yii::$app->request->post())) {
            $model->created_at = time();
            $model->user_id = Yii::$app->user->id;

            if ($model->save())
                return $this->redirect(['webmoney/tickets']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'dataProvider' => $dataProvider
            ]);
        }
    }

    public function actionTickets()
    {
        $models = Ticket::getTickets(Yii::$app->user->id);

        return $this->render('list', [
            'models' => $models,
        ]);
    }

    public function actionRules()
    {
        return $this->render('rules');
    }
}