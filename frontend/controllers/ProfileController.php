<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 01.10.2014
 * Time: 15:37
 */

namespace frontend\controllers;


use common\models\Invoice;
use common\models\Lottery;
use common\models\Station;
use common\models\User;
use common\models\UserBonus;
use common\models\UserPayment;
use DateTime;
use Yii;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

class ProfileController extends BaseController {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only'=>['account','farm','store','market','referals','swap','lottery', 'all-lottery'],
                'rules' => [
                    [
                        'actions'=>['account','farm','store','market','referals','swap','lottery', 'all-lottery'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    'denyCallback' => function (){throw new ForbiddenHttpException(Yii::t('yii', 'Login Required'));},
                    [
                        'allow' => false,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actionAccount(){

        $oUser = User::u();

        $userReferral = User::findOne($oUser->referrals);

        $oUserPayment = UserPayment::find()->andWhere('user_id=:user_id',[':user_id'=>$oUser->id])->all();

        $oUserReferrals = User::find()->andWhere('referrals=:referrals',[':referrals'=>$oUser->id])->all();

        return $this->render('account',compact('oUser','userReferral','oUserPayment','oUserReferrals'));
    }

    public function actionFarm(){

        $oUser = User::u();

        $oStation = Station::find()->all();

        return $this->render('farm',compact('oUser','oStation'));
    }


    public function actionStore(){

        $oUser = User::u();

        $oStation = Station::find()->all();

        return $this->render('store',compact('oUser','oStation'));
    }


    public function actionMarket(){

        $oUser = User::u();

        $oStation = Station::find()->all();

        return $this->render('market',compact('oUser','oStation'));
    }

    public function actionBonus(){

        $oUser = User::u();

        $oUserBonus = UserBonus::find()->andWhere('user_id=:user_id', [':user_id'=>User::u()->id])->one();


        $searchModel = new UserBonus();

        $dataProvider = $searchModel->search();

        if($oUserBonus){
            $oUser->getBonus($oUserBonus);
        }else{
            $oUserBonus = new UserBonus;
            $oUser->getBonus($oUserBonus);
        }

        return $this->render('bonus',compact('dataProvider'));
    }

    public function actionLottery(){

        $oLottery = new Lottery();

        $dataProvider = $oLottery->searchAll();

        return $this->render('lottery',compact('dataProvider'));
    }

    public function actionAllLottery(){

        $oLottery = new Lottery();

        $dataProvider = $oLottery->search();

        return $this->render('all_lottery',compact('dataProvider'));
    }


    public function actionSwap(){

        $oUser = User::u();

        $oUser->setScenario('swap');

        if ($oUser->load(Yii::$app->request->post())){

            $oUser->swap = $_POST['User']['swap'];

            $oUser->sawp($oUser->swap);
        }
        return $this->render('swap',compact('oUser'));
    }
    public function actionReferals(){

        $oUser = User::u();

        $dataProvider = $oUser->searchReferral();

        return $this->render('referals',compact('oUser','dataProvider'));
    }
}