<?php
namespace frontend\controllers;

use common\models\Invoice;
use common\models\User;
use common\models\UserInvoice;
use common\models\UserPayment;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use Yii;
use yii\web\BadRequestHttpException;

class PaymentController extends BaseController
{
    public $enableCsrfValidation = false;

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'success' => ['get'],
                    'result' => ['post'],
                    'fail' => ['get'],
                ],

            ],
            'access' => [
                'class' => AccessControl::className(),
                'only'=>['index', 'invoice', 'payment', 'order', 'success', 'fail'],
                'rules' => [
                    [
                        'actions' => ['index', 'invoice', 'payment', 'order', 'success', 'fail'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionList()
    {
        $model = new UserPayment();
        $dataProvider = $model->searchPayments();

        return $this->render('list', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionInvoice()
    {
        $model = new Invoice();
        $model->user_id = Yii::$app->user->id;
        $model->status = Invoice::STATUS_PENDING;
        $model->type = Invoice::TYPE_INCREMTNT;
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            /** @var \robokassa\Merchant $merchant */
            $merchant = Yii::$app->get('robokassa');
            return $merchant->payment($model->sum, $model->id, 'Пополнение баланса', null, Yii::$app->user->identity->email);
        } else {
            return $this->render('invoice', [
                'model' => $model,
            ]);
        }
    }

    public function actionPayment(){
        $oUser = User::u();

        if($oUser->getUserInvoise()){
            return $this->redirect('order');
        }

        return $this->render('payment',compact('oUser'));
    }

    public function actionOrder()
    {
        $oUser = User::u();

        $model = new UserPayment();
        $dataProvider = $model->searchUserPayment();
        if ($model->load(Yii::$app->request->post())) {
            $model->attributes = $_POST['UserPayment'];
            $model->user_id = $oUser->id;
            $model->status = 0;

            $sum = $model->sum;

            if ( $sum > $oUser->out_balance ) {
                Yii::$app->session->setFlash('error', 'У вас не достаточно средств!');
                return $this->render('order', compact('model','dataProvider'));
            }

            if ( $sum ) {
                $oUser->setScenario('buy');
                $oUser->out_balance -= $sum*100;
                $oUser->save();

                $model->save();

                Yii::$app->session->setFlash('success', 'Ваш запрос принят!');
            } else {
                Yii::$app->session->setFlash('error', 'Вы не ввели сумму');
                return $this->render('order', compact('model','dataProvider'));
            }
        }
        return $this->render('order', compact('model', 'dataProvider'));
    }

    public function actionSuccess()
    {
        $model = User::u();

        return $this->render('success',compact('model'));
    }

    public function actionFail()
    {
        $model = User::u();

        return $this->render('fail',compact('model'));
    }

    /**
     * @param integer $id
     * @return Invoice
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Invoice::find($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }
}