<?php
namespace frontend\controllers;

use common\models\UserInvoice;
use Yii;
use YandexMoney\API;
use common\models\Invoice;

class YandexController extends BaseController
{
    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionResult()
    {
        if (isset($_REQUEST['code'])){
            $code = $_REQUEST['code'];
            $client_id = \Yii::$app->params['yandexMoney']['client_id'];
            $redirect_uri = \Yii::$app->params['yandexMoney']['redirect_uri'];
            $client_secret = \Yii::$app->params['yandexMoney']['client_secret'];
            $access_token = API::getAccessToken($client_id, $code, $redirect_uri, $client_secret);
            $order = UserInvoice::getOrderByUser(Yii::$app->user->id);
            if (!$order->sum || $order->sum < 1)
                return $this->redirect('/yandex/pay');
            $sum = $order->sum;

            if ($access_token == null || $access_token instanceof \stdClass){
                $resCheck = $this->getAccessTokenError($access_token);
                if ($resCheck){
                    \Yii::$app->getSession()->setFlash('error', 'Ошбика авторизации или подверждения прав использования Яндекс Денег');
                    return $this->redirect('/payment/fail');
                } else {
                    $access_token = $access_token->access_token;
                }
            }

            $api = new API($access_token);

            $model = Invoice::addInvoice($sum, 'Пополнение баланса: Яндекс Деньги', Invoice::STATUS_PENDING, Invoice::TYPE_INCREMTNT);

            // make request payment
            $request_payment = $api->requestPayment(array(
                "pattern_id" => "p2p",
                "to" => \Yii::$app->params['yandexMoney']['wallet'],
                "amount_due" => $sum,
                "comment" => 'Пополнение баланаса Lucfactory',
                "message" => 'Пополнение баланса Lucfactory',
                "label" => $model->id,
            ));

            // check status
            if ($request_payment->status != 'success'){
                \Yii::$app->getSession()->setFlash('error', 'Ошибка платежа: '.$request_payment->error);
                return $this->redirect('/payment/fail');
            }

            // call process payment to finish payment
            $process_payment = $api->processPayment(array(
                "request_id" => $request_payment->request_id,
            ));

            if ($process_payment->status != 'success'){
                \Yii::$app->getSession()->setFlash('error', 'Ошибка перевода средств: '.$process_payment->error);
                return $this->redirect('/payment/fail');
            }
        } else {
            return $this->redirect('/payment/fail');
        }

        $model = Invoice::addInvoice($sum, 'Пополнение баланса: Яндекс Деньги', Invoice::STATUS_SUCCESS, Invoice::TYPE_INCREMTNT, $model->id);

        $order->delete();

        return $this->redirect('/payment/success');
    }

    public function actionPay()
    {
        $model = UserInvoice::getOrderByUser(Yii::$app->user->id);

        if ($model->load(Yii::$app->request->post()) && $model->save()){
            $client_id = \Yii::$app->params['yandexMoney']['client_id'];
            $redirect_uri = \Yii::$app->params['yandexMoney']['redirect_uri'];
            $client_secret = \Yii::$app->params['yandexMoney']['client_secret'];

            $order = UserInvoice::getOrderByUser(Yii::$app->user->id);
            if (!$order->sum || $order->sum < 1)
                return $this->redirect('/yandex/pay');

            $sum = $order->sum;
            $sum = round($sum, 2);
            $scope = [
                'payment-p2p.limit(,'.$sum.')',
            ];

            $auth_url = API::buildObtainTokenUrl($client_id, $redirect_uri, $scope, $client_secret);

            return $this->redirect($auth_url);
        }
        return $this->render('pay', [
            'model' => $model,
        ]);
    }

    private function getAccessTokenError($accessToken)
    {
        if (isset($accessToken->error)){
            switch ($accessToken->error){
                case 'invalid_request':
                    return 'Обязательные параметры запроса отсутствуют или имеют некорректные или недопустимые значения.';
                case 'unauthorized_client':
                    return 'Неверное значение параметра client_id или client_secret, либо приложение не имеет права запрашивать авторизацию (например, его client_id заблокирован Яндекс.Деньгами).';
                case 'invalid_grant':
                    return 'В выдаче access_token отказано. Временный токен не выдавался Яндекс.Деньгами, либо просрочен, либо по этому временному токену уже выдан access_token (повторный запрос токена авторизации с тем же временным токеном).';
                default:
                    return 'Неизвестная ошибка с кодом: '.$accessToken->error;
            }
        }

        return false;
    }
}