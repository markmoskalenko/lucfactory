<?php
/**
 * Created by PhpStorm.
 * User: Dima
 * Date: 12.11.2014
 * Time: 22:12
 */

namespace frontend\controllers;


use common\models\User;

class BaseController extends \frontend\components\Controller{

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
            ],
        ];
    }

    public function beforeAction($action){
        if(!\Yii::$app->user->isGuest){
            $oUser = User::u();
            $oUser->setScenario('online');
            $oUser->online = date('Y-m-d H:i:s');
            $oUser->save();
        }
        return true;
    }
} 