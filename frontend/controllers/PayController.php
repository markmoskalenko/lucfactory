<?php
namespace frontend\controllers;

use common\models\Lottery;
use common\models\Station;
use common\models\User;
use common\models\UserStation;
use Yii;
use yii\filters\AccessControl;

class PayController extends BaseController
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'=>['buy', 'lutz', 'sell-lutz', 'buy-lottery'],
                'rules' => [
                    [
                        'actions' => ['buy', 'lutz', 'sell-lutz', 'buy-lottery'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionBuy($station_id)
    {

        $station = Station::findOne($station_id);

        $oUserStation = UserStation::find()->andWhere('stantion_id=:stantion_id AND user_id=:user_id', [':stantion_id' => $station_id, ':user_id' => User::u()->id])->one();

        if ($oUserStation) {

            $station->buy($oUserStation, $station);

            return $this->redirect('/profile/farm');

        } else {

            $oUserStation = new UserStation();

            $station->buy($oUserStation, $station);

            return $this->redirect('/profile/farm');
        }
    }

    public function actionLutz()
    {

        $oUserStation = UserStation::find()->andWhere('user_id=:user_id', [':user_id' => User::u()->id])->all();

        $curentDate = strtotime(date('Y-m-d H:i:s'));

        foreach ($oUserStation as $station) {

            if ($curentDate > strtotime($station->update) + 10 * 60) {

                $station->lutz += $station->woter;

                if ($station->lutz == 0) {

                    Yii::$app->session->setFlash('error', Yii::t('app', 'У вас не достаточно нефти!'));

                    return $this->redirect('/profile/store');

                }

                $station->woter = 0;

                $station->update = date('Y-m-d H:i:s');

                if ($station->save()) {

                    Yii::$app->session->setFlash('success', Yii::t('app', 'Топливо успешно переработано!'));

                }
            } else {
                Yii::$app->session->setFlash('error', Yii::t('app', 'Не прошло еще 10 минут. Пожалуйста подождите!'));
            }
        }
        return $this->redirect('/profile/store');
    }

    public function actionSellLutz()
    {

        $oUser = User::u();

        $oUser->setScenario('lutz');

        $oUserStation = UserStation::find()->andWhere('user_id=:user_id', [':user_id' => User::u()->id])->all();

        $balance = 0;

        foreach ($oUserStation as $station) {

            $balance += $station->lutz;

            if ($balance == 0) {

                Yii::$app->session->setFlash('error', Yii::t('app', 'У вас не достаточно топлива!'));

                return $this->redirect('/profile/market');
            }

            $station->lutz = 0;

            $station->save();
        }
        $lutz = $balance / 100;

        if ($lutz != 0) {

            if ($oUser->getChatls($lutz)) {

                Yii::$app->session->setFlash('success', Yii::t('app', 'Топливо успешно продано!'));

                return $this->redirect('/profile/market');
            }
        }
    }

    public function actionBuyLottery()
    {
        $oLottery = new Lottery();
        $oLottery->buyLottery();
        return $this->redirect('/profile/lottery');
    }
}