<?php
namespace backend\controllers;
use backend\components\Controller;
use common\models\Invoice;
use common\models\InvoiceSearch;
use common\models\User;
use common\models\UserPayment;
use yii\web\BadRequestHttpException;
use Yii;
use yii\web\NotFoundHttpException;

class TransactionController extends Controller
{
    /**
     * Lists all Invoice models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new InvoiceSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('list', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreate()
    {
        $model = new Invoice();

        $oUsers = User::getTreeMap();
        if ($model->load(Yii::$app->request->post())) {
            $model->status = 1;
            $model->type = 1;
            $model->date = strtotime(date('Y-m-d h:i:s'));
            $oUser = User::findOne($model->user_id);
            $oUser->setScenario('buy');
            $oUser->in_balance +=$model->sum*100;

            $oReferralUsers = User::find()->andWhere('id=:id',[':id'=>$oUser->referrals])->one();
            $referral_balace = ($model->sum*100*20)/100;

            if($oReferralUsers){
                $oReferralUsers->setScenario('buy');
                $oReferralUsers->in_balance += $referral_balace;
                $oReferralUsers->save();
            }

            $oUser->save();
            if( $model->save())
            return $this->redirect(['index']);
        } else {
            return $this->render('create', [
                'model' => $model,
                'oUsers' =>$oUsers,
            ]);
        }
    }
}