<?php
namespace backend\controllers;
use backend\components\Controller;
use common\models\Invoice;
use common\models\InvoiceSearch;
use common\models\User;
use common\models\UserPayment;
use yii\web\BadRequestHttpException;
use Yii;
use yii\web\NotFoundHttpException;

class PaymentController extends Controller
{
    public function actionCreate()
    {
        $model = new UserPayment();

        $oUsers = User::getTreeMap();
        if ($model->load(Yii::$app->request->post())) {
            if($model->status){
                $oUser = User::findOne($model->user_id);
                $oUser->setScenario('buy');
                $oUser->out_balance -=$model->sum*100;
                $oUser->save();
            }

            if($model->save()){
                return $this->redirect(['index']);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'oUsers' =>$oUsers,
            ]);
        }
    }

    public function actionIndex()
    {
        $model = new UserPayment();
        $dataProvider = $model->search();

        return $this->render('index', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModelPayment($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return Invoice
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Invoice::find($id);
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }

    protected function findModelPayment($id)
    {
        if (($model = UserPayment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}