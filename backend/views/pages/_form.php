<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\imperavi\Widget as Editor;

/* @var $this yii\web\View */
/* @var $model common\models\Pages */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="pages-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype'=>'multipart/form-data']]);?>

    <?=  $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <!--    --><?//= $form->field($model, 'description')->textarea() ?>

    <!--    --><?//= $form->field($model, 'content')->textarea() ?>


    <!--    --><?//= $form->field($model, 'meta_k')->textInput(['maxlength' => 255]) ?>

    <!--    --><?//= $form->field($model, 'meta_d')->textarea(['maxlength' => 1024]) ?>

<!--    <?//= $form->field($model, 'created_at')->textInput() ?>-->

<!--    --><?//= $form->field($model, 'is_public')->checkbox() ?>

<!--    --><?php
//        if (!$model->isNewRecord){
//    ?>
<!--          --><?//=  $form->field($model, 'slug')->textInput(['maxlength' => 255]) ?>
<!--    --><?php
//        }
//    ?>

    <?= Html::activeLabel($model, 'content') ?>
    <?=
    Editor::widget([
        'model' => $model,
        'attribute' => 'content',

        // Some options, see http://imperavi.com/redactor/docs/
        'options' => [
            'toolbar' => true,
//        'css' => 'wym.css',
        ],
        'plugins' => [
        ]
    ]);
    ?>
    <br />

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Создать') : Yii::t('app', 'Обновить'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
