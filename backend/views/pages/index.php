<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PagesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Страницы');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pages-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Добавить'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'slug',
            [
                'attribute' => 'title',
            ],
//            [
//                'attribute' => 'description',
//            ],
//            [
//                'attribute' => 'is_public',
//                'value' => function ($value) {
//                        return $value->is_public ? Yii::t('app', 'Да') : Yii::t('app', 'Нет');
//                    }
//            ],
//            [
//                'attribute' => 'created_at',
//                'value' => function ($value) {
//                        return date('d.m.Y H:i', $value->created_at);
//                    }
//            ],

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}'
            ],
        ],
    ]); ?>

</div>
