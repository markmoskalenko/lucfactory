<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;

/**
 * @var \yii\web\View $this
 * @var string $content
 */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
    <?php $this->beginBody() ?>
    <div class="wrap">
        <?php
            NavBar::begin([
                'brandUrl' => Yii::$app->homeUrl,
                'innerContainerOptions'=>['class'=>'container-fluid'],
                'options' => [
                    'class' => 'navbar-inverse navbar-fixed-top',
                ],
            ]);
            $menuItems = [
                ['label' => 'Главная', 'url' => ['/site/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label' => \Yii::t('app', 'Выплаты'), 'url' => ['/payment/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label' => \Yii::t('app', 'Транзакции'), 'url' => ['/transaction/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label' => \Yii::t('app', 'Станции'), 'url' => ['/station/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label' => \Yii::t('app', 'Страницы'), 'url' => ['/pages/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label' => \Yii::t('app', 'Обратная связь'), 'url' => ['/feedback/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label' => \Yii::t('app', 'Тикеты'), 'url' => ['/ticket/index'], 'visible'=> !Yii::$app->user->isGuest],
                ['label'=>'Пользователи', 'items'=>[
                    ['label' => \Yii::t('app', 'Пользователи'), 'url' => ['/user/index'], 'visible'=> !Yii::$app->user->isGuest],
//                    ['label' => \Yii::t('app', 'Расходы'), 'url' => ['/payment/list'], 'visible'=> !Yii::$app->user->isGuest],
                ]],

            ];
            if (Yii::$app->user->isGuest) {
                $menuItems[] = ['label' => 'Войти', 'url' => ['/site/login']];
            } else {
                $menuItems[] = [
                    'label' => 'Выйти (' . Yii::$app->user->identity->username . ')',
                    'url' => ['/site/logout'],
                    'linkOptions' => ['data-method' => 'post']
                ];
            }

//        $menuItems[] = ['label'=>\backend\widgets\Lang::widget()];

        echo Nav::widget([
                'options' => ['class' => 'navbar-nav navbar-right'],
                'items' => $menuItems,
                'encodeLabels' => false,

        ]);
            NavBar::end();
        ?>
        <div class="container-fluid" style="margin-top: 60px">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>

        <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container-fluid">
        <p class="pull-left">&copy; <a href="it-yes.com">IT-YES.COM</a> <?= date('Y') ?></p>
        </div>
    </footer>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
