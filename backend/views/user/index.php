<?php

use common\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Пользователи';
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p style="color: green;">
        <?= Yii::$app->session->getFlash('messageSent'); ?>
    </p>

    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'username',
            'email:email',
            'created_at',
            'role',
            [
                'attribute'=>'station',
                'format' => 'html',
                'value' => function ($value) {
                    if($value->userStation) {
                        $stat = "";
                        foreach ($value->userStation as $station) {
                            $stat .= $station->stations->category . ':  ' . $station->count_stations_purchased."<br/>";
                        }
                        return $stat;
                    }else{
                        return 'Пользователь не приобрел ни одной станции';
                    }
                }
            ],
            'in_balance',

            'out_balance',
            [
                'header'=>'Количество рефералов пользователя',
                'format' => 'html',
                'value' => function ($value) {
                        return User::find()->where(['referrals'=>$value->id])->count();
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update} {delete}',
            ],
        ],
    ]); ?>
</div>