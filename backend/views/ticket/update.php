<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\models\Ticket $model
 */

$this->title = Yii::t('app', 'Редактирование {modelClass}: ', [
  'modelClass' => 'тикета',
]) . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Тикеты'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Редактирование');
?>
<div class="ticket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
