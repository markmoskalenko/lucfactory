<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\User;
use common\models\Ticket;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\models\TicketSearch $searchModel
 */

$this->title = Yii::t('app', 'Тикеты');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ticket-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a(Yii::t('app', 'Create {modelClass}', [
//  'modelClass' => 'Ticket',
//]), ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'title',
            'message:ntext',
            [
                'attribute'=>'created_at',
                'value'=>function($data){
                        return date('d.m.Y', $data->created_at);
                    }
            ],
            [
                'attribute'=>'user_id',
                'value'=>function($data){
                        $user = User::findOne(['id'=>$data->user_id]);
                        return $user->username;
                    }
            ],
            [
                'attribute'=>'status',
                'value'=>function($data){
                        return $data->status == Ticket::STATUS_OK ? 'Подтвержден' : 'Новый';
                    }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
