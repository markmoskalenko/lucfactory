<?php

use yii\db\Schema;
use yii\db\Migration;

class m141015_213949_dublicate_invoice extends Migration
{
    public function up()
    {
        $this->createTable('tbl_invoice', [
            'id' => 'pk',
            'sum' => Schema::TYPE_DECIMAL . ' NOT NULL',
            'date' => Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER,
            'type' => Schema::TYPE_DECIMAL,
            'description' => Schema::TYPE_STRING,
            'balance' => Schema::TYPE_DECIMAL,
        ]);
    }

    public function down()
    {
        $this->dropTable('tbl_invoice');
    }
}
