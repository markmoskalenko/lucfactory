<?php

use yii\db\Schema;
use yii\db\Migration;

class m140909_081146_update_column_s_status_title extends Migration
{
    public function up()
    {

        $this->alterColumn('{{%user}}', 's_status_title', 'text');
    }

    public function down()
    {
        echo "m140909_081146_update_column_s_status_title cannot be reverted.\n";

        return false;
    }
}
