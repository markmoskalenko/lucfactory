<?php

use yii\db\Schema;
use yii\db\Migration;

class m141127_065859_add_new_teble_feedback extends Migration
{
    public function up()
    {
        $this->execute("
         CREATE TABLE `tbl_feedback` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


         ");
    }

    public function down()
    {
        echo "m141127_065859_add_new_teble_feedback cannot be reverted.\n";

        return false;
    }
}
