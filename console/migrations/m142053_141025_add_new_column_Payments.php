<?php

use yii\db\Schema;
use yii\db\Migration;

class m142053_141025_add_new_column_Payments extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_payments','created_at','TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->addColumn('tbl_payments','purse','varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('tbl_payments','created_at','TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->dropColumn('tbl_payments','purse','varchar(255)');
    }
}
