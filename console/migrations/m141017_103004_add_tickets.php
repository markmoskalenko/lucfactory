<?php

use yii\db\Schema;

class m141017_103004_add_tickets extends \yii\db\Migration
{
    public function up()
    {
        $this->createTable('tbl_tickets', array(
            'id' => 'pk',
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'message' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER .' NOT NULL',
            'status' => Schema::TYPE_INTEGER .' DEFAULT 1',
        ));

    }

    public function down()
    {
        $this->dropTable('tbl_tickets');
    }
}
