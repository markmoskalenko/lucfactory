<?php

use yii\db\Schema;
use yii\db\Migration;

class m140829_075208_create_messages_table extends Migration
{
    public function up()
    {
        $this->createTable('tbl_messages', [
            'id' => 'pk',
            'date' => Schema::TYPE_DATETIME,
            'to_user_id' => Schema::TYPE_INTEGER,
            'name' => Schema::TYPE_STRING,
            'is_read' => Schema::TYPE_SMALLINT . '(6)',
            'email' => Schema::TYPE_STRING,
            'text' => Schema::TYPE_TEXT,
            'category' => Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        $this->dropTable('tbl_messages');
    }
}
