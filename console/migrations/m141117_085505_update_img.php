<?php

use yii\db\Schema;
use yii\db\Migration;

class m141117_085505_update_img extends Migration
{
    public function up()
    {
        $this->execute("
        UPDATE tbl_station SET img='001.png' WHERE id=1;
        UPDATE tbl_station SET img='002.png' WHERE id=2;
        UPDATE tbl_station SET img='003.png' WHERE id=3;
        UPDATE tbl_station SET img='004.png' WHERE id=4;
        UPDATE tbl_station SET img='005.png' WHERE id=5;
        ");

    }

    public function down()
    {
        echo "m141117_085505_update_img cannot be reverted.\n";

        return false;
    }
}
