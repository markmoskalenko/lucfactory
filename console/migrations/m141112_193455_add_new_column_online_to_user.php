<?php

use yii\db\Schema;
use yii\db\Migration;

class m141112_193455_add_new_column_online_to_user extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_user','online','timestamp');
    }

    public function down()
    {
        $this->dropColumn('tbl_user','online','timestamp');
    }
}
