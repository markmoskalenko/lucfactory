<?php

use yii\db\Schema;

class m140728_185104_activateToken extends \yii\db\Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}', 'activate_key', Schema::TYPE_STRING . '(255)');
    }

    public function down()
    {
        echo "m140728_185104_activateToken cannot be reverted.\n";

        return false;
    }
}
