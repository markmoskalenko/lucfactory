<?php

use yii\db\Schema;
use yii\db\Migration;

class m141126_103625_add_new_records_in_tbl_pages extends Migration
{
    public function up()
    {
        $this->execute(" insert into tbl_pages(created_at,slug,title) values ('1412604272','contact','Контакты')");
        $this->execute(" insert into tbl_pages(created_at,slug,title) values ('1412604272','rules','Правила')");
    }

    public function down()
    {
        echo "m141126_103625_add_new_records_in_tbl_pages cannot be reverted.\n";

        return false;
    }
}
