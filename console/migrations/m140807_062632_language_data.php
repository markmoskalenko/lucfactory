<?php

use yii\db\Schema;

class m140807_062632_language_data extends \yii\db\Migration
{
    public function up()
    {
        $this->dropColumn('{{%dictionary_language}}', 'abbr');

        $oLang = new \common\models\DictionaryLanguage();
        $oLang->url = 'en';
        $oLang->local = 'en-EN';
        $oLang->name = 'English';
        $oLang->default = 0;
        $oLang->save();

        $oLang = new \common\models\DictionaryLanguage();
        $oLang->url = 'ru';
        $oLang->local = 'ru-RU';
        $oLang->name = 'Русский';
        $oLang->default = 1;
        $oLang->save();

    }

    public function down()
    {
        echo "m140807_062632_language_data cannot be reverted.\n";

        return false;
    }
}
