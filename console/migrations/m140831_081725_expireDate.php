<?php

use yii\db\Schema;
use yii\db\Migration;

class m140831_081725_expireDate extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_user', 'expire_date', 'int(11) unsigned');

    }

    public function down()
    {
        echo "m140831_081725_expireDate cannot be reverted.\n";

        return false;
    }
}
