<?php

use yii\db\Schema;
use yii\db\Migration;

class m141053_140825_add_new_column_User extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_user','referrals','int(11)');
    }

    public function down()
    {
        $this->dropColumn('tbl_user','referrals','int(11)');
    }
}
