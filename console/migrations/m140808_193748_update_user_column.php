<?php

use yii\db\Schema;
use yii\db\Migration;

class m140808_193748_update_user_column extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%user}}', 'facebook', Schema::TYPE_STRING . '(255)');
        $this->dropColumn('{{%user}}', 'twitter', Schema::TYPE_STRING . '(255)');
        $this->dropColumn('{{%user}}', 'google', Schema::TYPE_STRING . '(255)');
        $this->dropColumn('{{%user}}', 'vk', Schema::TYPE_STRING . '(255)');
        $this->dropColumn('{{%user}}', 'git', Schema::TYPE_STRING . '(255)');
        $this->dropColumn('{{%user}}', 'mail', Schema::TYPE_STRING . '(255)');

        $this->addColumn('{{%user}}', 'my_site', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%user}}', 'phone_mob', Schema::TYPE_STRING . '(255)');

    }

    public function down()
    {
        echo "m140808_193748_update_user_column cannot be reverted.\n";

        return false;
    }
}
