<?php

use yii\db\Schema;
use yii\db\Migration;

class m141033_140525_add_new_table_lottery extends Migration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE `tbl_lottery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `number` int(11) NOT NULL,
  `winer` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_tbl_lottery` (`user_id`),
  CONSTRAINT `FK_tbl_lottery` FOREIGN KEY (`user_id`) REFERENCES `tbl_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=275 DEFAULT CHARSET=utf8

         ");
    }

    public function down()
    {
        $this->dropTable('tbl_lottery');
    }
}
