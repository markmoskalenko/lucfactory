<?php

use yii\db\Schema;
use yii\db\Migration;

class m140827_101041_create_pages_table extends Migration
{
    public function up()
    {
        $this->createTable('tbl_pages', [
            'id' => 'pk',
            'created_at' => Schema::TYPE_INTEGER,
            'is_public' => Schema::TYPE_SMALLINT . '(6)',
            'slug' => Schema::TYPE_STRING .' NOT NULL',
        ]);

        $this->createTable('tbl_pages_translate', [
            'id' => 'pk',
            'title' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT,
            'content' => Schema::TYPE_TEXT,
            'meta_k' => Schema::TYPE_STRING,
            'meta_d' => Schema::TYPE_STRING,
            'object_id' => Schema::TYPE_INTEGER .' NOT NULL',
            'language_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('fk_pages_pages_translate', 'tbl_pages_translate', 'object_id', 'tbl_pages', 'id', 'CASCADE');
//        $this->addForeignKey('fk_language_pages_translate', 'tbl_pages_translate', 'language_id', 'tbl_dictionary_language', 'id');
    }

    public function down()
    {
        $this->dropTable('tbl_pages_translate');
        $this->dropTable('tbl_pages');
    }
}
