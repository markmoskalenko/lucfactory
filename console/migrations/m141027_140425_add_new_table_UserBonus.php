<?php

use yii\db\Schema;
use yii\db\Migration;

class m141027_140425_add_new_table_UserBonus extends Migration
{
    public function up()
    {
        $this->execute("
          CREATE TABLE IF NOT EXISTS `tbl_bonus` (
          `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
          `user_id` int(11) unsigned NOT NULL,
          `money` int(11) NOT NULL,
          `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
          PRIMARY KEY (`id`),
          KEY `FK_bonus` (`user_id`)
          ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=35 ;
         ");
    }

    public function down()
    {
        $this->dropTable('tbl_bonus');
    }
}
