<?php

use yii\db\Schema;
use yii\db\Migration;

class m140909_120912_add_column_profession_for_User extends Migration
{
    public function up()
    {
        $this->addColumn('{{%user}}','profession','varchar(255)');
    }

    public function down()
    {
        echo "m140909_120912_add_column_profession_for_User cannot be reverted.\n";

        return false;
    }
}
