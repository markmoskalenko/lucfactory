<?php

use yii\db\Schema;
use yii\db\Migration;

class m141126_121507_add_forigen_key_bonus_user extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_bonus_user', 'tbl_bonus', 'user_id', 'tbl_user', 'id', 'cascade');
    }

    public function down()
    {
        echo "m141126_121507_add_forigen_key_bonus_user cannot be reverted.\n";

        return false;
    }
}
