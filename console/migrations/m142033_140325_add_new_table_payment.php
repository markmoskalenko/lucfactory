<?php

use yii\db\Schema;
use yii\db\Migration;

class m142033_140325_add_new_table_payment extends Migration
{
    public function up()
    {
        $this->execute("
         CREATE TABLE `tbl_payments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `sum` decimal(20,2) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


         ");
    }

    public function down()
    {
        $this->dropTable('tbl_payments');
    }
}
