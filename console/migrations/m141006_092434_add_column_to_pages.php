<?php

use yii\db\Schema;
use yii\db\Migration;

class m141006_092434_add_column_to_pages extends Migration
{
    public function up()
    {
        $this->addColumn('tbl_pages','title','varchar(255)');
        $this->addColumn('tbl_pages','description','text');
        $this->addColumn('tbl_pages','content','text');
        $this->addColumn('tbl_pages','meta_k','varchar(255)');
        $this->addColumn('tbl_pages','meta_d','varchar(255)');
    }

    public function down()
    {
        $this->dropColumn('tbl_pages','title','varchar(255)');
        $this->dropColumn('tbl_pages','description','text');
        $this->dropColumn('tbl_pages','content','text');
        $this->dropColumn('tbl_pages','meta_k','varchar(255)');
        $this->dropColumn('tbl_pages','meta_d','varchar(255)');
    }
}
