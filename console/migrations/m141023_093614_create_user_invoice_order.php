<?php

use yii\db\Schema;
use yii\db\Migration;

class m141023_093614_create_user_invoice_order extends Migration
{
    public function up()
    {
        $this->createTable('tbl_user_invoice', array(
            'id' => 'pk',
            'sum' => Schema::TYPE_TEXT,
            'created_at' => Schema::TYPE_INTEGER,
            'user_id' => Schema::TYPE_INTEGER .' NOT NULL',
        ));
    }

    public function down()
    {
        $this->dropTable('tbl_user_invoice');
    }
}
