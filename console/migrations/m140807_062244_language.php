<?php

use yii\db\Schema;

class m140807_062244_language extends \yii\db\Migration
{
    public function up()
    {

        $this->addColumn('{{%dictionary_language}}', 'local', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%dictionary_language}}', 'url', Schema::TYPE_STRING . '(255)');
        $this->addColumn('{{%dictionary_language}}', 'default', Schema::TYPE_SMALLINT . '(6)');
    }

    public function down()
    {
        echo "m140807_062244_language cannot be reverted.\n";

        return false;
    }
}
